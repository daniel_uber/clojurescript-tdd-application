// Compiled by ClojureScript 1.9.854 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__31750 = arguments.length;
switch (G__31750) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if(typeof cljs.core.async.t_cljs$core$async31751 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31751 = (function (f,blockable,meta31752){
this.f = f;
this.blockable = blockable;
this.meta31752 = meta31752;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async31751.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31753,meta31752__$1){
var self__ = this;
var _31753__$1 = this;
return (new cljs.core.async.t_cljs$core$async31751(self__.f,self__.blockable,meta31752__$1));
});

cljs.core.async.t_cljs$core$async31751.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31753){
var self__ = this;
var _31753__$1 = this;
return self__.meta31752;
});

cljs.core.async.t_cljs$core$async31751.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31751.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async31751.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async31751.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async31751.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta31752","meta31752",-793030918,null)], null);
});

cljs.core.async.t_cljs$core$async31751.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async31751.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31751";

cljs.core.async.t_cljs$core$async31751.cljs$lang$ctorPrWriter = (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async31751");
});

cljs.core.async.__GT_t_cljs$core$async31751 = (function cljs$core$async$__GT_t_cljs$core$async31751(f__$1,blockable__$1,meta31752){
return (new cljs.core.async.t_cljs$core$async31751(f__$1,blockable__$1,meta31752));
});

}

return (new cljs.core.async.t_cljs$core$async31751(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if(!((buff == null))){
if((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__31757 = arguments.length;
switch (G__31757) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Assert failed: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1("buffer must be supplied when transducer is"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("buf-or-n")].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__31760 = arguments.length;
switch (G__31760) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__31763 = arguments.length;
switch (G__31763) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_31765 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_31765);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_31765,ret){
return (function (){
return fn1.call(null,val_31765);
});})(val_31765,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__31767 = arguments.length;
switch (G__31767) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__4655__auto__)){
var ret = temp__4655__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__4655__auto__)){
var retb = temp__4655__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__4655__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__4655__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__28909__auto___31769 = n;
var x_31770 = (0);
while(true){
if((x_31770 < n__28909__auto___31769)){
(a[x_31770] = (0));

var G__31771 = (x_31770 + (1));
x_31770 = G__31771;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__31772 = (i + (1));
i = G__31772;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t_cljs$core$async31773 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31773 = (function (flag,meta31774){
this.flag = flag;
this.meta31774 = meta31774;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async31773.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_31775,meta31774__$1){
var self__ = this;
var _31775__$1 = this;
return (new cljs.core.async.t_cljs$core$async31773(self__.flag,meta31774__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async31773.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_31775){
var self__ = this;
var _31775__$1 = this;
return self__.meta31774;
});})(flag))
;

cljs.core.async.t_cljs$core$async31773.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31773.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async31773.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async31773.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async31773.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta31774","meta31774",-225765884,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async31773.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async31773.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31773";

cljs.core.async.t_cljs$core$async31773.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async31773");
});})(flag))
;

cljs.core.async.__GT_t_cljs$core$async31773 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async31773(flag__$1,meta31774){
return (new cljs.core.async.t_cljs$core$async31773(flag__$1,meta31774));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async31773(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t_cljs$core$async31776 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31776 = (function (flag,cb,meta31777){
this.flag = flag;
this.cb = cb;
this.meta31777 = meta31777;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async31776.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31778,meta31777__$1){
var self__ = this;
var _31778__$1 = this;
return (new cljs.core.async.t_cljs$core$async31776(self__.flag,self__.cb,meta31777__$1));
});

cljs.core.async.t_cljs$core$async31776.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31778){
var self__ = this;
var _31778__$1 = this;
return self__.meta31777;
});

cljs.core.async.t_cljs$core$async31776.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31776.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async31776.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async31776.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async31776.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta31777","meta31777",-1364483714,null)], null);
});

cljs.core.async.t_cljs$core$async31776.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async31776.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31776";

cljs.core.async.t_cljs$core$async31776.cljs$lang$ctorPrWriter = (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async31776");
});

cljs.core.async.__GT_t_cljs$core$async31776 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async31776(flag__$1,cb__$1,meta31777){
return (new cljs.core.async.t_cljs$core$async31776(flag__$1,cb__$1,meta31777));
});

}

return (new cljs.core.async.t_cljs$core$async31776(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__31779_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__31779_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__31780_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__31780_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__27969__auto__ = wport;
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
return port;
}
})()], null));
} else {
var G__31781 = (i + (1));
i = G__31781;
continue;
}
} else {
return null;
}
break;
}
})();
var or__27969__auto__ = ret;
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__4657__auto__ = (function (){var and__27957__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__27957__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__27957__auto__;
}
})();
if(cljs.core.truth_(temp__4657__auto__)){
var got = temp__4657__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__29140__auto__ = [];
var len__29133__auto___31787 = arguments.length;
var i__29134__auto___31788 = (0);
while(true){
if((i__29134__auto___31788 < len__29133__auto___31787)){
args__29140__auto__.push((arguments[i__29134__auto___31788]));

var G__31789 = (i__29134__auto___31788 + (1));
i__29134__auto___31788 = G__31789;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((1) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__29141__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__31784){
var map__31785 = p__31784;
var map__31785__$1 = ((((!((map__31785 == null)))?((((map__31785.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31785.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__31785):map__31785);
var opts = map__31785__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq31782){
var G__31783 = cljs.core.first.call(null,seq31782);
var seq31782__$1 = cljs.core.next.call(null,seq31782);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__31783,seq31782__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__31791 = arguments.length;
switch (G__31791) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__31704__auto___31837 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___31837){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___31837){
return (function (state_31815){
var state_val_31816 = (state_31815[(1)]);
if((state_val_31816 === (7))){
var inst_31811 = (state_31815[(2)]);
var state_31815__$1 = state_31815;
var statearr_31817_31838 = state_31815__$1;
(statearr_31817_31838[(2)] = inst_31811);

(statearr_31817_31838[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (1))){
var state_31815__$1 = state_31815;
var statearr_31818_31839 = state_31815__$1;
(statearr_31818_31839[(2)] = null);

(statearr_31818_31839[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (4))){
var inst_31794 = (state_31815[(7)]);
var inst_31794__$1 = (state_31815[(2)]);
var inst_31795 = (inst_31794__$1 == null);
var state_31815__$1 = (function (){var statearr_31819 = state_31815;
(statearr_31819[(7)] = inst_31794__$1);

return statearr_31819;
})();
if(cljs.core.truth_(inst_31795)){
var statearr_31820_31840 = state_31815__$1;
(statearr_31820_31840[(1)] = (5));

} else {
var statearr_31821_31841 = state_31815__$1;
(statearr_31821_31841[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (13))){
var state_31815__$1 = state_31815;
var statearr_31822_31842 = state_31815__$1;
(statearr_31822_31842[(2)] = null);

(statearr_31822_31842[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (6))){
var inst_31794 = (state_31815[(7)]);
var state_31815__$1 = state_31815;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31815__$1,(11),to,inst_31794);
} else {
if((state_val_31816 === (3))){
var inst_31813 = (state_31815[(2)]);
var state_31815__$1 = state_31815;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31815__$1,inst_31813);
} else {
if((state_val_31816 === (12))){
var state_31815__$1 = state_31815;
var statearr_31823_31843 = state_31815__$1;
(statearr_31823_31843[(2)] = null);

(statearr_31823_31843[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (2))){
var state_31815__$1 = state_31815;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31815__$1,(4),from);
} else {
if((state_val_31816 === (11))){
var inst_31804 = (state_31815[(2)]);
var state_31815__$1 = state_31815;
if(cljs.core.truth_(inst_31804)){
var statearr_31824_31844 = state_31815__$1;
(statearr_31824_31844[(1)] = (12));

} else {
var statearr_31825_31845 = state_31815__$1;
(statearr_31825_31845[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (9))){
var state_31815__$1 = state_31815;
var statearr_31826_31846 = state_31815__$1;
(statearr_31826_31846[(2)] = null);

(statearr_31826_31846[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (5))){
var state_31815__$1 = state_31815;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31827_31847 = state_31815__$1;
(statearr_31827_31847[(1)] = (8));

} else {
var statearr_31828_31848 = state_31815__$1;
(statearr_31828_31848[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (14))){
var inst_31809 = (state_31815[(2)]);
var state_31815__$1 = state_31815;
var statearr_31829_31849 = state_31815__$1;
(statearr_31829_31849[(2)] = inst_31809);

(statearr_31829_31849[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (10))){
var inst_31801 = (state_31815[(2)]);
var state_31815__$1 = state_31815;
var statearr_31830_31850 = state_31815__$1;
(statearr_31830_31850[(2)] = inst_31801);

(statearr_31830_31850[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31816 === (8))){
var inst_31798 = cljs.core.async.close_BANG_.call(null,to);
var state_31815__$1 = state_31815;
var statearr_31831_31851 = state_31815__$1;
(statearr_31831_31851[(2)] = inst_31798);

(statearr_31831_31851[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___31837))
;
return ((function (switch__31616__auto__,c__31704__auto___31837){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_31832 = [null,null,null,null,null,null,null,null];
(statearr_31832[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_31832[(1)] = (1));

return statearr_31832;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_31815){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_31815);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e31833){if((e31833 instanceof Object)){
var ex__31620__auto__ = e31833;
var statearr_31834_31852 = state_31815;
(statearr_31834_31852[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31815);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31833;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31853 = state_31815;
state_31815 = G__31853;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_31815){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_31815);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___31837))
})();
var state__31706__auto__ = (function (){var statearr_31835 = f__31705__auto__.call(null);
(statearr_31835[(6)] = c__31704__auto___31837);

return statearr_31835;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___31837))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__31854){
var vec__31855 = p__31854;
var v = cljs.core.nth.call(null,vec__31855,(0),null);
var p = cljs.core.nth.call(null,vec__31855,(1),null);
var job = vec__31855;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__31704__auto___32026 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___32026,res,vec__31855,v,p,job,jobs,results){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___32026,res,vec__31855,v,p,job,jobs,results){
return (function (state_31862){
var state_val_31863 = (state_31862[(1)]);
if((state_val_31863 === (1))){
var state_31862__$1 = state_31862;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31862__$1,(2),res,v);
} else {
if((state_val_31863 === (2))){
var inst_31859 = (state_31862[(2)]);
var inst_31860 = cljs.core.async.close_BANG_.call(null,res);
var state_31862__$1 = (function (){var statearr_31864 = state_31862;
(statearr_31864[(7)] = inst_31859);

return statearr_31864;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31862__$1,inst_31860);
} else {
return null;
}
}
});})(c__31704__auto___32026,res,vec__31855,v,p,job,jobs,results))
;
return ((function (switch__31616__auto__,c__31704__auto___32026,res,vec__31855,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0 = (function (){
var statearr_31865 = [null,null,null,null,null,null,null,null];
(statearr_31865[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__);

(statearr_31865[(1)] = (1));

return statearr_31865;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1 = (function (state_31862){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_31862);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e31866){if((e31866 instanceof Object)){
var ex__31620__auto__ = e31866;
var statearr_31867_32027 = state_31862;
(statearr_31867_32027[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31862);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31866;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32028 = state_31862;
state_31862 = G__32028;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = function(state_31862){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1.call(this,state_31862);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___32026,res,vec__31855,v,p,job,jobs,results))
})();
var state__31706__auto__ = (function (){var statearr_31868 = f__31705__auto__.call(null);
(statearr_31868[(6)] = c__31704__auto___32026);

return statearr_31868;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___32026,res,vec__31855,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__31869){
var vec__31870 = p__31869;
var v = cljs.core.nth.call(null,vec__31870,(0),null);
var p = cljs.core.nth.call(null,vec__31870,(1),null);
var job = vec__31870;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__28909__auto___32029 = n;
var __32030 = (0);
while(true){
if((__32030 < n__28909__auto___32029)){
var G__31873_32031 = type;
var G__31873_32032__$1 = (((G__31873_32031 instanceof cljs.core.Keyword))?G__31873_32031.fqn:null);
switch (G__31873_32032__$1) {
case "compute":
var c__31704__auto___32034 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__32030,c__31704__auto___32034,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (__32030,c__31704__auto___32034,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async){
return (function (state_31886){
var state_val_31887 = (state_31886[(1)]);
if((state_val_31887 === (1))){
var state_31886__$1 = state_31886;
var statearr_31888_32035 = state_31886__$1;
(statearr_31888_32035[(2)] = null);

(statearr_31888_32035[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31887 === (2))){
var state_31886__$1 = state_31886;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31886__$1,(4),jobs);
} else {
if((state_val_31887 === (3))){
var inst_31884 = (state_31886[(2)]);
var state_31886__$1 = state_31886;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31886__$1,inst_31884);
} else {
if((state_val_31887 === (4))){
var inst_31876 = (state_31886[(2)]);
var inst_31877 = process.call(null,inst_31876);
var state_31886__$1 = state_31886;
if(cljs.core.truth_(inst_31877)){
var statearr_31889_32036 = state_31886__$1;
(statearr_31889_32036[(1)] = (5));

} else {
var statearr_31890_32037 = state_31886__$1;
(statearr_31890_32037[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31887 === (5))){
var state_31886__$1 = state_31886;
var statearr_31891_32038 = state_31886__$1;
(statearr_31891_32038[(2)] = null);

(statearr_31891_32038[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31887 === (6))){
var state_31886__$1 = state_31886;
var statearr_31892_32039 = state_31886__$1;
(statearr_31892_32039[(2)] = null);

(statearr_31892_32039[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31887 === (7))){
var inst_31882 = (state_31886[(2)]);
var state_31886__$1 = state_31886;
var statearr_31893_32040 = state_31886__$1;
(statearr_31893_32040[(2)] = inst_31882);

(statearr_31893_32040[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__32030,c__31704__auto___32034,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async))
;
return ((function (__32030,switch__31616__auto__,c__31704__auto___32034,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0 = (function (){
var statearr_31894 = [null,null,null,null,null,null,null];
(statearr_31894[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__);

(statearr_31894[(1)] = (1));

return statearr_31894;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1 = (function (state_31886){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_31886);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e31895){if((e31895 instanceof Object)){
var ex__31620__auto__ = e31895;
var statearr_31896_32041 = state_31886;
(statearr_31896_32041[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31886);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31895;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32042 = state_31886;
state_31886 = G__32042;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = function(state_31886){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1.call(this,state_31886);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__;
})()
;})(__32030,switch__31616__auto__,c__31704__auto___32034,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async))
})();
var state__31706__auto__ = (function (){var statearr_31897 = f__31705__auto__.call(null);
(statearr_31897[(6)] = c__31704__auto___32034);

return statearr_31897;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(__32030,c__31704__auto___32034,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async))
);


break;
case "async":
var c__31704__auto___32043 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__32030,c__31704__auto___32043,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (__32030,c__31704__auto___32043,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async){
return (function (state_31910){
var state_val_31911 = (state_31910[(1)]);
if((state_val_31911 === (1))){
var state_31910__$1 = state_31910;
var statearr_31912_32044 = state_31910__$1;
(statearr_31912_32044[(2)] = null);

(statearr_31912_32044[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31911 === (2))){
var state_31910__$1 = state_31910;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31910__$1,(4),jobs);
} else {
if((state_val_31911 === (3))){
var inst_31908 = (state_31910[(2)]);
var state_31910__$1 = state_31910;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31910__$1,inst_31908);
} else {
if((state_val_31911 === (4))){
var inst_31900 = (state_31910[(2)]);
var inst_31901 = async.call(null,inst_31900);
var state_31910__$1 = state_31910;
if(cljs.core.truth_(inst_31901)){
var statearr_31913_32045 = state_31910__$1;
(statearr_31913_32045[(1)] = (5));

} else {
var statearr_31914_32046 = state_31910__$1;
(statearr_31914_32046[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31911 === (5))){
var state_31910__$1 = state_31910;
var statearr_31915_32047 = state_31910__$1;
(statearr_31915_32047[(2)] = null);

(statearr_31915_32047[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31911 === (6))){
var state_31910__$1 = state_31910;
var statearr_31916_32048 = state_31910__$1;
(statearr_31916_32048[(2)] = null);

(statearr_31916_32048[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31911 === (7))){
var inst_31906 = (state_31910[(2)]);
var state_31910__$1 = state_31910;
var statearr_31917_32049 = state_31910__$1;
(statearr_31917_32049[(2)] = inst_31906);

(statearr_31917_32049[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__32030,c__31704__auto___32043,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async))
;
return ((function (__32030,switch__31616__auto__,c__31704__auto___32043,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0 = (function (){
var statearr_31918 = [null,null,null,null,null,null,null];
(statearr_31918[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__);

(statearr_31918[(1)] = (1));

return statearr_31918;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1 = (function (state_31910){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_31910);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e31919){if((e31919 instanceof Object)){
var ex__31620__auto__ = e31919;
var statearr_31920_32050 = state_31910;
(statearr_31920_32050[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31910);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31919;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32051 = state_31910;
state_31910 = G__32051;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = function(state_31910){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1.call(this,state_31910);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__;
})()
;})(__32030,switch__31616__auto__,c__31704__auto___32043,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async))
})();
var state__31706__auto__ = (function (){var statearr_31921 = f__31705__auto__.call(null);
(statearr_31921[(6)] = c__31704__auto___32043);

return statearr_31921;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(__32030,c__31704__auto___32043,G__31873_32031,G__31873_32032__$1,n__28909__auto___32029,jobs,results,process,async))
);


break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("No matching clause: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__31873_32032__$1)].join('')));

}

var G__32052 = (__32030 + (1));
__32030 = G__32052;
continue;
} else {
}
break;
}

var c__31704__auto___32053 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___32053,jobs,results,process,async){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___32053,jobs,results,process,async){
return (function (state_31943){
var state_val_31944 = (state_31943[(1)]);
if((state_val_31944 === (1))){
var state_31943__$1 = state_31943;
var statearr_31945_32054 = state_31943__$1;
(statearr_31945_32054[(2)] = null);

(statearr_31945_32054[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31944 === (2))){
var state_31943__$1 = state_31943;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31943__$1,(4),from);
} else {
if((state_val_31944 === (3))){
var inst_31941 = (state_31943[(2)]);
var state_31943__$1 = state_31943;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31943__$1,inst_31941);
} else {
if((state_val_31944 === (4))){
var inst_31924 = (state_31943[(7)]);
var inst_31924__$1 = (state_31943[(2)]);
var inst_31925 = (inst_31924__$1 == null);
var state_31943__$1 = (function (){var statearr_31946 = state_31943;
(statearr_31946[(7)] = inst_31924__$1);

return statearr_31946;
})();
if(cljs.core.truth_(inst_31925)){
var statearr_31947_32055 = state_31943__$1;
(statearr_31947_32055[(1)] = (5));

} else {
var statearr_31948_32056 = state_31943__$1;
(statearr_31948_32056[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31944 === (5))){
var inst_31927 = cljs.core.async.close_BANG_.call(null,jobs);
var state_31943__$1 = state_31943;
var statearr_31949_32057 = state_31943__$1;
(statearr_31949_32057[(2)] = inst_31927);

(statearr_31949_32057[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31944 === (6))){
var inst_31929 = (state_31943[(8)]);
var inst_31924 = (state_31943[(7)]);
var inst_31929__$1 = cljs.core.async.chan.call(null,(1));
var inst_31930 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_31931 = [inst_31924,inst_31929__$1];
var inst_31932 = (new cljs.core.PersistentVector(null,2,(5),inst_31930,inst_31931,null));
var state_31943__$1 = (function (){var statearr_31950 = state_31943;
(statearr_31950[(8)] = inst_31929__$1);

return statearr_31950;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31943__$1,(8),jobs,inst_31932);
} else {
if((state_val_31944 === (7))){
var inst_31939 = (state_31943[(2)]);
var state_31943__$1 = state_31943;
var statearr_31951_32058 = state_31943__$1;
(statearr_31951_32058[(2)] = inst_31939);

(statearr_31951_32058[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31944 === (8))){
var inst_31929 = (state_31943[(8)]);
var inst_31934 = (state_31943[(2)]);
var state_31943__$1 = (function (){var statearr_31952 = state_31943;
(statearr_31952[(9)] = inst_31934);

return statearr_31952;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31943__$1,(9),results,inst_31929);
} else {
if((state_val_31944 === (9))){
var inst_31936 = (state_31943[(2)]);
var state_31943__$1 = (function (){var statearr_31953 = state_31943;
(statearr_31953[(10)] = inst_31936);

return statearr_31953;
})();
var statearr_31954_32059 = state_31943__$1;
(statearr_31954_32059[(2)] = null);

(statearr_31954_32059[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___32053,jobs,results,process,async))
;
return ((function (switch__31616__auto__,c__31704__auto___32053,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0 = (function (){
var statearr_31955 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31955[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__);

(statearr_31955[(1)] = (1));

return statearr_31955;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1 = (function (state_31943){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_31943);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e31956){if((e31956 instanceof Object)){
var ex__31620__auto__ = e31956;
var statearr_31957_32060 = state_31943;
(statearr_31957_32060[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31943);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31956;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32061 = state_31943;
state_31943 = G__32061;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = function(state_31943){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1.call(this,state_31943);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___32053,jobs,results,process,async))
})();
var state__31706__auto__ = (function (){var statearr_31958 = f__31705__auto__.call(null);
(statearr_31958[(6)] = c__31704__auto___32053);

return statearr_31958;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___32053,jobs,results,process,async))
);


var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__,jobs,results,process,async){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__,jobs,results,process,async){
return (function (state_31996){
var state_val_31997 = (state_31996[(1)]);
if((state_val_31997 === (7))){
var inst_31992 = (state_31996[(2)]);
var state_31996__$1 = state_31996;
var statearr_31998_32062 = state_31996__$1;
(statearr_31998_32062[(2)] = inst_31992);

(statearr_31998_32062[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (20))){
var state_31996__$1 = state_31996;
var statearr_31999_32063 = state_31996__$1;
(statearr_31999_32063[(2)] = null);

(statearr_31999_32063[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (1))){
var state_31996__$1 = state_31996;
var statearr_32000_32064 = state_31996__$1;
(statearr_32000_32064[(2)] = null);

(statearr_32000_32064[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (4))){
var inst_31961 = (state_31996[(7)]);
var inst_31961__$1 = (state_31996[(2)]);
var inst_31962 = (inst_31961__$1 == null);
var state_31996__$1 = (function (){var statearr_32001 = state_31996;
(statearr_32001[(7)] = inst_31961__$1);

return statearr_32001;
})();
if(cljs.core.truth_(inst_31962)){
var statearr_32002_32065 = state_31996__$1;
(statearr_32002_32065[(1)] = (5));

} else {
var statearr_32003_32066 = state_31996__$1;
(statearr_32003_32066[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (15))){
var inst_31974 = (state_31996[(8)]);
var state_31996__$1 = state_31996;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31996__$1,(18),to,inst_31974);
} else {
if((state_val_31997 === (21))){
var inst_31987 = (state_31996[(2)]);
var state_31996__$1 = state_31996;
var statearr_32004_32067 = state_31996__$1;
(statearr_32004_32067[(2)] = inst_31987);

(statearr_32004_32067[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (13))){
var inst_31989 = (state_31996[(2)]);
var state_31996__$1 = (function (){var statearr_32005 = state_31996;
(statearr_32005[(9)] = inst_31989);

return statearr_32005;
})();
var statearr_32006_32068 = state_31996__$1;
(statearr_32006_32068[(2)] = null);

(statearr_32006_32068[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (6))){
var inst_31961 = (state_31996[(7)]);
var state_31996__$1 = state_31996;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31996__$1,(11),inst_31961);
} else {
if((state_val_31997 === (17))){
var inst_31982 = (state_31996[(2)]);
var state_31996__$1 = state_31996;
if(cljs.core.truth_(inst_31982)){
var statearr_32007_32069 = state_31996__$1;
(statearr_32007_32069[(1)] = (19));

} else {
var statearr_32008_32070 = state_31996__$1;
(statearr_32008_32070[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (3))){
var inst_31994 = (state_31996[(2)]);
var state_31996__$1 = state_31996;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31996__$1,inst_31994);
} else {
if((state_val_31997 === (12))){
var inst_31971 = (state_31996[(10)]);
var state_31996__$1 = state_31996;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31996__$1,(14),inst_31971);
} else {
if((state_val_31997 === (2))){
var state_31996__$1 = state_31996;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31996__$1,(4),results);
} else {
if((state_val_31997 === (19))){
var state_31996__$1 = state_31996;
var statearr_32009_32071 = state_31996__$1;
(statearr_32009_32071[(2)] = null);

(statearr_32009_32071[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (11))){
var inst_31971 = (state_31996[(2)]);
var state_31996__$1 = (function (){var statearr_32010 = state_31996;
(statearr_32010[(10)] = inst_31971);

return statearr_32010;
})();
var statearr_32011_32072 = state_31996__$1;
(statearr_32011_32072[(2)] = null);

(statearr_32011_32072[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (9))){
var state_31996__$1 = state_31996;
var statearr_32012_32073 = state_31996__$1;
(statearr_32012_32073[(2)] = null);

(statearr_32012_32073[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (5))){
var state_31996__$1 = state_31996;
if(cljs.core.truth_(close_QMARK_)){
var statearr_32013_32074 = state_31996__$1;
(statearr_32013_32074[(1)] = (8));

} else {
var statearr_32014_32075 = state_31996__$1;
(statearr_32014_32075[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (14))){
var inst_31976 = (state_31996[(11)]);
var inst_31974 = (state_31996[(8)]);
var inst_31974__$1 = (state_31996[(2)]);
var inst_31975 = (inst_31974__$1 == null);
var inst_31976__$1 = cljs.core.not.call(null,inst_31975);
var state_31996__$1 = (function (){var statearr_32015 = state_31996;
(statearr_32015[(11)] = inst_31976__$1);

(statearr_32015[(8)] = inst_31974__$1);

return statearr_32015;
})();
if(inst_31976__$1){
var statearr_32016_32076 = state_31996__$1;
(statearr_32016_32076[(1)] = (15));

} else {
var statearr_32017_32077 = state_31996__$1;
(statearr_32017_32077[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (16))){
var inst_31976 = (state_31996[(11)]);
var state_31996__$1 = state_31996;
var statearr_32018_32078 = state_31996__$1;
(statearr_32018_32078[(2)] = inst_31976);

(statearr_32018_32078[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (10))){
var inst_31968 = (state_31996[(2)]);
var state_31996__$1 = state_31996;
var statearr_32019_32079 = state_31996__$1;
(statearr_32019_32079[(2)] = inst_31968);

(statearr_32019_32079[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (18))){
var inst_31979 = (state_31996[(2)]);
var state_31996__$1 = state_31996;
var statearr_32020_32080 = state_31996__$1;
(statearr_32020_32080[(2)] = inst_31979);

(statearr_32020_32080[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31997 === (8))){
var inst_31965 = cljs.core.async.close_BANG_.call(null,to);
var state_31996__$1 = state_31996;
var statearr_32021_32081 = state_31996__$1;
(statearr_32021_32081[(2)] = inst_31965);

(statearr_32021_32081[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto__,jobs,results,process,async))
;
return ((function (switch__31616__auto__,c__31704__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0 = (function (){
var statearr_32022 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32022[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__);

(statearr_32022[(1)] = (1));

return statearr_32022;
});
var cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1 = (function (state_31996){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_31996);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32023){if((e32023 instanceof Object)){
var ex__31620__auto__ = e32023;
var statearr_32024_32082 = state_31996;
(statearr_32024_32082[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31996);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32023;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32083 = state_31996;
state_31996 = G__32083;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__ = function(state_31996){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1.call(this,state_31996);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__31617__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__,jobs,results,process,async))
})();
var state__31706__auto__ = (function (){var statearr_32025 = f__31705__auto__.call(null);
(statearr_32025[(6)] = c__31704__auto__);

return statearr_32025;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__,jobs,results,process,async))
);

return c__31704__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__32085 = arguments.length;
switch (G__32085) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__32088 = arguments.length;
switch (G__32088) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__32091 = arguments.length;
switch (G__32091) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__31704__auto___32140 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___32140,tc,fc){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___32140,tc,fc){
return (function (state_32117){
var state_val_32118 = (state_32117[(1)]);
if((state_val_32118 === (7))){
var inst_32113 = (state_32117[(2)]);
var state_32117__$1 = state_32117;
var statearr_32119_32141 = state_32117__$1;
(statearr_32119_32141[(2)] = inst_32113);

(statearr_32119_32141[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (1))){
var state_32117__$1 = state_32117;
var statearr_32120_32142 = state_32117__$1;
(statearr_32120_32142[(2)] = null);

(statearr_32120_32142[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (4))){
var inst_32094 = (state_32117[(7)]);
var inst_32094__$1 = (state_32117[(2)]);
var inst_32095 = (inst_32094__$1 == null);
var state_32117__$1 = (function (){var statearr_32121 = state_32117;
(statearr_32121[(7)] = inst_32094__$1);

return statearr_32121;
})();
if(cljs.core.truth_(inst_32095)){
var statearr_32122_32143 = state_32117__$1;
(statearr_32122_32143[(1)] = (5));

} else {
var statearr_32123_32144 = state_32117__$1;
(statearr_32123_32144[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (13))){
var state_32117__$1 = state_32117;
var statearr_32124_32145 = state_32117__$1;
(statearr_32124_32145[(2)] = null);

(statearr_32124_32145[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (6))){
var inst_32094 = (state_32117[(7)]);
var inst_32100 = p.call(null,inst_32094);
var state_32117__$1 = state_32117;
if(cljs.core.truth_(inst_32100)){
var statearr_32125_32146 = state_32117__$1;
(statearr_32125_32146[(1)] = (9));

} else {
var statearr_32126_32147 = state_32117__$1;
(statearr_32126_32147[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (3))){
var inst_32115 = (state_32117[(2)]);
var state_32117__$1 = state_32117;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32117__$1,inst_32115);
} else {
if((state_val_32118 === (12))){
var state_32117__$1 = state_32117;
var statearr_32127_32148 = state_32117__$1;
(statearr_32127_32148[(2)] = null);

(statearr_32127_32148[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (2))){
var state_32117__$1 = state_32117;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32117__$1,(4),ch);
} else {
if((state_val_32118 === (11))){
var inst_32094 = (state_32117[(7)]);
var inst_32104 = (state_32117[(2)]);
var state_32117__$1 = state_32117;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32117__$1,(8),inst_32104,inst_32094);
} else {
if((state_val_32118 === (9))){
var state_32117__$1 = state_32117;
var statearr_32128_32149 = state_32117__$1;
(statearr_32128_32149[(2)] = tc);

(statearr_32128_32149[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (5))){
var inst_32097 = cljs.core.async.close_BANG_.call(null,tc);
var inst_32098 = cljs.core.async.close_BANG_.call(null,fc);
var state_32117__$1 = (function (){var statearr_32129 = state_32117;
(statearr_32129[(8)] = inst_32097);

return statearr_32129;
})();
var statearr_32130_32150 = state_32117__$1;
(statearr_32130_32150[(2)] = inst_32098);

(statearr_32130_32150[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (14))){
var inst_32111 = (state_32117[(2)]);
var state_32117__$1 = state_32117;
var statearr_32131_32151 = state_32117__$1;
(statearr_32131_32151[(2)] = inst_32111);

(statearr_32131_32151[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (10))){
var state_32117__$1 = state_32117;
var statearr_32132_32152 = state_32117__$1;
(statearr_32132_32152[(2)] = fc);

(statearr_32132_32152[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32118 === (8))){
var inst_32106 = (state_32117[(2)]);
var state_32117__$1 = state_32117;
if(cljs.core.truth_(inst_32106)){
var statearr_32133_32153 = state_32117__$1;
(statearr_32133_32153[(1)] = (12));

} else {
var statearr_32134_32154 = state_32117__$1;
(statearr_32134_32154[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___32140,tc,fc))
;
return ((function (switch__31616__auto__,c__31704__auto___32140,tc,fc){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_32135 = [null,null,null,null,null,null,null,null,null];
(statearr_32135[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_32135[(1)] = (1));

return statearr_32135;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_32117){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_32117);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32136){if((e32136 instanceof Object)){
var ex__31620__auto__ = e32136;
var statearr_32137_32155 = state_32117;
(statearr_32137_32155[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32117);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32136;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32156 = state_32117;
state_32117 = G__32156;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_32117){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_32117);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___32140,tc,fc))
})();
var state__31706__auto__ = (function (){var statearr_32138 = f__31705__auto__.call(null);
(statearr_32138[(6)] = c__31704__auto___32140);

return statearr_32138;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___32140,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__){
return (function (state_32177){
var state_val_32178 = (state_32177[(1)]);
if((state_val_32178 === (7))){
var inst_32173 = (state_32177[(2)]);
var state_32177__$1 = state_32177;
var statearr_32179_32197 = state_32177__$1;
(statearr_32179_32197[(2)] = inst_32173);

(statearr_32179_32197[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32178 === (1))){
var inst_32157 = init;
var state_32177__$1 = (function (){var statearr_32180 = state_32177;
(statearr_32180[(7)] = inst_32157);

return statearr_32180;
})();
var statearr_32181_32198 = state_32177__$1;
(statearr_32181_32198[(2)] = null);

(statearr_32181_32198[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32178 === (4))){
var inst_32160 = (state_32177[(8)]);
var inst_32160__$1 = (state_32177[(2)]);
var inst_32161 = (inst_32160__$1 == null);
var state_32177__$1 = (function (){var statearr_32182 = state_32177;
(statearr_32182[(8)] = inst_32160__$1);

return statearr_32182;
})();
if(cljs.core.truth_(inst_32161)){
var statearr_32183_32199 = state_32177__$1;
(statearr_32183_32199[(1)] = (5));

} else {
var statearr_32184_32200 = state_32177__$1;
(statearr_32184_32200[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32178 === (6))){
var inst_32160 = (state_32177[(8)]);
var inst_32164 = (state_32177[(9)]);
var inst_32157 = (state_32177[(7)]);
var inst_32164__$1 = f.call(null,inst_32157,inst_32160);
var inst_32165 = cljs.core.reduced_QMARK_.call(null,inst_32164__$1);
var state_32177__$1 = (function (){var statearr_32185 = state_32177;
(statearr_32185[(9)] = inst_32164__$1);

return statearr_32185;
})();
if(inst_32165){
var statearr_32186_32201 = state_32177__$1;
(statearr_32186_32201[(1)] = (8));

} else {
var statearr_32187_32202 = state_32177__$1;
(statearr_32187_32202[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32178 === (3))){
var inst_32175 = (state_32177[(2)]);
var state_32177__$1 = state_32177;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32177__$1,inst_32175);
} else {
if((state_val_32178 === (2))){
var state_32177__$1 = state_32177;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32177__$1,(4),ch);
} else {
if((state_val_32178 === (9))){
var inst_32164 = (state_32177[(9)]);
var inst_32157 = inst_32164;
var state_32177__$1 = (function (){var statearr_32188 = state_32177;
(statearr_32188[(7)] = inst_32157);

return statearr_32188;
})();
var statearr_32189_32203 = state_32177__$1;
(statearr_32189_32203[(2)] = null);

(statearr_32189_32203[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32178 === (5))){
var inst_32157 = (state_32177[(7)]);
var state_32177__$1 = state_32177;
var statearr_32190_32204 = state_32177__$1;
(statearr_32190_32204[(2)] = inst_32157);

(statearr_32190_32204[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32178 === (10))){
var inst_32171 = (state_32177[(2)]);
var state_32177__$1 = state_32177;
var statearr_32191_32205 = state_32177__$1;
(statearr_32191_32205[(2)] = inst_32171);

(statearr_32191_32205[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32178 === (8))){
var inst_32164 = (state_32177[(9)]);
var inst_32167 = cljs.core.deref.call(null,inst_32164);
var state_32177__$1 = state_32177;
var statearr_32192_32206 = state_32177__$1;
(statearr_32192_32206[(2)] = inst_32167);

(statearr_32192_32206[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto__))
;
return ((function (switch__31616__auto__,c__31704__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__31617__auto__ = null;
var cljs$core$async$reduce_$_state_machine__31617__auto____0 = (function (){
var statearr_32193 = [null,null,null,null,null,null,null,null,null,null];
(statearr_32193[(0)] = cljs$core$async$reduce_$_state_machine__31617__auto__);

(statearr_32193[(1)] = (1));

return statearr_32193;
});
var cljs$core$async$reduce_$_state_machine__31617__auto____1 = (function (state_32177){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_32177);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32194){if((e32194 instanceof Object)){
var ex__31620__auto__ = e32194;
var statearr_32195_32207 = state_32177;
(statearr_32195_32207[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32177);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32194;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32208 = state_32177;
state_32177 = G__32208;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__31617__auto__ = function(state_32177){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__31617__auto____1.call(this,state_32177);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$reduce_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__31617__auto____0;
cljs$core$async$reduce_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__31617__auto____1;
return cljs$core$async$reduce_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__))
})();
var state__31706__auto__ = (function (){var statearr_32196 = f__31705__auto__.call(null);
(statearr_32196[(6)] = c__31704__auto__);

return statearr_32196;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__))
);

return c__31704__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = xform.call(null,f);
var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__,f__$1){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__,f__$1){
return (function (state_32214){
var state_val_32215 = (state_32214[(1)]);
if((state_val_32215 === (1))){
var inst_32209 = cljs.core.async.reduce.call(null,f__$1,init,ch);
var state_32214__$1 = state_32214;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32214__$1,(2),inst_32209);
} else {
if((state_val_32215 === (2))){
var inst_32211 = (state_32214[(2)]);
var inst_32212 = f__$1.call(null,inst_32211);
var state_32214__$1 = state_32214;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32214__$1,inst_32212);
} else {
return null;
}
}
});})(c__31704__auto__,f__$1))
;
return ((function (switch__31616__auto__,c__31704__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__31617__auto__ = null;
var cljs$core$async$transduce_$_state_machine__31617__auto____0 = (function (){
var statearr_32216 = [null,null,null,null,null,null,null];
(statearr_32216[(0)] = cljs$core$async$transduce_$_state_machine__31617__auto__);

(statearr_32216[(1)] = (1));

return statearr_32216;
});
var cljs$core$async$transduce_$_state_machine__31617__auto____1 = (function (state_32214){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_32214);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32217){if((e32217 instanceof Object)){
var ex__31620__auto__ = e32217;
var statearr_32218_32220 = state_32214;
(statearr_32218_32220[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32214);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32217;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32221 = state_32214;
state_32214 = G__32221;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__31617__auto__ = function(state_32214){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__31617__auto____1.call(this,state_32214);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$transduce_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__31617__auto____0;
cljs$core$async$transduce_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__31617__auto____1;
return cljs$core$async$transduce_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__,f__$1))
})();
var state__31706__auto__ = (function (){var statearr_32219 = f__31705__auto__.call(null);
(statearr_32219[(6)] = c__31704__auto__);

return statearr_32219;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__,f__$1))
);

return c__31704__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__32223 = arguments.length;
switch (G__32223) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__){
return (function (state_32248){
var state_val_32249 = (state_32248[(1)]);
if((state_val_32249 === (7))){
var inst_32230 = (state_32248[(2)]);
var state_32248__$1 = state_32248;
var statearr_32250_32271 = state_32248__$1;
(statearr_32250_32271[(2)] = inst_32230);

(statearr_32250_32271[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (1))){
var inst_32224 = cljs.core.seq.call(null,coll);
var inst_32225 = inst_32224;
var state_32248__$1 = (function (){var statearr_32251 = state_32248;
(statearr_32251[(7)] = inst_32225);

return statearr_32251;
})();
var statearr_32252_32272 = state_32248__$1;
(statearr_32252_32272[(2)] = null);

(statearr_32252_32272[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (4))){
var inst_32225 = (state_32248[(7)]);
var inst_32228 = cljs.core.first.call(null,inst_32225);
var state_32248__$1 = state_32248;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32248__$1,(7),ch,inst_32228);
} else {
if((state_val_32249 === (13))){
var inst_32242 = (state_32248[(2)]);
var state_32248__$1 = state_32248;
var statearr_32253_32273 = state_32248__$1;
(statearr_32253_32273[(2)] = inst_32242);

(statearr_32253_32273[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (6))){
var inst_32233 = (state_32248[(2)]);
var state_32248__$1 = state_32248;
if(cljs.core.truth_(inst_32233)){
var statearr_32254_32274 = state_32248__$1;
(statearr_32254_32274[(1)] = (8));

} else {
var statearr_32255_32275 = state_32248__$1;
(statearr_32255_32275[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (3))){
var inst_32246 = (state_32248[(2)]);
var state_32248__$1 = state_32248;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32248__$1,inst_32246);
} else {
if((state_val_32249 === (12))){
var state_32248__$1 = state_32248;
var statearr_32256_32276 = state_32248__$1;
(statearr_32256_32276[(2)] = null);

(statearr_32256_32276[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (2))){
var inst_32225 = (state_32248[(7)]);
var state_32248__$1 = state_32248;
if(cljs.core.truth_(inst_32225)){
var statearr_32257_32277 = state_32248__$1;
(statearr_32257_32277[(1)] = (4));

} else {
var statearr_32258_32278 = state_32248__$1;
(statearr_32258_32278[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (11))){
var inst_32239 = cljs.core.async.close_BANG_.call(null,ch);
var state_32248__$1 = state_32248;
var statearr_32259_32279 = state_32248__$1;
(statearr_32259_32279[(2)] = inst_32239);

(statearr_32259_32279[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (9))){
var state_32248__$1 = state_32248;
if(cljs.core.truth_(close_QMARK_)){
var statearr_32260_32280 = state_32248__$1;
(statearr_32260_32280[(1)] = (11));

} else {
var statearr_32261_32281 = state_32248__$1;
(statearr_32261_32281[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (5))){
var inst_32225 = (state_32248[(7)]);
var state_32248__$1 = state_32248;
var statearr_32262_32282 = state_32248__$1;
(statearr_32262_32282[(2)] = inst_32225);

(statearr_32262_32282[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (10))){
var inst_32244 = (state_32248[(2)]);
var state_32248__$1 = state_32248;
var statearr_32263_32283 = state_32248__$1;
(statearr_32263_32283[(2)] = inst_32244);

(statearr_32263_32283[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32249 === (8))){
var inst_32225 = (state_32248[(7)]);
var inst_32235 = cljs.core.next.call(null,inst_32225);
var inst_32225__$1 = inst_32235;
var state_32248__$1 = (function (){var statearr_32264 = state_32248;
(statearr_32264[(7)] = inst_32225__$1);

return statearr_32264;
})();
var statearr_32265_32284 = state_32248__$1;
(statearr_32265_32284[(2)] = null);

(statearr_32265_32284[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto__))
;
return ((function (switch__31616__auto__,c__31704__auto__){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_32266 = [null,null,null,null,null,null,null,null];
(statearr_32266[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_32266[(1)] = (1));

return statearr_32266;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_32248){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_32248);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32267){if((e32267 instanceof Object)){
var ex__31620__auto__ = e32267;
var statearr_32268_32285 = state_32248;
(statearr_32268_32285[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32248);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32267;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32286 = state_32248;
state_32248 = G__32286;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_32248){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_32248);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__))
})();
var state__31706__auto__ = (function (){var statearr_32269 = f__31705__auto__.call(null);
(statearr_32269[(6)] = c__31704__auto__);

return statearr_32269;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__))
);

return c__31704__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((!((_ == null))) && (!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__28652__auto__ = (((_ == null))?null:_);
var m__28653__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,_);
} else {
var m__28653__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((!((m == null))) && (!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__28653__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m,ch);
} else {
var m__28653__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m);
} else {
var m__28653__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async32287 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32287 = (function (ch,cs,meta32288){
this.ch = ch;
this.cs = cs;
this.meta32288 = meta32288;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_32289,meta32288__$1){
var self__ = this;
var _32289__$1 = this;
return (new cljs.core.async.t_cljs$core$async32287(self__.ch,self__.cs,meta32288__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_32289){
var self__ = this;
var _32289__$1 = this;
return self__.meta32288;
});})(cs))
;

cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async32287.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async32287.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta32288","meta32288",1948576753,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async32287.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32287.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32287";

cljs.core.async.t_cljs$core$async32287.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async32287");
});})(cs))
;

cljs.core.async.__GT_t_cljs$core$async32287 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async32287(ch__$1,cs__$1,meta32288){
return (new cljs.core.async.t_cljs$core$async32287(ch__$1,cs__$1,meta32288));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async32287(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__31704__auto___32509 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___32509,cs,m,dchan,dctr,done){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___32509,cs,m,dchan,dctr,done){
return (function (state_32424){
var state_val_32425 = (state_32424[(1)]);
if((state_val_32425 === (7))){
var inst_32420 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32426_32510 = state_32424__$1;
(statearr_32426_32510[(2)] = inst_32420);

(statearr_32426_32510[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (20))){
var inst_32323 = (state_32424[(7)]);
var inst_32335 = cljs.core.first.call(null,inst_32323);
var inst_32336 = cljs.core.nth.call(null,inst_32335,(0),null);
var inst_32337 = cljs.core.nth.call(null,inst_32335,(1),null);
var state_32424__$1 = (function (){var statearr_32427 = state_32424;
(statearr_32427[(8)] = inst_32336);

return statearr_32427;
})();
if(cljs.core.truth_(inst_32337)){
var statearr_32428_32511 = state_32424__$1;
(statearr_32428_32511[(1)] = (22));

} else {
var statearr_32429_32512 = state_32424__$1;
(statearr_32429_32512[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (27))){
var inst_32367 = (state_32424[(9)]);
var inst_32365 = (state_32424[(10)]);
var inst_32292 = (state_32424[(11)]);
var inst_32372 = (state_32424[(12)]);
var inst_32372__$1 = cljs.core._nth.call(null,inst_32365,inst_32367);
var inst_32373 = cljs.core.async.put_BANG_.call(null,inst_32372__$1,inst_32292,done);
var state_32424__$1 = (function (){var statearr_32430 = state_32424;
(statearr_32430[(12)] = inst_32372__$1);

return statearr_32430;
})();
if(cljs.core.truth_(inst_32373)){
var statearr_32431_32513 = state_32424__$1;
(statearr_32431_32513[(1)] = (30));

} else {
var statearr_32432_32514 = state_32424__$1;
(statearr_32432_32514[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (1))){
var state_32424__$1 = state_32424;
var statearr_32433_32515 = state_32424__$1;
(statearr_32433_32515[(2)] = null);

(statearr_32433_32515[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (24))){
var inst_32323 = (state_32424[(7)]);
var inst_32342 = (state_32424[(2)]);
var inst_32343 = cljs.core.next.call(null,inst_32323);
var inst_32301 = inst_32343;
var inst_32302 = null;
var inst_32303 = (0);
var inst_32304 = (0);
var state_32424__$1 = (function (){var statearr_32434 = state_32424;
(statearr_32434[(13)] = inst_32303);

(statearr_32434[(14)] = inst_32301);

(statearr_32434[(15)] = inst_32302);

(statearr_32434[(16)] = inst_32304);

(statearr_32434[(17)] = inst_32342);

return statearr_32434;
})();
var statearr_32435_32516 = state_32424__$1;
(statearr_32435_32516[(2)] = null);

(statearr_32435_32516[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (39))){
var state_32424__$1 = state_32424;
var statearr_32439_32517 = state_32424__$1;
(statearr_32439_32517[(2)] = null);

(statearr_32439_32517[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (4))){
var inst_32292 = (state_32424[(11)]);
var inst_32292__$1 = (state_32424[(2)]);
var inst_32293 = (inst_32292__$1 == null);
var state_32424__$1 = (function (){var statearr_32440 = state_32424;
(statearr_32440[(11)] = inst_32292__$1);

return statearr_32440;
})();
if(cljs.core.truth_(inst_32293)){
var statearr_32441_32518 = state_32424__$1;
(statearr_32441_32518[(1)] = (5));

} else {
var statearr_32442_32519 = state_32424__$1;
(statearr_32442_32519[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (15))){
var inst_32303 = (state_32424[(13)]);
var inst_32301 = (state_32424[(14)]);
var inst_32302 = (state_32424[(15)]);
var inst_32304 = (state_32424[(16)]);
var inst_32319 = (state_32424[(2)]);
var inst_32320 = (inst_32304 + (1));
var tmp32436 = inst_32303;
var tmp32437 = inst_32301;
var tmp32438 = inst_32302;
var inst_32301__$1 = tmp32437;
var inst_32302__$1 = tmp32438;
var inst_32303__$1 = tmp32436;
var inst_32304__$1 = inst_32320;
var state_32424__$1 = (function (){var statearr_32443 = state_32424;
(statearr_32443[(18)] = inst_32319);

(statearr_32443[(13)] = inst_32303__$1);

(statearr_32443[(14)] = inst_32301__$1);

(statearr_32443[(15)] = inst_32302__$1);

(statearr_32443[(16)] = inst_32304__$1);

return statearr_32443;
})();
var statearr_32444_32520 = state_32424__$1;
(statearr_32444_32520[(2)] = null);

(statearr_32444_32520[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (21))){
var inst_32346 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32448_32521 = state_32424__$1;
(statearr_32448_32521[(2)] = inst_32346);

(statearr_32448_32521[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (31))){
var inst_32372 = (state_32424[(12)]);
var inst_32376 = done.call(null,null);
var inst_32377 = cljs.core.async.untap_STAR_.call(null,m,inst_32372);
var state_32424__$1 = (function (){var statearr_32449 = state_32424;
(statearr_32449[(19)] = inst_32376);

return statearr_32449;
})();
var statearr_32450_32522 = state_32424__$1;
(statearr_32450_32522[(2)] = inst_32377);

(statearr_32450_32522[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (32))){
var inst_32367 = (state_32424[(9)]);
var inst_32365 = (state_32424[(10)]);
var inst_32364 = (state_32424[(20)]);
var inst_32366 = (state_32424[(21)]);
var inst_32379 = (state_32424[(2)]);
var inst_32380 = (inst_32367 + (1));
var tmp32445 = inst_32365;
var tmp32446 = inst_32364;
var tmp32447 = inst_32366;
var inst_32364__$1 = tmp32446;
var inst_32365__$1 = tmp32445;
var inst_32366__$1 = tmp32447;
var inst_32367__$1 = inst_32380;
var state_32424__$1 = (function (){var statearr_32451 = state_32424;
(statearr_32451[(22)] = inst_32379);

(statearr_32451[(9)] = inst_32367__$1);

(statearr_32451[(10)] = inst_32365__$1);

(statearr_32451[(20)] = inst_32364__$1);

(statearr_32451[(21)] = inst_32366__$1);

return statearr_32451;
})();
var statearr_32452_32523 = state_32424__$1;
(statearr_32452_32523[(2)] = null);

(statearr_32452_32523[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (40))){
var inst_32392 = (state_32424[(23)]);
var inst_32396 = done.call(null,null);
var inst_32397 = cljs.core.async.untap_STAR_.call(null,m,inst_32392);
var state_32424__$1 = (function (){var statearr_32453 = state_32424;
(statearr_32453[(24)] = inst_32396);

return statearr_32453;
})();
var statearr_32454_32524 = state_32424__$1;
(statearr_32454_32524[(2)] = inst_32397);

(statearr_32454_32524[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (33))){
var inst_32383 = (state_32424[(25)]);
var inst_32385 = cljs.core.chunked_seq_QMARK_.call(null,inst_32383);
var state_32424__$1 = state_32424;
if(inst_32385){
var statearr_32455_32525 = state_32424__$1;
(statearr_32455_32525[(1)] = (36));

} else {
var statearr_32456_32526 = state_32424__$1;
(statearr_32456_32526[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (13))){
var inst_32313 = (state_32424[(26)]);
var inst_32316 = cljs.core.async.close_BANG_.call(null,inst_32313);
var state_32424__$1 = state_32424;
var statearr_32457_32527 = state_32424__$1;
(statearr_32457_32527[(2)] = inst_32316);

(statearr_32457_32527[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (22))){
var inst_32336 = (state_32424[(8)]);
var inst_32339 = cljs.core.async.close_BANG_.call(null,inst_32336);
var state_32424__$1 = state_32424;
var statearr_32458_32528 = state_32424__$1;
(statearr_32458_32528[(2)] = inst_32339);

(statearr_32458_32528[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (36))){
var inst_32383 = (state_32424[(25)]);
var inst_32387 = cljs.core.chunk_first.call(null,inst_32383);
var inst_32388 = cljs.core.chunk_rest.call(null,inst_32383);
var inst_32389 = cljs.core.count.call(null,inst_32387);
var inst_32364 = inst_32388;
var inst_32365 = inst_32387;
var inst_32366 = inst_32389;
var inst_32367 = (0);
var state_32424__$1 = (function (){var statearr_32459 = state_32424;
(statearr_32459[(9)] = inst_32367);

(statearr_32459[(10)] = inst_32365);

(statearr_32459[(20)] = inst_32364);

(statearr_32459[(21)] = inst_32366);

return statearr_32459;
})();
var statearr_32460_32529 = state_32424__$1;
(statearr_32460_32529[(2)] = null);

(statearr_32460_32529[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (41))){
var inst_32383 = (state_32424[(25)]);
var inst_32399 = (state_32424[(2)]);
var inst_32400 = cljs.core.next.call(null,inst_32383);
var inst_32364 = inst_32400;
var inst_32365 = null;
var inst_32366 = (0);
var inst_32367 = (0);
var state_32424__$1 = (function (){var statearr_32461 = state_32424;
(statearr_32461[(27)] = inst_32399);

(statearr_32461[(9)] = inst_32367);

(statearr_32461[(10)] = inst_32365);

(statearr_32461[(20)] = inst_32364);

(statearr_32461[(21)] = inst_32366);

return statearr_32461;
})();
var statearr_32462_32530 = state_32424__$1;
(statearr_32462_32530[(2)] = null);

(statearr_32462_32530[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (43))){
var state_32424__$1 = state_32424;
var statearr_32463_32531 = state_32424__$1;
(statearr_32463_32531[(2)] = null);

(statearr_32463_32531[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (29))){
var inst_32408 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32464_32532 = state_32424__$1;
(statearr_32464_32532[(2)] = inst_32408);

(statearr_32464_32532[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (44))){
var inst_32417 = (state_32424[(2)]);
var state_32424__$1 = (function (){var statearr_32465 = state_32424;
(statearr_32465[(28)] = inst_32417);

return statearr_32465;
})();
var statearr_32466_32533 = state_32424__$1;
(statearr_32466_32533[(2)] = null);

(statearr_32466_32533[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (6))){
var inst_32356 = (state_32424[(29)]);
var inst_32355 = cljs.core.deref.call(null,cs);
var inst_32356__$1 = cljs.core.keys.call(null,inst_32355);
var inst_32357 = cljs.core.count.call(null,inst_32356__$1);
var inst_32358 = cljs.core.reset_BANG_.call(null,dctr,inst_32357);
var inst_32363 = cljs.core.seq.call(null,inst_32356__$1);
var inst_32364 = inst_32363;
var inst_32365 = null;
var inst_32366 = (0);
var inst_32367 = (0);
var state_32424__$1 = (function (){var statearr_32467 = state_32424;
(statearr_32467[(30)] = inst_32358);

(statearr_32467[(9)] = inst_32367);

(statearr_32467[(10)] = inst_32365);

(statearr_32467[(20)] = inst_32364);

(statearr_32467[(29)] = inst_32356__$1);

(statearr_32467[(21)] = inst_32366);

return statearr_32467;
})();
var statearr_32468_32534 = state_32424__$1;
(statearr_32468_32534[(2)] = null);

(statearr_32468_32534[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (28))){
var inst_32383 = (state_32424[(25)]);
var inst_32364 = (state_32424[(20)]);
var inst_32383__$1 = cljs.core.seq.call(null,inst_32364);
var state_32424__$1 = (function (){var statearr_32469 = state_32424;
(statearr_32469[(25)] = inst_32383__$1);

return statearr_32469;
})();
if(inst_32383__$1){
var statearr_32470_32535 = state_32424__$1;
(statearr_32470_32535[(1)] = (33));

} else {
var statearr_32471_32536 = state_32424__$1;
(statearr_32471_32536[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (25))){
var inst_32367 = (state_32424[(9)]);
var inst_32366 = (state_32424[(21)]);
var inst_32369 = (inst_32367 < inst_32366);
var inst_32370 = inst_32369;
var state_32424__$1 = state_32424;
if(cljs.core.truth_(inst_32370)){
var statearr_32472_32537 = state_32424__$1;
(statearr_32472_32537[(1)] = (27));

} else {
var statearr_32473_32538 = state_32424__$1;
(statearr_32473_32538[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (34))){
var state_32424__$1 = state_32424;
var statearr_32474_32539 = state_32424__$1;
(statearr_32474_32539[(2)] = null);

(statearr_32474_32539[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (17))){
var state_32424__$1 = state_32424;
var statearr_32475_32540 = state_32424__$1;
(statearr_32475_32540[(2)] = null);

(statearr_32475_32540[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (3))){
var inst_32422 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32424__$1,inst_32422);
} else {
if((state_val_32425 === (12))){
var inst_32351 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32476_32541 = state_32424__$1;
(statearr_32476_32541[(2)] = inst_32351);

(statearr_32476_32541[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (2))){
var state_32424__$1 = state_32424;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32424__$1,(4),ch);
} else {
if((state_val_32425 === (23))){
var state_32424__$1 = state_32424;
var statearr_32477_32542 = state_32424__$1;
(statearr_32477_32542[(2)] = null);

(statearr_32477_32542[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (35))){
var inst_32406 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32478_32543 = state_32424__$1;
(statearr_32478_32543[(2)] = inst_32406);

(statearr_32478_32543[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (19))){
var inst_32323 = (state_32424[(7)]);
var inst_32327 = cljs.core.chunk_first.call(null,inst_32323);
var inst_32328 = cljs.core.chunk_rest.call(null,inst_32323);
var inst_32329 = cljs.core.count.call(null,inst_32327);
var inst_32301 = inst_32328;
var inst_32302 = inst_32327;
var inst_32303 = inst_32329;
var inst_32304 = (0);
var state_32424__$1 = (function (){var statearr_32479 = state_32424;
(statearr_32479[(13)] = inst_32303);

(statearr_32479[(14)] = inst_32301);

(statearr_32479[(15)] = inst_32302);

(statearr_32479[(16)] = inst_32304);

return statearr_32479;
})();
var statearr_32480_32544 = state_32424__$1;
(statearr_32480_32544[(2)] = null);

(statearr_32480_32544[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (11))){
var inst_32301 = (state_32424[(14)]);
var inst_32323 = (state_32424[(7)]);
var inst_32323__$1 = cljs.core.seq.call(null,inst_32301);
var state_32424__$1 = (function (){var statearr_32481 = state_32424;
(statearr_32481[(7)] = inst_32323__$1);

return statearr_32481;
})();
if(inst_32323__$1){
var statearr_32482_32545 = state_32424__$1;
(statearr_32482_32545[(1)] = (16));

} else {
var statearr_32483_32546 = state_32424__$1;
(statearr_32483_32546[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (9))){
var inst_32353 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32484_32547 = state_32424__$1;
(statearr_32484_32547[(2)] = inst_32353);

(statearr_32484_32547[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (5))){
var inst_32299 = cljs.core.deref.call(null,cs);
var inst_32300 = cljs.core.seq.call(null,inst_32299);
var inst_32301 = inst_32300;
var inst_32302 = null;
var inst_32303 = (0);
var inst_32304 = (0);
var state_32424__$1 = (function (){var statearr_32485 = state_32424;
(statearr_32485[(13)] = inst_32303);

(statearr_32485[(14)] = inst_32301);

(statearr_32485[(15)] = inst_32302);

(statearr_32485[(16)] = inst_32304);

return statearr_32485;
})();
var statearr_32486_32548 = state_32424__$1;
(statearr_32486_32548[(2)] = null);

(statearr_32486_32548[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (14))){
var state_32424__$1 = state_32424;
var statearr_32487_32549 = state_32424__$1;
(statearr_32487_32549[(2)] = null);

(statearr_32487_32549[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (45))){
var inst_32414 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32488_32550 = state_32424__$1;
(statearr_32488_32550[(2)] = inst_32414);

(statearr_32488_32550[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (26))){
var inst_32356 = (state_32424[(29)]);
var inst_32410 = (state_32424[(2)]);
var inst_32411 = cljs.core.seq.call(null,inst_32356);
var state_32424__$1 = (function (){var statearr_32489 = state_32424;
(statearr_32489[(31)] = inst_32410);

return statearr_32489;
})();
if(inst_32411){
var statearr_32490_32551 = state_32424__$1;
(statearr_32490_32551[(1)] = (42));

} else {
var statearr_32491_32552 = state_32424__$1;
(statearr_32491_32552[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (16))){
var inst_32323 = (state_32424[(7)]);
var inst_32325 = cljs.core.chunked_seq_QMARK_.call(null,inst_32323);
var state_32424__$1 = state_32424;
if(inst_32325){
var statearr_32492_32553 = state_32424__$1;
(statearr_32492_32553[(1)] = (19));

} else {
var statearr_32493_32554 = state_32424__$1;
(statearr_32493_32554[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (38))){
var inst_32403 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32494_32555 = state_32424__$1;
(statearr_32494_32555[(2)] = inst_32403);

(statearr_32494_32555[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (30))){
var state_32424__$1 = state_32424;
var statearr_32495_32556 = state_32424__$1;
(statearr_32495_32556[(2)] = null);

(statearr_32495_32556[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (10))){
var inst_32302 = (state_32424[(15)]);
var inst_32304 = (state_32424[(16)]);
var inst_32312 = cljs.core._nth.call(null,inst_32302,inst_32304);
var inst_32313 = cljs.core.nth.call(null,inst_32312,(0),null);
var inst_32314 = cljs.core.nth.call(null,inst_32312,(1),null);
var state_32424__$1 = (function (){var statearr_32496 = state_32424;
(statearr_32496[(26)] = inst_32313);

return statearr_32496;
})();
if(cljs.core.truth_(inst_32314)){
var statearr_32497_32557 = state_32424__$1;
(statearr_32497_32557[(1)] = (13));

} else {
var statearr_32498_32558 = state_32424__$1;
(statearr_32498_32558[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (18))){
var inst_32349 = (state_32424[(2)]);
var state_32424__$1 = state_32424;
var statearr_32499_32559 = state_32424__$1;
(statearr_32499_32559[(2)] = inst_32349);

(statearr_32499_32559[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (42))){
var state_32424__$1 = state_32424;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32424__$1,(45),dchan);
} else {
if((state_val_32425 === (37))){
var inst_32383 = (state_32424[(25)]);
var inst_32392 = (state_32424[(23)]);
var inst_32292 = (state_32424[(11)]);
var inst_32392__$1 = cljs.core.first.call(null,inst_32383);
var inst_32393 = cljs.core.async.put_BANG_.call(null,inst_32392__$1,inst_32292,done);
var state_32424__$1 = (function (){var statearr_32500 = state_32424;
(statearr_32500[(23)] = inst_32392__$1);

return statearr_32500;
})();
if(cljs.core.truth_(inst_32393)){
var statearr_32501_32560 = state_32424__$1;
(statearr_32501_32560[(1)] = (39));

} else {
var statearr_32502_32561 = state_32424__$1;
(statearr_32502_32561[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32425 === (8))){
var inst_32303 = (state_32424[(13)]);
var inst_32304 = (state_32424[(16)]);
var inst_32306 = (inst_32304 < inst_32303);
var inst_32307 = inst_32306;
var state_32424__$1 = state_32424;
if(cljs.core.truth_(inst_32307)){
var statearr_32503_32562 = state_32424__$1;
(statearr_32503_32562[(1)] = (10));

} else {
var statearr_32504_32563 = state_32424__$1;
(statearr_32504_32563[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___32509,cs,m,dchan,dctr,done))
;
return ((function (switch__31616__auto__,c__31704__auto___32509,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__31617__auto__ = null;
var cljs$core$async$mult_$_state_machine__31617__auto____0 = (function (){
var statearr_32505 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32505[(0)] = cljs$core$async$mult_$_state_machine__31617__auto__);

(statearr_32505[(1)] = (1));

return statearr_32505;
});
var cljs$core$async$mult_$_state_machine__31617__auto____1 = (function (state_32424){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_32424);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32506){if((e32506 instanceof Object)){
var ex__31620__auto__ = e32506;
var statearr_32507_32564 = state_32424;
(statearr_32507_32564[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32424);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32506;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32565 = state_32424;
state_32424 = G__32565;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__31617__auto__ = function(state_32424){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__31617__auto____1.call(this,state_32424);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mult_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__31617__auto____0;
cljs$core$async$mult_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__31617__auto____1;
return cljs$core$async$mult_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___32509,cs,m,dchan,dctr,done))
})();
var state__31706__auto__ = (function (){var statearr_32508 = f__31705__auto__.call(null);
(statearr_32508[(6)] = c__31704__auto___32509);

return statearr_32508;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___32509,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__32567 = arguments.length;
switch (G__32567) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m,ch);
} else {
var m__28653__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m,ch);
} else {
var m__28653__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m);
} else {
var m__28653__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((!((m == null))) && (!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m,state_map);
} else {
var m__28653__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((!((m == null))) && (!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__28652__auto__ = (((m == null))?null:m);
var m__28653__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,m,mode);
} else {
var m__28653__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__29140__auto__ = [];
var len__29133__auto___32579 = arguments.length;
var i__29134__auto___32580 = (0);
while(true){
if((i__29134__auto___32580 < len__29133__auto___32579)){
args__29140__auto__.push((arguments[i__29134__auto___32580]));

var G__32581 = (i__29134__auto___32580 + (1));
i__29134__auto___32580 = G__32581;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((3) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__29141__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__32573){
var map__32574 = p__32573;
var map__32574__$1 = ((((!((map__32574 == null)))?((((map__32574.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__32574.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__32574):map__32574);
var opts = map__32574__$1;
var statearr_32576_32582 = state;
(statearr_32576_32582[(1)] = cont_block);


var temp__4657__auto__ = cljs.core.async.do_alts.call(null,((function (map__32574,map__32574__$1,opts){
return (function (val){
var statearr_32577_32583 = state;
(statearr_32577_32583[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__32574,map__32574__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__4657__auto__)){
var cb = temp__4657__auto__;
var statearr_32578_32584 = state;
(statearr_32578_32584[(2)] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq32569){
var G__32570 = cljs.core.first.call(null,seq32569);
var seq32569__$1 = cljs.core.next.call(null,seq32569);
var G__32571 = cljs.core.first.call(null,seq32569__$1);
var seq32569__$2 = cljs.core.next.call(null,seq32569__$1);
var G__32572 = cljs.core.first.call(null,seq32569__$2);
var seq32569__$3 = cljs.core.next.call(null,seq32569__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__32570,G__32571,G__32572,seq32569__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async32585 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32585 = (function (out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,meta32586){
this.out = out;
this.cs = cs;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.solo_mode = solo_mode;
this.change = change;
this.changed = changed;
this.pick = pick;
this.calc_state = calc_state;
this.meta32586 = meta32586;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_32587,meta32586__$1){
var self__ = this;
var _32587__$1 = this;
return (new cljs.core.async.t_cljs$core$async32585(self__.out,self__.cs,self__.solo_modes,self__.attrs,self__.solo_mode,self__.change,self__.changed,self__.pick,self__.calc_state,meta32586__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_32587){
var self__ = this;
var _32587__$1 = this;
return self__.meta32586;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Assert failed: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1([cljs.core.str.cljs$core$IFn$_invoke$arity$1("mode must be one of: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join('')),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("(solo-modes mode)")].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"meta32586","meta32586",908713215,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async32585.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32585.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32585";

cljs.core.async.t_cljs$core$async32585.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async32585");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t_cljs$core$async32585 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async32585(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta32586){
return (new cljs.core.async.t_cljs$core$async32585(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta32586));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async32585(out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__31704__auto___32749 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___32749,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___32749,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_32689){
var state_val_32690 = (state_32689[(1)]);
if((state_val_32690 === (7))){
var inst_32604 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
var statearr_32691_32750 = state_32689__$1;
(statearr_32691_32750[(2)] = inst_32604);

(statearr_32691_32750[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (20))){
var inst_32616 = (state_32689[(7)]);
var state_32689__$1 = state_32689;
var statearr_32692_32751 = state_32689__$1;
(statearr_32692_32751[(2)] = inst_32616);

(statearr_32692_32751[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (27))){
var state_32689__$1 = state_32689;
var statearr_32693_32752 = state_32689__$1;
(statearr_32693_32752[(2)] = null);

(statearr_32693_32752[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (1))){
var inst_32591 = (state_32689[(8)]);
var inst_32591__$1 = calc_state.call(null);
var inst_32593 = (inst_32591__$1 == null);
var inst_32594 = cljs.core.not.call(null,inst_32593);
var state_32689__$1 = (function (){var statearr_32694 = state_32689;
(statearr_32694[(8)] = inst_32591__$1);

return statearr_32694;
})();
if(inst_32594){
var statearr_32695_32753 = state_32689__$1;
(statearr_32695_32753[(1)] = (2));

} else {
var statearr_32696_32754 = state_32689__$1;
(statearr_32696_32754[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (24))){
var inst_32663 = (state_32689[(9)]);
var inst_32649 = (state_32689[(10)]);
var inst_32640 = (state_32689[(11)]);
var inst_32663__$1 = inst_32640.call(null,inst_32649);
var state_32689__$1 = (function (){var statearr_32697 = state_32689;
(statearr_32697[(9)] = inst_32663__$1);

return statearr_32697;
})();
if(cljs.core.truth_(inst_32663__$1)){
var statearr_32698_32755 = state_32689__$1;
(statearr_32698_32755[(1)] = (29));

} else {
var statearr_32699_32756 = state_32689__$1;
(statearr_32699_32756[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (4))){
var inst_32607 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
if(cljs.core.truth_(inst_32607)){
var statearr_32700_32757 = state_32689__$1;
(statearr_32700_32757[(1)] = (8));

} else {
var statearr_32701_32758 = state_32689__$1;
(statearr_32701_32758[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (15))){
var inst_32634 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
if(cljs.core.truth_(inst_32634)){
var statearr_32702_32759 = state_32689__$1;
(statearr_32702_32759[(1)] = (19));

} else {
var statearr_32703_32760 = state_32689__$1;
(statearr_32703_32760[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (21))){
var inst_32639 = (state_32689[(12)]);
var inst_32639__$1 = (state_32689[(2)]);
var inst_32640 = cljs.core.get.call(null,inst_32639__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32641 = cljs.core.get.call(null,inst_32639__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32642 = cljs.core.get.call(null,inst_32639__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_32689__$1 = (function (){var statearr_32704 = state_32689;
(statearr_32704[(13)] = inst_32641);

(statearr_32704[(12)] = inst_32639__$1);

(statearr_32704[(11)] = inst_32640);

return statearr_32704;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_32689__$1,(22),inst_32642);
} else {
if((state_val_32690 === (31))){
var inst_32671 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
if(cljs.core.truth_(inst_32671)){
var statearr_32705_32761 = state_32689__$1;
(statearr_32705_32761[(1)] = (32));

} else {
var statearr_32706_32762 = state_32689__$1;
(statearr_32706_32762[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (32))){
var inst_32648 = (state_32689[(14)]);
var state_32689__$1 = state_32689;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32689__$1,(35),out,inst_32648);
} else {
if((state_val_32690 === (33))){
var inst_32639 = (state_32689[(12)]);
var inst_32616 = inst_32639;
var state_32689__$1 = (function (){var statearr_32707 = state_32689;
(statearr_32707[(7)] = inst_32616);

return statearr_32707;
})();
var statearr_32708_32763 = state_32689__$1;
(statearr_32708_32763[(2)] = null);

(statearr_32708_32763[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (13))){
var inst_32616 = (state_32689[(7)]);
var inst_32623 = inst_32616.cljs$lang$protocol_mask$partition0$;
var inst_32624 = (inst_32623 & (64));
var inst_32625 = inst_32616.cljs$core$ISeq$;
var inst_32626 = (cljs.core.PROTOCOL_SENTINEL === inst_32625);
var inst_32627 = (inst_32624) || (inst_32626);
var state_32689__$1 = state_32689;
if(cljs.core.truth_(inst_32627)){
var statearr_32709_32764 = state_32689__$1;
(statearr_32709_32764[(1)] = (16));

} else {
var statearr_32710_32765 = state_32689__$1;
(statearr_32710_32765[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (22))){
var inst_32649 = (state_32689[(10)]);
var inst_32648 = (state_32689[(14)]);
var inst_32647 = (state_32689[(2)]);
var inst_32648__$1 = cljs.core.nth.call(null,inst_32647,(0),null);
var inst_32649__$1 = cljs.core.nth.call(null,inst_32647,(1),null);
var inst_32650 = (inst_32648__$1 == null);
var inst_32651 = cljs.core._EQ_.call(null,inst_32649__$1,change);
var inst_32652 = (inst_32650) || (inst_32651);
var state_32689__$1 = (function (){var statearr_32711 = state_32689;
(statearr_32711[(10)] = inst_32649__$1);

(statearr_32711[(14)] = inst_32648__$1);

return statearr_32711;
})();
if(cljs.core.truth_(inst_32652)){
var statearr_32712_32766 = state_32689__$1;
(statearr_32712_32766[(1)] = (23));

} else {
var statearr_32713_32767 = state_32689__$1;
(statearr_32713_32767[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (36))){
var inst_32639 = (state_32689[(12)]);
var inst_32616 = inst_32639;
var state_32689__$1 = (function (){var statearr_32714 = state_32689;
(statearr_32714[(7)] = inst_32616);

return statearr_32714;
})();
var statearr_32715_32768 = state_32689__$1;
(statearr_32715_32768[(2)] = null);

(statearr_32715_32768[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (29))){
var inst_32663 = (state_32689[(9)]);
var state_32689__$1 = state_32689;
var statearr_32716_32769 = state_32689__$1;
(statearr_32716_32769[(2)] = inst_32663);

(statearr_32716_32769[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (6))){
var state_32689__$1 = state_32689;
var statearr_32717_32770 = state_32689__$1;
(statearr_32717_32770[(2)] = false);

(statearr_32717_32770[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (28))){
var inst_32659 = (state_32689[(2)]);
var inst_32660 = calc_state.call(null);
var inst_32616 = inst_32660;
var state_32689__$1 = (function (){var statearr_32718 = state_32689;
(statearr_32718[(7)] = inst_32616);

(statearr_32718[(15)] = inst_32659);

return statearr_32718;
})();
var statearr_32719_32771 = state_32689__$1;
(statearr_32719_32771[(2)] = null);

(statearr_32719_32771[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (25))){
var inst_32685 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
var statearr_32720_32772 = state_32689__$1;
(statearr_32720_32772[(2)] = inst_32685);

(statearr_32720_32772[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (34))){
var inst_32683 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
var statearr_32721_32773 = state_32689__$1;
(statearr_32721_32773[(2)] = inst_32683);

(statearr_32721_32773[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (17))){
var state_32689__$1 = state_32689;
var statearr_32722_32774 = state_32689__$1;
(statearr_32722_32774[(2)] = false);

(statearr_32722_32774[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (3))){
var state_32689__$1 = state_32689;
var statearr_32723_32775 = state_32689__$1;
(statearr_32723_32775[(2)] = false);

(statearr_32723_32775[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (12))){
var inst_32687 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32689__$1,inst_32687);
} else {
if((state_val_32690 === (2))){
var inst_32591 = (state_32689[(8)]);
var inst_32596 = inst_32591.cljs$lang$protocol_mask$partition0$;
var inst_32597 = (inst_32596 & (64));
var inst_32598 = inst_32591.cljs$core$ISeq$;
var inst_32599 = (cljs.core.PROTOCOL_SENTINEL === inst_32598);
var inst_32600 = (inst_32597) || (inst_32599);
var state_32689__$1 = state_32689;
if(cljs.core.truth_(inst_32600)){
var statearr_32724_32776 = state_32689__$1;
(statearr_32724_32776[(1)] = (5));

} else {
var statearr_32725_32777 = state_32689__$1;
(statearr_32725_32777[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (23))){
var inst_32648 = (state_32689[(14)]);
var inst_32654 = (inst_32648 == null);
var state_32689__$1 = state_32689;
if(cljs.core.truth_(inst_32654)){
var statearr_32726_32778 = state_32689__$1;
(statearr_32726_32778[(1)] = (26));

} else {
var statearr_32727_32779 = state_32689__$1;
(statearr_32727_32779[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (35))){
var inst_32674 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
if(cljs.core.truth_(inst_32674)){
var statearr_32728_32780 = state_32689__$1;
(statearr_32728_32780[(1)] = (36));

} else {
var statearr_32729_32781 = state_32689__$1;
(statearr_32729_32781[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (19))){
var inst_32616 = (state_32689[(7)]);
var inst_32636 = cljs.core.apply.call(null,cljs.core.hash_map,inst_32616);
var state_32689__$1 = state_32689;
var statearr_32730_32782 = state_32689__$1;
(statearr_32730_32782[(2)] = inst_32636);

(statearr_32730_32782[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (11))){
var inst_32616 = (state_32689[(7)]);
var inst_32620 = (inst_32616 == null);
var inst_32621 = cljs.core.not.call(null,inst_32620);
var state_32689__$1 = state_32689;
if(inst_32621){
var statearr_32731_32783 = state_32689__$1;
(statearr_32731_32783[(1)] = (13));

} else {
var statearr_32732_32784 = state_32689__$1;
(statearr_32732_32784[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (9))){
var inst_32591 = (state_32689[(8)]);
var state_32689__$1 = state_32689;
var statearr_32733_32785 = state_32689__$1;
(statearr_32733_32785[(2)] = inst_32591);

(statearr_32733_32785[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (5))){
var state_32689__$1 = state_32689;
var statearr_32734_32786 = state_32689__$1;
(statearr_32734_32786[(2)] = true);

(statearr_32734_32786[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (14))){
var state_32689__$1 = state_32689;
var statearr_32735_32787 = state_32689__$1;
(statearr_32735_32787[(2)] = false);

(statearr_32735_32787[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (26))){
var inst_32649 = (state_32689[(10)]);
var inst_32656 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_32649);
var state_32689__$1 = state_32689;
var statearr_32736_32788 = state_32689__$1;
(statearr_32736_32788[(2)] = inst_32656);

(statearr_32736_32788[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (16))){
var state_32689__$1 = state_32689;
var statearr_32737_32789 = state_32689__$1;
(statearr_32737_32789[(2)] = true);

(statearr_32737_32789[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (38))){
var inst_32679 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
var statearr_32738_32790 = state_32689__$1;
(statearr_32738_32790[(2)] = inst_32679);

(statearr_32738_32790[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (30))){
var inst_32641 = (state_32689[(13)]);
var inst_32649 = (state_32689[(10)]);
var inst_32640 = (state_32689[(11)]);
var inst_32666 = cljs.core.empty_QMARK_.call(null,inst_32640);
var inst_32667 = inst_32641.call(null,inst_32649);
var inst_32668 = cljs.core.not.call(null,inst_32667);
var inst_32669 = (inst_32666) && (inst_32668);
var state_32689__$1 = state_32689;
var statearr_32739_32791 = state_32689__$1;
(statearr_32739_32791[(2)] = inst_32669);

(statearr_32739_32791[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (10))){
var inst_32591 = (state_32689[(8)]);
var inst_32612 = (state_32689[(2)]);
var inst_32613 = cljs.core.get.call(null,inst_32612,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32614 = cljs.core.get.call(null,inst_32612,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32615 = cljs.core.get.call(null,inst_32612,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_32616 = inst_32591;
var state_32689__$1 = (function (){var statearr_32740 = state_32689;
(statearr_32740[(16)] = inst_32613);

(statearr_32740[(7)] = inst_32616);

(statearr_32740[(17)] = inst_32615);

(statearr_32740[(18)] = inst_32614);

return statearr_32740;
})();
var statearr_32741_32792 = state_32689__$1;
(statearr_32741_32792[(2)] = null);

(statearr_32741_32792[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (18))){
var inst_32631 = (state_32689[(2)]);
var state_32689__$1 = state_32689;
var statearr_32742_32793 = state_32689__$1;
(statearr_32742_32793[(2)] = inst_32631);

(statearr_32742_32793[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (37))){
var state_32689__$1 = state_32689;
var statearr_32743_32794 = state_32689__$1;
(statearr_32743_32794[(2)] = null);

(statearr_32743_32794[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32690 === (8))){
var inst_32591 = (state_32689[(8)]);
var inst_32609 = cljs.core.apply.call(null,cljs.core.hash_map,inst_32591);
var state_32689__$1 = state_32689;
var statearr_32744_32795 = state_32689__$1;
(statearr_32744_32795[(2)] = inst_32609);

(statearr_32744_32795[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___32749,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__31616__auto__,c__31704__auto___32749,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__31617__auto__ = null;
var cljs$core$async$mix_$_state_machine__31617__auto____0 = (function (){
var statearr_32745 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32745[(0)] = cljs$core$async$mix_$_state_machine__31617__auto__);

(statearr_32745[(1)] = (1));

return statearr_32745;
});
var cljs$core$async$mix_$_state_machine__31617__auto____1 = (function (state_32689){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_32689);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32746){if((e32746 instanceof Object)){
var ex__31620__auto__ = e32746;
var statearr_32747_32796 = state_32689;
(statearr_32747_32796[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32689);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32746;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32797 = state_32689;
state_32689 = G__32797;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__31617__auto__ = function(state_32689){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__31617__auto____1.call(this,state_32689);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mix_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__31617__auto____0;
cljs$core$async$mix_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__31617__auto____1;
return cljs$core$async$mix_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___32749,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__31706__auto__ = (function (){var statearr_32748 = f__31705__auto__.call(null);
(statearr_32748[(6)] = c__31704__auto___32749);

return statearr_32748;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___32749,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((!((p == null))) && (!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__28652__auto__ = (((p == null))?null:p);
var m__28653__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__28653__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__28652__auto__ = (((p == null))?null:p);
var m__28653__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,p,v,ch);
} else {
var m__28653__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__32799 = arguments.length;
switch (G__32799) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__28652__auto__ = (((p == null))?null:p);
var m__28653__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,p);
} else {
var m__28653__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__28652__auto__ = (((p == null))?null:p);
var m__28653__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__28652__auto__)]);
if(!((m__28653__auto__ == null))){
return m__28653__auto__.call(null,p,v);
} else {
var m__28653__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__28653__auto____$1 == null))){
return m__28653__auto____$1.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__32803 = arguments.length;
switch (G__32803) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__27969__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__27969__auto__,mults){
return (function (p1__32801_SHARP_){
if(cljs.core.truth_(p1__32801_SHARP_.call(null,topic))){
return p1__32801_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__32801_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__27969__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t_cljs$core$async32804 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32804 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta32805){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta32805 = meta32805;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_32806,meta32805__$1){
var self__ = this;
var _32806__$1 = this;
return (new cljs.core.async.t_cljs$core$async32804(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta32805__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_32806){
var self__ = this;
var _32806__$1 = this;
return self__.meta32805;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__4657__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__4657__auto__)){
var m = temp__4657__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta32805","meta32805",2094605299,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32804.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32804.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32804";

cljs.core.async.t_cljs$core$async32804.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async32804");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t_cljs$core$async32804 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async32804(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32805){
return (new cljs.core.async.t_cljs$core$async32804(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32805));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async32804(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__31704__auto___32924 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___32924,mults,ensure_mult,p){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___32924,mults,ensure_mult,p){
return (function (state_32878){
var state_val_32879 = (state_32878[(1)]);
if((state_val_32879 === (7))){
var inst_32874 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
var statearr_32880_32925 = state_32878__$1;
(statearr_32880_32925[(2)] = inst_32874);

(statearr_32880_32925[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (20))){
var state_32878__$1 = state_32878;
var statearr_32881_32926 = state_32878__$1;
(statearr_32881_32926[(2)] = null);

(statearr_32881_32926[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (1))){
var state_32878__$1 = state_32878;
var statearr_32882_32927 = state_32878__$1;
(statearr_32882_32927[(2)] = null);

(statearr_32882_32927[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (24))){
var inst_32857 = (state_32878[(7)]);
var inst_32866 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_32857);
var state_32878__$1 = state_32878;
var statearr_32883_32928 = state_32878__$1;
(statearr_32883_32928[(2)] = inst_32866);

(statearr_32883_32928[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (4))){
var inst_32809 = (state_32878[(8)]);
var inst_32809__$1 = (state_32878[(2)]);
var inst_32810 = (inst_32809__$1 == null);
var state_32878__$1 = (function (){var statearr_32884 = state_32878;
(statearr_32884[(8)] = inst_32809__$1);

return statearr_32884;
})();
if(cljs.core.truth_(inst_32810)){
var statearr_32885_32929 = state_32878__$1;
(statearr_32885_32929[(1)] = (5));

} else {
var statearr_32886_32930 = state_32878__$1;
(statearr_32886_32930[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (15))){
var inst_32851 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
var statearr_32887_32931 = state_32878__$1;
(statearr_32887_32931[(2)] = inst_32851);

(statearr_32887_32931[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (21))){
var inst_32871 = (state_32878[(2)]);
var state_32878__$1 = (function (){var statearr_32888 = state_32878;
(statearr_32888[(9)] = inst_32871);

return statearr_32888;
})();
var statearr_32889_32932 = state_32878__$1;
(statearr_32889_32932[(2)] = null);

(statearr_32889_32932[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (13))){
var inst_32833 = (state_32878[(10)]);
var inst_32835 = cljs.core.chunked_seq_QMARK_.call(null,inst_32833);
var state_32878__$1 = state_32878;
if(inst_32835){
var statearr_32890_32933 = state_32878__$1;
(statearr_32890_32933[(1)] = (16));

} else {
var statearr_32891_32934 = state_32878__$1;
(statearr_32891_32934[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (22))){
var inst_32863 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
if(cljs.core.truth_(inst_32863)){
var statearr_32892_32935 = state_32878__$1;
(statearr_32892_32935[(1)] = (23));

} else {
var statearr_32893_32936 = state_32878__$1;
(statearr_32893_32936[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (6))){
var inst_32857 = (state_32878[(7)]);
var inst_32859 = (state_32878[(11)]);
var inst_32809 = (state_32878[(8)]);
var inst_32857__$1 = topic_fn.call(null,inst_32809);
var inst_32858 = cljs.core.deref.call(null,mults);
var inst_32859__$1 = cljs.core.get.call(null,inst_32858,inst_32857__$1);
var state_32878__$1 = (function (){var statearr_32894 = state_32878;
(statearr_32894[(7)] = inst_32857__$1);

(statearr_32894[(11)] = inst_32859__$1);

return statearr_32894;
})();
if(cljs.core.truth_(inst_32859__$1)){
var statearr_32895_32937 = state_32878__$1;
(statearr_32895_32937[(1)] = (19));

} else {
var statearr_32896_32938 = state_32878__$1;
(statearr_32896_32938[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (25))){
var inst_32868 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
var statearr_32897_32939 = state_32878__$1;
(statearr_32897_32939[(2)] = inst_32868);

(statearr_32897_32939[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (17))){
var inst_32833 = (state_32878[(10)]);
var inst_32842 = cljs.core.first.call(null,inst_32833);
var inst_32843 = cljs.core.async.muxch_STAR_.call(null,inst_32842);
var inst_32844 = cljs.core.async.close_BANG_.call(null,inst_32843);
var inst_32845 = cljs.core.next.call(null,inst_32833);
var inst_32819 = inst_32845;
var inst_32820 = null;
var inst_32821 = (0);
var inst_32822 = (0);
var state_32878__$1 = (function (){var statearr_32898 = state_32878;
(statearr_32898[(12)] = inst_32821);

(statearr_32898[(13)] = inst_32844);

(statearr_32898[(14)] = inst_32820);

(statearr_32898[(15)] = inst_32819);

(statearr_32898[(16)] = inst_32822);

return statearr_32898;
})();
var statearr_32899_32940 = state_32878__$1;
(statearr_32899_32940[(2)] = null);

(statearr_32899_32940[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (3))){
var inst_32876 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32878__$1,inst_32876);
} else {
if((state_val_32879 === (12))){
var inst_32853 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
var statearr_32900_32941 = state_32878__$1;
(statearr_32900_32941[(2)] = inst_32853);

(statearr_32900_32941[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (2))){
var state_32878__$1 = state_32878;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32878__$1,(4),ch);
} else {
if((state_val_32879 === (23))){
var state_32878__$1 = state_32878;
var statearr_32901_32942 = state_32878__$1;
(statearr_32901_32942[(2)] = null);

(statearr_32901_32942[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (19))){
var inst_32859 = (state_32878[(11)]);
var inst_32809 = (state_32878[(8)]);
var inst_32861 = cljs.core.async.muxch_STAR_.call(null,inst_32859);
var state_32878__$1 = state_32878;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32878__$1,(22),inst_32861,inst_32809);
} else {
if((state_val_32879 === (11))){
var inst_32833 = (state_32878[(10)]);
var inst_32819 = (state_32878[(15)]);
var inst_32833__$1 = cljs.core.seq.call(null,inst_32819);
var state_32878__$1 = (function (){var statearr_32902 = state_32878;
(statearr_32902[(10)] = inst_32833__$1);

return statearr_32902;
})();
if(inst_32833__$1){
var statearr_32903_32943 = state_32878__$1;
(statearr_32903_32943[(1)] = (13));

} else {
var statearr_32904_32944 = state_32878__$1;
(statearr_32904_32944[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (9))){
var inst_32855 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
var statearr_32905_32945 = state_32878__$1;
(statearr_32905_32945[(2)] = inst_32855);

(statearr_32905_32945[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (5))){
var inst_32816 = cljs.core.deref.call(null,mults);
var inst_32817 = cljs.core.vals.call(null,inst_32816);
var inst_32818 = cljs.core.seq.call(null,inst_32817);
var inst_32819 = inst_32818;
var inst_32820 = null;
var inst_32821 = (0);
var inst_32822 = (0);
var state_32878__$1 = (function (){var statearr_32906 = state_32878;
(statearr_32906[(12)] = inst_32821);

(statearr_32906[(14)] = inst_32820);

(statearr_32906[(15)] = inst_32819);

(statearr_32906[(16)] = inst_32822);

return statearr_32906;
})();
var statearr_32907_32946 = state_32878__$1;
(statearr_32907_32946[(2)] = null);

(statearr_32907_32946[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (14))){
var state_32878__$1 = state_32878;
var statearr_32911_32947 = state_32878__$1;
(statearr_32911_32947[(2)] = null);

(statearr_32911_32947[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (16))){
var inst_32833 = (state_32878[(10)]);
var inst_32837 = cljs.core.chunk_first.call(null,inst_32833);
var inst_32838 = cljs.core.chunk_rest.call(null,inst_32833);
var inst_32839 = cljs.core.count.call(null,inst_32837);
var inst_32819 = inst_32838;
var inst_32820 = inst_32837;
var inst_32821 = inst_32839;
var inst_32822 = (0);
var state_32878__$1 = (function (){var statearr_32912 = state_32878;
(statearr_32912[(12)] = inst_32821);

(statearr_32912[(14)] = inst_32820);

(statearr_32912[(15)] = inst_32819);

(statearr_32912[(16)] = inst_32822);

return statearr_32912;
})();
var statearr_32913_32948 = state_32878__$1;
(statearr_32913_32948[(2)] = null);

(statearr_32913_32948[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (10))){
var inst_32821 = (state_32878[(12)]);
var inst_32820 = (state_32878[(14)]);
var inst_32819 = (state_32878[(15)]);
var inst_32822 = (state_32878[(16)]);
var inst_32827 = cljs.core._nth.call(null,inst_32820,inst_32822);
var inst_32828 = cljs.core.async.muxch_STAR_.call(null,inst_32827);
var inst_32829 = cljs.core.async.close_BANG_.call(null,inst_32828);
var inst_32830 = (inst_32822 + (1));
var tmp32908 = inst_32821;
var tmp32909 = inst_32820;
var tmp32910 = inst_32819;
var inst_32819__$1 = tmp32910;
var inst_32820__$1 = tmp32909;
var inst_32821__$1 = tmp32908;
var inst_32822__$1 = inst_32830;
var state_32878__$1 = (function (){var statearr_32914 = state_32878;
(statearr_32914[(12)] = inst_32821__$1);

(statearr_32914[(14)] = inst_32820__$1);

(statearr_32914[(15)] = inst_32819__$1);

(statearr_32914[(17)] = inst_32829);

(statearr_32914[(16)] = inst_32822__$1);

return statearr_32914;
})();
var statearr_32915_32949 = state_32878__$1;
(statearr_32915_32949[(2)] = null);

(statearr_32915_32949[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (18))){
var inst_32848 = (state_32878[(2)]);
var state_32878__$1 = state_32878;
var statearr_32916_32950 = state_32878__$1;
(statearr_32916_32950[(2)] = inst_32848);

(statearr_32916_32950[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32879 === (8))){
var inst_32821 = (state_32878[(12)]);
var inst_32822 = (state_32878[(16)]);
var inst_32824 = (inst_32822 < inst_32821);
var inst_32825 = inst_32824;
var state_32878__$1 = state_32878;
if(cljs.core.truth_(inst_32825)){
var statearr_32917_32951 = state_32878__$1;
(statearr_32917_32951[(1)] = (10));

} else {
var statearr_32918_32952 = state_32878__$1;
(statearr_32918_32952[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___32924,mults,ensure_mult,p))
;
return ((function (switch__31616__auto__,c__31704__auto___32924,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_32919 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32919[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_32919[(1)] = (1));

return statearr_32919;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_32878){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_32878);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e32920){if((e32920 instanceof Object)){
var ex__31620__auto__ = e32920;
var statearr_32921_32953 = state_32878;
(statearr_32921_32953[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32878);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32920;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32954 = state_32878;
state_32878 = G__32954;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_32878){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_32878);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___32924,mults,ensure_mult,p))
})();
var state__31706__auto__ = (function (){var statearr_32922 = f__31705__auto__.call(null);
(statearr_32922[(6)] = c__31704__auto___32924);

return statearr_32922;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___32924,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__32956 = arguments.length;
switch (G__32956) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__32959 = arguments.length;
switch (G__32959) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__32962 = arguments.length;
switch (G__32962) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__31704__auto___33029 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___33029,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___33029,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_33001){
var state_val_33002 = (state_33001[(1)]);
if((state_val_33002 === (7))){
var state_33001__$1 = state_33001;
var statearr_33003_33030 = state_33001__$1;
(statearr_33003_33030[(2)] = null);

(statearr_33003_33030[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (1))){
var state_33001__$1 = state_33001;
var statearr_33004_33031 = state_33001__$1;
(statearr_33004_33031[(2)] = null);

(statearr_33004_33031[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (4))){
var inst_32965 = (state_33001[(7)]);
var inst_32967 = (inst_32965 < cnt);
var state_33001__$1 = state_33001;
if(cljs.core.truth_(inst_32967)){
var statearr_33005_33032 = state_33001__$1;
(statearr_33005_33032[(1)] = (6));

} else {
var statearr_33006_33033 = state_33001__$1;
(statearr_33006_33033[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (15))){
var inst_32997 = (state_33001[(2)]);
var state_33001__$1 = state_33001;
var statearr_33007_33034 = state_33001__$1;
(statearr_33007_33034[(2)] = inst_32997);

(statearr_33007_33034[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (13))){
var inst_32990 = cljs.core.async.close_BANG_.call(null,out);
var state_33001__$1 = state_33001;
var statearr_33008_33035 = state_33001__$1;
(statearr_33008_33035[(2)] = inst_32990);

(statearr_33008_33035[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (6))){
var state_33001__$1 = state_33001;
var statearr_33009_33036 = state_33001__$1;
(statearr_33009_33036[(2)] = null);

(statearr_33009_33036[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (3))){
var inst_32999 = (state_33001[(2)]);
var state_33001__$1 = state_33001;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33001__$1,inst_32999);
} else {
if((state_val_33002 === (12))){
var inst_32987 = (state_33001[(8)]);
var inst_32987__$1 = (state_33001[(2)]);
var inst_32988 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_32987__$1);
var state_33001__$1 = (function (){var statearr_33010 = state_33001;
(statearr_33010[(8)] = inst_32987__$1);

return statearr_33010;
})();
if(cljs.core.truth_(inst_32988)){
var statearr_33011_33037 = state_33001__$1;
(statearr_33011_33037[(1)] = (13));

} else {
var statearr_33012_33038 = state_33001__$1;
(statearr_33012_33038[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (2))){
var inst_32964 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_32965 = (0);
var state_33001__$1 = (function (){var statearr_33013 = state_33001;
(statearr_33013[(9)] = inst_32964);

(statearr_33013[(7)] = inst_32965);

return statearr_33013;
})();
var statearr_33014_33039 = state_33001__$1;
(statearr_33014_33039[(2)] = null);

(statearr_33014_33039[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (11))){
var inst_32965 = (state_33001[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_33001,(10),Object,null,(9));
var inst_32974 = chs__$1.call(null,inst_32965);
var inst_32975 = done.call(null,inst_32965);
var inst_32976 = cljs.core.async.take_BANG_.call(null,inst_32974,inst_32975);
var state_33001__$1 = state_33001;
var statearr_33015_33040 = state_33001__$1;
(statearr_33015_33040[(2)] = inst_32976);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33001__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (9))){
var inst_32965 = (state_33001[(7)]);
var inst_32978 = (state_33001[(2)]);
var inst_32979 = (inst_32965 + (1));
var inst_32965__$1 = inst_32979;
var state_33001__$1 = (function (){var statearr_33016 = state_33001;
(statearr_33016[(10)] = inst_32978);

(statearr_33016[(7)] = inst_32965__$1);

return statearr_33016;
})();
var statearr_33017_33041 = state_33001__$1;
(statearr_33017_33041[(2)] = null);

(statearr_33017_33041[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (5))){
var inst_32985 = (state_33001[(2)]);
var state_33001__$1 = (function (){var statearr_33018 = state_33001;
(statearr_33018[(11)] = inst_32985);

return statearr_33018;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33001__$1,(12),dchan);
} else {
if((state_val_33002 === (14))){
var inst_32987 = (state_33001[(8)]);
var inst_32992 = cljs.core.apply.call(null,f,inst_32987);
var state_33001__$1 = state_33001;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33001__$1,(16),out,inst_32992);
} else {
if((state_val_33002 === (16))){
var inst_32994 = (state_33001[(2)]);
var state_33001__$1 = (function (){var statearr_33019 = state_33001;
(statearr_33019[(12)] = inst_32994);

return statearr_33019;
})();
var statearr_33020_33042 = state_33001__$1;
(statearr_33020_33042[(2)] = null);

(statearr_33020_33042[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (10))){
var inst_32969 = (state_33001[(2)]);
var inst_32970 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_33001__$1 = (function (){var statearr_33021 = state_33001;
(statearr_33021[(13)] = inst_32969);

return statearr_33021;
})();
var statearr_33022_33043 = state_33001__$1;
(statearr_33022_33043[(2)] = inst_32970);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33001__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33002 === (8))){
var inst_32983 = (state_33001[(2)]);
var state_33001__$1 = state_33001;
var statearr_33023_33044 = state_33001__$1;
(statearr_33023_33044[(2)] = inst_32983);

(statearr_33023_33044[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___33029,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__31616__auto__,c__31704__auto___33029,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_33024 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33024[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_33024[(1)] = (1));

return statearr_33024;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_33001){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33001);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33025){if((e33025 instanceof Object)){
var ex__31620__auto__ = e33025;
var statearr_33026_33045 = state_33001;
(statearr_33026_33045[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33001);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33025;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33046 = state_33001;
state_33001 = G__33046;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_33001){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_33001);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___33029,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__31706__auto__ = (function (){var statearr_33027 = f__31705__auto__.call(null);
(statearr_33027[(6)] = c__31704__auto___33029);

return statearr_33027;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___33029,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__33049 = arguments.length;
switch (G__33049) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__31704__auto___33103 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___33103,out){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___33103,out){
return (function (state_33081){
var state_val_33082 = (state_33081[(1)]);
if((state_val_33082 === (7))){
var inst_33061 = (state_33081[(7)]);
var inst_33060 = (state_33081[(8)]);
var inst_33060__$1 = (state_33081[(2)]);
var inst_33061__$1 = cljs.core.nth.call(null,inst_33060__$1,(0),null);
var inst_33062 = cljs.core.nth.call(null,inst_33060__$1,(1),null);
var inst_33063 = (inst_33061__$1 == null);
var state_33081__$1 = (function (){var statearr_33083 = state_33081;
(statearr_33083[(7)] = inst_33061__$1);

(statearr_33083[(8)] = inst_33060__$1);

(statearr_33083[(9)] = inst_33062);

return statearr_33083;
})();
if(cljs.core.truth_(inst_33063)){
var statearr_33084_33104 = state_33081__$1;
(statearr_33084_33104[(1)] = (8));

} else {
var statearr_33085_33105 = state_33081__$1;
(statearr_33085_33105[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33082 === (1))){
var inst_33050 = cljs.core.vec.call(null,chs);
var inst_33051 = inst_33050;
var state_33081__$1 = (function (){var statearr_33086 = state_33081;
(statearr_33086[(10)] = inst_33051);

return statearr_33086;
})();
var statearr_33087_33106 = state_33081__$1;
(statearr_33087_33106[(2)] = null);

(statearr_33087_33106[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33082 === (4))){
var inst_33051 = (state_33081[(10)]);
var state_33081__$1 = state_33081;
return cljs.core.async.ioc_alts_BANG_.call(null,state_33081__$1,(7),inst_33051);
} else {
if((state_val_33082 === (6))){
var inst_33077 = (state_33081[(2)]);
var state_33081__$1 = state_33081;
var statearr_33088_33107 = state_33081__$1;
(statearr_33088_33107[(2)] = inst_33077);

(statearr_33088_33107[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33082 === (3))){
var inst_33079 = (state_33081[(2)]);
var state_33081__$1 = state_33081;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33081__$1,inst_33079);
} else {
if((state_val_33082 === (2))){
var inst_33051 = (state_33081[(10)]);
var inst_33053 = cljs.core.count.call(null,inst_33051);
var inst_33054 = (inst_33053 > (0));
var state_33081__$1 = state_33081;
if(cljs.core.truth_(inst_33054)){
var statearr_33090_33108 = state_33081__$1;
(statearr_33090_33108[(1)] = (4));

} else {
var statearr_33091_33109 = state_33081__$1;
(statearr_33091_33109[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33082 === (11))){
var inst_33051 = (state_33081[(10)]);
var inst_33070 = (state_33081[(2)]);
var tmp33089 = inst_33051;
var inst_33051__$1 = tmp33089;
var state_33081__$1 = (function (){var statearr_33092 = state_33081;
(statearr_33092[(10)] = inst_33051__$1);

(statearr_33092[(11)] = inst_33070);

return statearr_33092;
})();
var statearr_33093_33110 = state_33081__$1;
(statearr_33093_33110[(2)] = null);

(statearr_33093_33110[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33082 === (9))){
var inst_33061 = (state_33081[(7)]);
var state_33081__$1 = state_33081;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33081__$1,(11),out,inst_33061);
} else {
if((state_val_33082 === (5))){
var inst_33075 = cljs.core.async.close_BANG_.call(null,out);
var state_33081__$1 = state_33081;
var statearr_33094_33111 = state_33081__$1;
(statearr_33094_33111[(2)] = inst_33075);

(statearr_33094_33111[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33082 === (10))){
var inst_33073 = (state_33081[(2)]);
var state_33081__$1 = state_33081;
var statearr_33095_33112 = state_33081__$1;
(statearr_33095_33112[(2)] = inst_33073);

(statearr_33095_33112[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33082 === (8))){
var inst_33061 = (state_33081[(7)]);
var inst_33051 = (state_33081[(10)]);
var inst_33060 = (state_33081[(8)]);
var inst_33062 = (state_33081[(9)]);
var inst_33065 = (function (){var cs = inst_33051;
var vec__33056 = inst_33060;
var v = inst_33061;
var c = inst_33062;
return ((function (cs,vec__33056,v,c,inst_33061,inst_33051,inst_33060,inst_33062,state_val_33082,c__31704__auto___33103,out){
return (function (p1__33047_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__33047_SHARP_);
});
;})(cs,vec__33056,v,c,inst_33061,inst_33051,inst_33060,inst_33062,state_val_33082,c__31704__auto___33103,out))
})();
var inst_33066 = cljs.core.filterv.call(null,inst_33065,inst_33051);
var inst_33051__$1 = inst_33066;
var state_33081__$1 = (function (){var statearr_33096 = state_33081;
(statearr_33096[(10)] = inst_33051__$1);

return statearr_33096;
})();
var statearr_33097_33113 = state_33081__$1;
(statearr_33097_33113[(2)] = null);

(statearr_33097_33113[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___33103,out))
;
return ((function (switch__31616__auto__,c__31704__auto___33103,out){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_33098 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33098[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_33098[(1)] = (1));

return statearr_33098;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_33081){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33081);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33099){if((e33099 instanceof Object)){
var ex__31620__auto__ = e33099;
var statearr_33100_33114 = state_33081;
(statearr_33100_33114[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33081);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33099;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33115 = state_33081;
state_33081 = G__33115;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_33081){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_33081);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___33103,out))
})();
var state__31706__auto__ = (function (){var statearr_33101 = f__31705__auto__.call(null);
(statearr_33101[(6)] = c__31704__auto___33103);

return statearr_33101;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___33103,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__33117 = arguments.length;
switch (G__33117) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__31704__auto___33162 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___33162,out){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___33162,out){
return (function (state_33141){
var state_val_33142 = (state_33141[(1)]);
if((state_val_33142 === (7))){
var inst_33123 = (state_33141[(7)]);
var inst_33123__$1 = (state_33141[(2)]);
var inst_33124 = (inst_33123__$1 == null);
var inst_33125 = cljs.core.not.call(null,inst_33124);
var state_33141__$1 = (function (){var statearr_33143 = state_33141;
(statearr_33143[(7)] = inst_33123__$1);

return statearr_33143;
})();
if(inst_33125){
var statearr_33144_33163 = state_33141__$1;
(statearr_33144_33163[(1)] = (8));

} else {
var statearr_33145_33164 = state_33141__$1;
(statearr_33145_33164[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (1))){
var inst_33118 = (0);
var state_33141__$1 = (function (){var statearr_33146 = state_33141;
(statearr_33146[(8)] = inst_33118);

return statearr_33146;
})();
var statearr_33147_33165 = state_33141__$1;
(statearr_33147_33165[(2)] = null);

(statearr_33147_33165[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (4))){
var state_33141__$1 = state_33141;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33141__$1,(7),ch);
} else {
if((state_val_33142 === (6))){
var inst_33136 = (state_33141[(2)]);
var state_33141__$1 = state_33141;
var statearr_33148_33166 = state_33141__$1;
(statearr_33148_33166[(2)] = inst_33136);

(statearr_33148_33166[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (3))){
var inst_33138 = (state_33141[(2)]);
var inst_33139 = cljs.core.async.close_BANG_.call(null,out);
var state_33141__$1 = (function (){var statearr_33149 = state_33141;
(statearr_33149[(9)] = inst_33138);

return statearr_33149;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33141__$1,inst_33139);
} else {
if((state_val_33142 === (2))){
var inst_33118 = (state_33141[(8)]);
var inst_33120 = (inst_33118 < n);
var state_33141__$1 = state_33141;
if(cljs.core.truth_(inst_33120)){
var statearr_33150_33167 = state_33141__$1;
(statearr_33150_33167[(1)] = (4));

} else {
var statearr_33151_33168 = state_33141__$1;
(statearr_33151_33168[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (11))){
var inst_33118 = (state_33141[(8)]);
var inst_33128 = (state_33141[(2)]);
var inst_33129 = (inst_33118 + (1));
var inst_33118__$1 = inst_33129;
var state_33141__$1 = (function (){var statearr_33152 = state_33141;
(statearr_33152[(10)] = inst_33128);

(statearr_33152[(8)] = inst_33118__$1);

return statearr_33152;
})();
var statearr_33153_33169 = state_33141__$1;
(statearr_33153_33169[(2)] = null);

(statearr_33153_33169[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (9))){
var state_33141__$1 = state_33141;
var statearr_33154_33170 = state_33141__$1;
(statearr_33154_33170[(2)] = null);

(statearr_33154_33170[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (5))){
var state_33141__$1 = state_33141;
var statearr_33155_33171 = state_33141__$1;
(statearr_33155_33171[(2)] = null);

(statearr_33155_33171[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (10))){
var inst_33133 = (state_33141[(2)]);
var state_33141__$1 = state_33141;
var statearr_33156_33172 = state_33141__$1;
(statearr_33156_33172[(2)] = inst_33133);

(statearr_33156_33172[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33142 === (8))){
var inst_33123 = (state_33141[(7)]);
var state_33141__$1 = state_33141;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33141__$1,(11),out,inst_33123);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___33162,out))
;
return ((function (switch__31616__auto__,c__31704__auto___33162,out){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_33157 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33157[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_33157[(1)] = (1));

return statearr_33157;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_33141){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33141);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33158){if((e33158 instanceof Object)){
var ex__31620__auto__ = e33158;
var statearr_33159_33173 = state_33141;
(statearr_33159_33173[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33141);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33158;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33174 = state_33141;
state_33141 = G__33174;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_33141){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_33141);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___33162,out))
})();
var state__31706__auto__ = (function (){var statearr_33160 = f__31705__auto__.call(null);
(statearr_33160[(6)] = c__31704__auto___33162);

return statearr_33160;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___33162,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async33176 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33176 = (function (f,ch,meta33177){
this.f = f;
this.ch = ch;
this.meta33177 = meta33177;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33178,meta33177__$1){
var self__ = this;
var _33178__$1 = this;
return (new cljs.core.async.t_cljs$core$async33176(self__.f,self__.ch,meta33177__$1));
});

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33178){
var self__ = this;
var _33178__$1 = this;
return self__.meta33177;
});

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t_cljs$core$async33179 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33179 = (function (f,ch,meta33177,_,fn1,meta33180){
this.f = f;
this.ch = ch;
this.meta33177 = meta33177;
this._ = _;
this.fn1 = fn1;
this.meta33180 = meta33180;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33179.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_33181,meta33180__$1){
var self__ = this;
var _33181__$1 = this;
return (new cljs.core.async.t_cljs$core$async33179(self__.f,self__.ch,self__.meta33177,self__._,self__.fn1,meta33180__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async33179.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_33181){
var self__ = this;
var _33181__$1 = this;
return self__.meta33180;
});})(___$1))
;

cljs.core.async.t_cljs$core$async33179.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33179.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async33179.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async33179.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__33175_SHARP_){
return f1.call(null,(((p1__33175_SHARP_ == null))?null:self__.f.call(null,p1__33175_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async33179.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33177","meta33177",509770629,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async33176","cljs.core.async/t_cljs$core$async33176",-1610802918,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta33180","meta33180",-1081853356,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async33179.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33179.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33179";

cljs.core.async.t_cljs$core$async33179.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async33179");
});})(___$1))
;

cljs.core.async.__GT_t_cljs$core$async33179 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async33179(f__$1,ch__$1,meta33177__$1,___$2,fn1__$1,meta33180){
return (new cljs.core.async.t_cljs$core$async33179(f__$1,ch__$1,meta33177__$1,___$2,fn1__$1,meta33180));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async33179(self__.f,self__.ch,self__.meta33177,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__27957__auto__ = ret;
if(cljs.core.truth_(and__27957__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__27957__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33176.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async33176.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33177","meta33177",509770629,null)], null);
});

cljs.core.async.t_cljs$core$async33176.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33176.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33176";

cljs.core.async.t_cljs$core$async33176.cljs$lang$ctorPrWriter = (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async33176");
});

cljs.core.async.__GT_t_cljs$core$async33176 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async33176(f__$1,ch__$1,meta33177){
return (new cljs.core.async.t_cljs$core$async33176(f__$1,ch__$1,meta33177));
});

}

return (new cljs.core.async.t_cljs$core$async33176(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async33182 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33182 = (function (f,ch,meta33183){
this.f = f;
this.ch = ch;
this.meta33183 = meta33183;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33184,meta33183__$1){
var self__ = this;
var _33184__$1 = this;
return (new cljs.core.async.t_cljs$core$async33182(self__.f,self__.ch,meta33183__$1));
});

cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33184){
var self__ = this;
var _33184__$1 = this;
return self__.meta33183;
});

cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33182.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async33182.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33183","meta33183",986290405,null)], null);
});

cljs.core.async.t_cljs$core$async33182.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33182.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33182";

cljs.core.async.t_cljs$core$async33182.cljs$lang$ctorPrWriter = (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async33182");
});

cljs.core.async.__GT_t_cljs$core$async33182 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async33182(f__$1,ch__$1,meta33183){
return (new cljs.core.async.t_cljs$core$async33182(f__$1,ch__$1,meta33183));
});

}

return (new cljs.core.async.t_cljs$core$async33182(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t_cljs$core$async33185 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33185 = (function (p,ch,meta33186){
this.p = p;
this.ch = ch;
this.meta33186 = meta33186;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33187,meta33186__$1){
var self__ = this;
var _33187__$1 = this;
return (new cljs.core.async.t_cljs$core$async33185(self__.p,self__.ch,meta33186__$1));
});

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33187){
var self__ = this;
var _33187__$1 = this;
return self__.meta33186;
});

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33185.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async33185.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33186","meta33186",-165630468,null)], null);
});

cljs.core.async.t_cljs$core$async33185.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33185.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33185";

cljs.core.async.t_cljs$core$async33185.cljs$lang$ctorPrWriter = (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.core.async/t_cljs$core$async33185");
});

cljs.core.async.__GT_t_cljs$core$async33185 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async33185(p__$1,ch__$1,meta33186){
return (new cljs.core.async.t_cljs$core$async33185(p__$1,ch__$1,meta33186));
});

}

return (new cljs.core.async.t_cljs$core$async33185(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__33189 = arguments.length;
switch (G__33189) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__31704__auto___33229 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___33229,out){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___33229,out){
return (function (state_33210){
var state_val_33211 = (state_33210[(1)]);
if((state_val_33211 === (7))){
var inst_33206 = (state_33210[(2)]);
var state_33210__$1 = state_33210;
var statearr_33212_33230 = state_33210__$1;
(statearr_33212_33230[(2)] = inst_33206);

(statearr_33212_33230[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (1))){
var state_33210__$1 = state_33210;
var statearr_33213_33231 = state_33210__$1;
(statearr_33213_33231[(2)] = null);

(statearr_33213_33231[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (4))){
var inst_33192 = (state_33210[(7)]);
var inst_33192__$1 = (state_33210[(2)]);
var inst_33193 = (inst_33192__$1 == null);
var state_33210__$1 = (function (){var statearr_33214 = state_33210;
(statearr_33214[(7)] = inst_33192__$1);

return statearr_33214;
})();
if(cljs.core.truth_(inst_33193)){
var statearr_33215_33232 = state_33210__$1;
(statearr_33215_33232[(1)] = (5));

} else {
var statearr_33216_33233 = state_33210__$1;
(statearr_33216_33233[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (6))){
var inst_33192 = (state_33210[(7)]);
var inst_33197 = p.call(null,inst_33192);
var state_33210__$1 = state_33210;
if(cljs.core.truth_(inst_33197)){
var statearr_33217_33234 = state_33210__$1;
(statearr_33217_33234[(1)] = (8));

} else {
var statearr_33218_33235 = state_33210__$1;
(statearr_33218_33235[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (3))){
var inst_33208 = (state_33210[(2)]);
var state_33210__$1 = state_33210;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33210__$1,inst_33208);
} else {
if((state_val_33211 === (2))){
var state_33210__$1 = state_33210;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33210__$1,(4),ch);
} else {
if((state_val_33211 === (11))){
var inst_33200 = (state_33210[(2)]);
var state_33210__$1 = state_33210;
var statearr_33219_33236 = state_33210__$1;
(statearr_33219_33236[(2)] = inst_33200);

(statearr_33219_33236[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (9))){
var state_33210__$1 = state_33210;
var statearr_33220_33237 = state_33210__$1;
(statearr_33220_33237[(2)] = null);

(statearr_33220_33237[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (5))){
var inst_33195 = cljs.core.async.close_BANG_.call(null,out);
var state_33210__$1 = state_33210;
var statearr_33221_33238 = state_33210__$1;
(statearr_33221_33238[(2)] = inst_33195);

(statearr_33221_33238[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (10))){
var inst_33203 = (state_33210[(2)]);
var state_33210__$1 = (function (){var statearr_33222 = state_33210;
(statearr_33222[(8)] = inst_33203);

return statearr_33222;
})();
var statearr_33223_33239 = state_33210__$1;
(statearr_33223_33239[(2)] = null);

(statearr_33223_33239[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33211 === (8))){
var inst_33192 = (state_33210[(7)]);
var state_33210__$1 = state_33210;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33210__$1,(11),out,inst_33192);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___33229,out))
;
return ((function (switch__31616__auto__,c__31704__auto___33229,out){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_33224 = [null,null,null,null,null,null,null,null,null];
(statearr_33224[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_33224[(1)] = (1));

return statearr_33224;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_33210){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33210);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33225){if((e33225 instanceof Object)){
var ex__31620__auto__ = e33225;
var statearr_33226_33240 = state_33210;
(statearr_33226_33240[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33210);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33225;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33241 = state_33210;
state_33210 = G__33241;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_33210){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_33210);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___33229,out))
})();
var state__31706__auto__ = (function (){var statearr_33227 = f__31705__auto__.call(null);
(statearr_33227[(6)] = c__31704__auto___33229);

return statearr_33227;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___33229,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__33243 = arguments.length;
switch (G__33243) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__){
return (function (state_33306){
var state_val_33307 = (state_33306[(1)]);
if((state_val_33307 === (7))){
var inst_33302 = (state_33306[(2)]);
var state_33306__$1 = state_33306;
var statearr_33308_33346 = state_33306__$1;
(statearr_33308_33346[(2)] = inst_33302);

(statearr_33308_33346[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (20))){
var inst_33272 = (state_33306[(7)]);
var inst_33283 = (state_33306[(2)]);
var inst_33284 = cljs.core.next.call(null,inst_33272);
var inst_33258 = inst_33284;
var inst_33259 = null;
var inst_33260 = (0);
var inst_33261 = (0);
var state_33306__$1 = (function (){var statearr_33309 = state_33306;
(statearr_33309[(8)] = inst_33283);

(statearr_33309[(9)] = inst_33259);

(statearr_33309[(10)] = inst_33258);

(statearr_33309[(11)] = inst_33260);

(statearr_33309[(12)] = inst_33261);

return statearr_33309;
})();
var statearr_33310_33347 = state_33306__$1;
(statearr_33310_33347[(2)] = null);

(statearr_33310_33347[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (1))){
var state_33306__$1 = state_33306;
var statearr_33311_33348 = state_33306__$1;
(statearr_33311_33348[(2)] = null);

(statearr_33311_33348[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (4))){
var inst_33247 = (state_33306[(13)]);
var inst_33247__$1 = (state_33306[(2)]);
var inst_33248 = (inst_33247__$1 == null);
var state_33306__$1 = (function (){var statearr_33312 = state_33306;
(statearr_33312[(13)] = inst_33247__$1);

return statearr_33312;
})();
if(cljs.core.truth_(inst_33248)){
var statearr_33313_33349 = state_33306__$1;
(statearr_33313_33349[(1)] = (5));

} else {
var statearr_33314_33350 = state_33306__$1;
(statearr_33314_33350[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (15))){
var state_33306__$1 = state_33306;
var statearr_33318_33351 = state_33306__$1;
(statearr_33318_33351[(2)] = null);

(statearr_33318_33351[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (21))){
var state_33306__$1 = state_33306;
var statearr_33319_33352 = state_33306__$1;
(statearr_33319_33352[(2)] = null);

(statearr_33319_33352[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (13))){
var inst_33259 = (state_33306[(9)]);
var inst_33258 = (state_33306[(10)]);
var inst_33260 = (state_33306[(11)]);
var inst_33261 = (state_33306[(12)]);
var inst_33268 = (state_33306[(2)]);
var inst_33269 = (inst_33261 + (1));
var tmp33315 = inst_33259;
var tmp33316 = inst_33258;
var tmp33317 = inst_33260;
var inst_33258__$1 = tmp33316;
var inst_33259__$1 = tmp33315;
var inst_33260__$1 = tmp33317;
var inst_33261__$1 = inst_33269;
var state_33306__$1 = (function (){var statearr_33320 = state_33306;
(statearr_33320[(9)] = inst_33259__$1);

(statearr_33320[(14)] = inst_33268);

(statearr_33320[(10)] = inst_33258__$1);

(statearr_33320[(11)] = inst_33260__$1);

(statearr_33320[(12)] = inst_33261__$1);

return statearr_33320;
})();
var statearr_33321_33353 = state_33306__$1;
(statearr_33321_33353[(2)] = null);

(statearr_33321_33353[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (22))){
var state_33306__$1 = state_33306;
var statearr_33322_33354 = state_33306__$1;
(statearr_33322_33354[(2)] = null);

(statearr_33322_33354[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (6))){
var inst_33247 = (state_33306[(13)]);
var inst_33256 = f.call(null,inst_33247);
var inst_33257 = cljs.core.seq.call(null,inst_33256);
var inst_33258 = inst_33257;
var inst_33259 = null;
var inst_33260 = (0);
var inst_33261 = (0);
var state_33306__$1 = (function (){var statearr_33323 = state_33306;
(statearr_33323[(9)] = inst_33259);

(statearr_33323[(10)] = inst_33258);

(statearr_33323[(11)] = inst_33260);

(statearr_33323[(12)] = inst_33261);

return statearr_33323;
})();
var statearr_33324_33355 = state_33306__$1;
(statearr_33324_33355[(2)] = null);

(statearr_33324_33355[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (17))){
var inst_33272 = (state_33306[(7)]);
var inst_33276 = cljs.core.chunk_first.call(null,inst_33272);
var inst_33277 = cljs.core.chunk_rest.call(null,inst_33272);
var inst_33278 = cljs.core.count.call(null,inst_33276);
var inst_33258 = inst_33277;
var inst_33259 = inst_33276;
var inst_33260 = inst_33278;
var inst_33261 = (0);
var state_33306__$1 = (function (){var statearr_33325 = state_33306;
(statearr_33325[(9)] = inst_33259);

(statearr_33325[(10)] = inst_33258);

(statearr_33325[(11)] = inst_33260);

(statearr_33325[(12)] = inst_33261);

return statearr_33325;
})();
var statearr_33326_33356 = state_33306__$1;
(statearr_33326_33356[(2)] = null);

(statearr_33326_33356[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (3))){
var inst_33304 = (state_33306[(2)]);
var state_33306__$1 = state_33306;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33306__$1,inst_33304);
} else {
if((state_val_33307 === (12))){
var inst_33292 = (state_33306[(2)]);
var state_33306__$1 = state_33306;
var statearr_33327_33357 = state_33306__$1;
(statearr_33327_33357[(2)] = inst_33292);

(statearr_33327_33357[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (2))){
var state_33306__$1 = state_33306;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33306__$1,(4),in$);
} else {
if((state_val_33307 === (23))){
var inst_33300 = (state_33306[(2)]);
var state_33306__$1 = state_33306;
var statearr_33328_33358 = state_33306__$1;
(statearr_33328_33358[(2)] = inst_33300);

(statearr_33328_33358[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (19))){
var inst_33287 = (state_33306[(2)]);
var state_33306__$1 = state_33306;
var statearr_33329_33359 = state_33306__$1;
(statearr_33329_33359[(2)] = inst_33287);

(statearr_33329_33359[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (11))){
var inst_33272 = (state_33306[(7)]);
var inst_33258 = (state_33306[(10)]);
var inst_33272__$1 = cljs.core.seq.call(null,inst_33258);
var state_33306__$1 = (function (){var statearr_33330 = state_33306;
(statearr_33330[(7)] = inst_33272__$1);

return statearr_33330;
})();
if(inst_33272__$1){
var statearr_33331_33360 = state_33306__$1;
(statearr_33331_33360[(1)] = (14));

} else {
var statearr_33332_33361 = state_33306__$1;
(statearr_33332_33361[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (9))){
var inst_33294 = (state_33306[(2)]);
var inst_33295 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_33306__$1 = (function (){var statearr_33333 = state_33306;
(statearr_33333[(15)] = inst_33294);

return statearr_33333;
})();
if(cljs.core.truth_(inst_33295)){
var statearr_33334_33362 = state_33306__$1;
(statearr_33334_33362[(1)] = (21));

} else {
var statearr_33335_33363 = state_33306__$1;
(statearr_33335_33363[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (5))){
var inst_33250 = cljs.core.async.close_BANG_.call(null,out);
var state_33306__$1 = state_33306;
var statearr_33336_33364 = state_33306__$1;
(statearr_33336_33364[(2)] = inst_33250);

(statearr_33336_33364[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (14))){
var inst_33272 = (state_33306[(7)]);
var inst_33274 = cljs.core.chunked_seq_QMARK_.call(null,inst_33272);
var state_33306__$1 = state_33306;
if(inst_33274){
var statearr_33337_33365 = state_33306__$1;
(statearr_33337_33365[(1)] = (17));

} else {
var statearr_33338_33366 = state_33306__$1;
(statearr_33338_33366[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (16))){
var inst_33290 = (state_33306[(2)]);
var state_33306__$1 = state_33306;
var statearr_33339_33367 = state_33306__$1;
(statearr_33339_33367[(2)] = inst_33290);

(statearr_33339_33367[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33307 === (10))){
var inst_33259 = (state_33306[(9)]);
var inst_33261 = (state_33306[(12)]);
var inst_33266 = cljs.core._nth.call(null,inst_33259,inst_33261);
var state_33306__$1 = state_33306;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33306__$1,(13),out,inst_33266);
} else {
if((state_val_33307 === (18))){
var inst_33272 = (state_33306[(7)]);
var inst_33281 = cljs.core.first.call(null,inst_33272);
var state_33306__$1 = state_33306;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33306__$1,(20),out,inst_33281);
} else {
if((state_val_33307 === (8))){
var inst_33260 = (state_33306[(11)]);
var inst_33261 = (state_33306[(12)]);
var inst_33263 = (inst_33261 < inst_33260);
var inst_33264 = inst_33263;
var state_33306__$1 = state_33306;
if(cljs.core.truth_(inst_33264)){
var statearr_33340_33368 = state_33306__$1;
(statearr_33340_33368[(1)] = (10));

} else {
var statearr_33341_33369 = state_33306__$1;
(statearr_33341_33369[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto__))
;
return ((function (switch__31616__auto__,c__31704__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__31617__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__31617__auto____0 = (function (){
var statearr_33342 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33342[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__31617__auto__);

(statearr_33342[(1)] = (1));

return statearr_33342;
});
var cljs$core$async$mapcat_STAR__$_state_machine__31617__auto____1 = (function (state_33306){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33306);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33343){if((e33343 instanceof Object)){
var ex__31620__auto__ = e33343;
var statearr_33344_33370 = state_33306;
(statearr_33344_33370[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33306);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33343;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33371 = state_33306;
state_33306 = G__33371;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__31617__auto__ = function(state_33306){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__31617__auto____1.call(this,state_33306);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mapcat_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__31617__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__31617__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__))
})();
var state__31706__auto__ = (function (){var statearr_33345 = f__31705__auto__.call(null);
(statearr_33345[(6)] = c__31704__auto__);

return statearr_33345;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__))
);

return c__31704__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__33373 = arguments.length;
switch (G__33373) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__33376 = arguments.length;
switch (G__33376) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__33379 = arguments.length;
switch (G__33379) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__31704__auto___33426 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___33426,out){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___33426,out){
return (function (state_33403){
var state_val_33404 = (state_33403[(1)]);
if((state_val_33404 === (7))){
var inst_33398 = (state_33403[(2)]);
var state_33403__$1 = state_33403;
var statearr_33405_33427 = state_33403__$1;
(statearr_33405_33427[(2)] = inst_33398);

(statearr_33405_33427[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33404 === (1))){
var inst_33380 = null;
var state_33403__$1 = (function (){var statearr_33406 = state_33403;
(statearr_33406[(7)] = inst_33380);

return statearr_33406;
})();
var statearr_33407_33428 = state_33403__$1;
(statearr_33407_33428[(2)] = null);

(statearr_33407_33428[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33404 === (4))){
var inst_33383 = (state_33403[(8)]);
var inst_33383__$1 = (state_33403[(2)]);
var inst_33384 = (inst_33383__$1 == null);
var inst_33385 = cljs.core.not.call(null,inst_33384);
var state_33403__$1 = (function (){var statearr_33408 = state_33403;
(statearr_33408[(8)] = inst_33383__$1);

return statearr_33408;
})();
if(inst_33385){
var statearr_33409_33429 = state_33403__$1;
(statearr_33409_33429[(1)] = (5));

} else {
var statearr_33410_33430 = state_33403__$1;
(statearr_33410_33430[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33404 === (6))){
var state_33403__$1 = state_33403;
var statearr_33411_33431 = state_33403__$1;
(statearr_33411_33431[(2)] = null);

(statearr_33411_33431[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33404 === (3))){
var inst_33400 = (state_33403[(2)]);
var inst_33401 = cljs.core.async.close_BANG_.call(null,out);
var state_33403__$1 = (function (){var statearr_33412 = state_33403;
(statearr_33412[(9)] = inst_33400);

return statearr_33412;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33403__$1,inst_33401);
} else {
if((state_val_33404 === (2))){
var state_33403__$1 = state_33403;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33403__$1,(4),ch);
} else {
if((state_val_33404 === (11))){
var inst_33383 = (state_33403[(8)]);
var inst_33392 = (state_33403[(2)]);
var inst_33380 = inst_33383;
var state_33403__$1 = (function (){var statearr_33413 = state_33403;
(statearr_33413[(10)] = inst_33392);

(statearr_33413[(7)] = inst_33380);

return statearr_33413;
})();
var statearr_33414_33432 = state_33403__$1;
(statearr_33414_33432[(2)] = null);

(statearr_33414_33432[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33404 === (9))){
var inst_33383 = (state_33403[(8)]);
var state_33403__$1 = state_33403;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33403__$1,(11),out,inst_33383);
} else {
if((state_val_33404 === (5))){
var inst_33383 = (state_33403[(8)]);
var inst_33380 = (state_33403[(7)]);
var inst_33387 = cljs.core._EQ_.call(null,inst_33383,inst_33380);
var state_33403__$1 = state_33403;
if(inst_33387){
var statearr_33416_33433 = state_33403__$1;
(statearr_33416_33433[(1)] = (8));

} else {
var statearr_33417_33434 = state_33403__$1;
(statearr_33417_33434[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33404 === (10))){
var inst_33395 = (state_33403[(2)]);
var state_33403__$1 = state_33403;
var statearr_33418_33435 = state_33403__$1;
(statearr_33418_33435[(2)] = inst_33395);

(statearr_33418_33435[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33404 === (8))){
var inst_33380 = (state_33403[(7)]);
var tmp33415 = inst_33380;
var inst_33380__$1 = tmp33415;
var state_33403__$1 = (function (){var statearr_33419 = state_33403;
(statearr_33419[(7)] = inst_33380__$1);

return statearr_33419;
})();
var statearr_33420_33436 = state_33403__$1;
(statearr_33420_33436[(2)] = null);

(statearr_33420_33436[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___33426,out))
;
return ((function (switch__31616__auto__,c__31704__auto___33426,out){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_33421 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33421[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_33421[(1)] = (1));

return statearr_33421;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_33403){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33403);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33422){if((e33422 instanceof Object)){
var ex__31620__auto__ = e33422;
var statearr_33423_33437 = state_33403;
(statearr_33423_33437[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33403);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33422;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33438 = state_33403;
state_33403 = G__33438;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_33403){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_33403);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___33426,out))
})();
var state__31706__auto__ = (function (){var statearr_33424 = f__31705__auto__.call(null);
(statearr_33424[(6)] = c__31704__auto___33426);

return statearr_33424;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___33426,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__33440 = arguments.length;
switch (G__33440) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__31704__auto___33506 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___33506,out){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___33506,out){
return (function (state_33478){
var state_val_33479 = (state_33478[(1)]);
if((state_val_33479 === (7))){
var inst_33474 = (state_33478[(2)]);
var state_33478__$1 = state_33478;
var statearr_33480_33507 = state_33478__$1;
(statearr_33480_33507[(2)] = inst_33474);

(statearr_33480_33507[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (1))){
var inst_33441 = (new Array(n));
var inst_33442 = inst_33441;
var inst_33443 = (0);
var state_33478__$1 = (function (){var statearr_33481 = state_33478;
(statearr_33481[(7)] = inst_33443);

(statearr_33481[(8)] = inst_33442);

return statearr_33481;
})();
var statearr_33482_33508 = state_33478__$1;
(statearr_33482_33508[(2)] = null);

(statearr_33482_33508[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (4))){
var inst_33446 = (state_33478[(9)]);
var inst_33446__$1 = (state_33478[(2)]);
var inst_33447 = (inst_33446__$1 == null);
var inst_33448 = cljs.core.not.call(null,inst_33447);
var state_33478__$1 = (function (){var statearr_33483 = state_33478;
(statearr_33483[(9)] = inst_33446__$1);

return statearr_33483;
})();
if(inst_33448){
var statearr_33484_33509 = state_33478__$1;
(statearr_33484_33509[(1)] = (5));

} else {
var statearr_33485_33510 = state_33478__$1;
(statearr_33485_33510[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (15))){
var inst_33468 = (state_33478[(2)]);
var state_33478__$1 = state_33478;
var statearr_33486_33511 = state_33478__$1;
(statearr_33486_33511[(2)] = inst_33468);

(statearr_33486_33511[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (13))){
var state_33478__$1 = state_33478;
var statearr_33487_33512 = state_33478__$1;
(statearr_33487_33512[(2)] = null);

(statearr_33487_33512[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (6))){
var inst_33443 = (state_33478[(7)]);
var inst_33464 = (inst_33443 > (0));
var state_33478__$1 = state_33478;
if(cljs.core.truth_(inst_33464)){
var statearr_33488_33513 = state_33478__$1;
(statearr_33488_33513[(1)] = (12));

} else {
var statearr_33489_33514 = state_33478__$1;
(statearr_33489_33514[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (3))){
var inst_33476 = (state_33478[(2)]);
var state_33478__$1 = state_33478;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33478__$1,inst_33476);
} else {
if((state_val_33479 === (12))){
var inst_33442 = (state_33478[(8)]);
var inst_33466 = cljs.core.vec.call(null,inst_33442);
var state_33478__$1 = state_33478;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33478__$1,(15),out,inst_33466);
} else {
if((state_val_33479 === (2))){
var state_33478__$1 = state_33478;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33478__$1,(4),ch);
} else {
if((state_val_33479 === (11))){
var inst_33458 = (state_33478[(2)]);
var inst_33459 = (new Array(n));
var inst_33442 = inst_33459;
var inst_33443 = (0);
var state_33478__$1 = (function (){var statearr_33490 = state_33478;
(statearr_33490[(7)] = inst_33443);

(statearr_33490[(8)] = inst_33442);

(statearr_33490[(10)] = inst_33458);

return statearr_33490;
})();
var statearr_33491_33515 = state_33478__$1;
(statearr_33491_33515[(2)] = null);

(statearr_33491_33515[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (9))){
var inst_33442 = (state_33478[(8)]);
var inst_33456 = cljs.core.vec.call(null,inst_33442);
var state_33478__$1 = state_33478;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33478__$1,(11),out,inst_33456);
} else {
if((state_val_33479 === (5))){
var inst_33443 = (state_33478[(7)]);
var inst_33451 = (state_33478[(11)]);
var inst_33442 = (state_33478[(8)]);
var inst_33446 = (state_33478[(9)]);
var inst_33450 = (inst_33442[inst_33443] = inst_33446);
var inst_33451__$1 = (inst_33443 + (1));
var inst_33452 = (inst_33451__$1 < n);
var state_33478__$1 = (function (){var statearr_33492 = state_33478;
(statearr_33492[(11)] = inst_33451__$1);

(statearr_33492[(12)] = inst_33450);

return statearr_33492;
})();
if(cljs.core.truth_(inst_33452)){
var statearr_33493_33516 = state_33478__$1;
(statearr_33493_33516[(1)] = (8));

} else {
var statearr_33494_33517 = state_33478__$1;
(statearr_33494_33517[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (14))){
var inst_33471 = (state_33478[(2)]);
var inst_33472 = cljs.core.async.close_BANG_.call(null,out);
var state_33478__$1 = (function (){var statearr_33496 = state_33478;
(statearr_33496[(13)] = inst_33471);

return statearr_33496;
})();
var statearr_33497_33518 = state_33478__$1;
(statearr_33497_33518[(2)] = inst_33472);

(statearr_33497_33518[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (10))){
var inst_33462 = (state_33478[(2)]);
var state_33478__$1 = state_33478;
var statearr_33498_33519 = state_33478__$1;
(statearr_33498_33519[(2)] = inst_33462);

(statearr_33498_33519[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33479 === (8))){
var inst_33451 = (state_33478[(11)]);
var inst_33442 = (state_33478[(8)]);
var tmp33495 = inst_33442;
var inst_33442__$1 = tmp33495;
var inst_33443 = inst_33451;
var state_33478__$1 = (function (){var statearr_33499 = state_33478;
(statearr_33499[(7)] = inst_33443);

(statearr_33499[(8)] = inst_33442__$1);

return statearr_33499;
})();
var statearr_33500_33520 = state_33478__$1;
(statearr_33500_33520[(2)] = null);

(statearr_33500_33520[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___33506,out))
;
return ((function (switch__31616__auto__,c__31704__auto___33506,out){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_33501 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33501[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_33501[(1)] = (1));

return statearr_33501;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_33478){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33478);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33502){if((e33502 instanceof Object)){
var ex__31620__auto__ = e33502;
var statearr_33503_33521 = state_33478;
(statearr_33503_33521[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33478);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33502;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33522 = state_33478;
state_33478 = G__33522;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_33478){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_33478);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___33506,out))
})();
var state__31706__auto__ = (function (){var statearr_33504 = f__31705__auto__.call(null);
(statearr_33504[(6)] = c__31704__auto___33506);

return statearr_33504;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___33506,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__33524 = arguments.length;
switch (G__33524) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__31704__auto___33594 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___33594,out){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___33594,out){
return (function (state_33566){
var state_val_33567 = (state_33566[(1)]);
if((state_val_33567 === (7))){
var inst_33562 = (state_33566[(2)]);
var state_33566__$1 = state_33566;
var statearr_33568_33595 = state_33566__$1;
(statearr_33568_33595[(2)] = inst_33562);

(statearr_33568_33595[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (1))){
var inst_33525 = [];
var inst_33526 = inst_33525;
var inst_33527 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_33566__$1 = (function (){var statearr_33569 = state_33566;
(statearr_33569[(7)] = inst_33526);

(statearr_33569[(8)] = inst_33527);

return statearr_33569;
})();
var statearr_33570_33596 = state_33566__$1;
(statearr_33570_33596[(2)] = null);

(statearr_33570_33596[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (4))){
var inst_33530 = (state_33566[(9)]);
var inst_33530__$1 = (state_33566[(2)]);
var inst_33531 = (inst_33530__$1 == null);
var inst_33532 = cljs.core.not.call(null,inst_33531);
var state_33566__$1 = (function (){var statearr_33571 = state_33566;
(statearr_33571[(9)] = inst_33530__$1);

return statearr_33571;
})();
if(inst_33532){
var statearr_33572_33597 = state_33566__$1;
(statearr_33572_33597[(1)] = (5));

} else {
var statearr_33573_33598 = state_33566__$1;
(statearr_33573_33598[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (15))){
var inst_33556 = (state_33566[(2)]);
var state_33566__$1 = state_33566;
var statearr_33574_33599 = state_33566__$1;
(statearr_33574_33599[(2)] = inst_33556);

(statearr_33574_33599[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (13))){
var state_33566__$1 = state_33566;
var statearr_33575_33600 = state_33566__$1;
(statearr_33575_33600[(2)] = null);

(statearr_33575_33600[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (6))){
var inst_33526 = (state_33566[(7)]);
var inst_33551 = inst_33526.length;
var inst_33552 = (inst_33551 > (0));
var state_33566__$1 = state_33566;
if(cljs.core.truth_(inst_33552)){
var statearr_33576_33601 = state_33566__$1;
(statearr_33576_33601[(1)] = (12));

} else {
var statearr_33577_33602 = state_33566__$1;
(statearr_33577_33602[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (3))){
var inst_33564 = (state_33566[(2)]);
var state_33566__$1 = state_33566;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33566__$1,inst_33564);
} else {
if((state_val_33567 === (12))){
var inst_33526 = (state_33566[(7)]);
var inst_33554 = cljs.core.vec.call(null,inst_33526);
var state_33566__$1 = state_33566;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33566__$1,(15),out,inst_33554);
} else {
if((state_val_33567 === (2))){
var state_33566__$1 = state_33566;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33566__$1,(4),ch);
} else {
if((state_val_33567 === (11))){
var inst_33534 = (state_33566[(10)]);
var inst_33530 = (state_33566[(9)]);
var inst_33544 = (state_33566[(2)]);
var inst_33545 = [];
var inst_33546 = inst_33545.push(inst_33530);
var inst_33526 = inst_33545;
var inst_33527 = inst_33534;
var state_33566__$1 = (function (){var statearr_33578 = state_33566;
(statearr_33578[(7)] = inst_33526);

(statearr_33578[(8)] = inst_33527);

(statearr_33578[(11)] = inst_33544);

(statearr_33578[(12)] = inst_33546);

return statearr_33578;
})();
var statearr_33579_33603 = state_33566__$1;
(statearr_33579_33603[(2)] = null);

(statearr_33579_33603[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (9))){
var inst_33526 = (state_33566[(7)]);
var inst_33542 = cljs.core.vec.call(null,inst_33526);
var state_33566__$1 = state_33566;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33566__$1,(11),out,inst_33542);
} else {
if((state_val_33567 === (5))){
var inst_33527 = (state_33566[(8)]);
var inst_33534 = (state_33566[(10)]);
var inst_33530 = (state_33566[(9)]);
var inst_33534__$1 = f.call(null,inst_33530);
var inst_33535 = cljs.core._EQ_.call(null,inst_33534__$1,inst_33527);
var inst_33536 = cljs.core.keyword_identical_QMARK_.call(null,inst_33527,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_33537 = (inst_33535) || (inst_33536);
var state_33566__$1 = (function (){var statearr_33580 = state_33566;
(statearr_33580[(10)] = inst_33534__$1);

return statearr_33580;
})();
if(cljs.core.truth_(inst_33537)){
var statearr_33581_33604 = state_33566__$1;
(statearr_33581_33604[(1)] = (8));

} else {
var statearr_33582_33605 = state_33566__$1;
(statearr_33582_33605[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (14))){
var inst_33559 = (state_33566[(2)]);
var inst_33560 = cljs.core.async.close_BANG_.call(null,out);
var state_33566__$1 = (function (){var statearr_33584 = state_33566;
(statearr_33584[(13)] = inst_33559);

return statearr_33584;
})();
var statearr_33585_33606 = state_33566__$1;
(statearr_33585_33606[(2)] = inst_33560);

(statearr_33585_33606[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (10))){
var inst_33549 = (state_33566[(2)]);
var state_33566__$1 = state_33566;
var statearr_33586_33607 = state_33566__$1;
(statearr_33586_33607[(2)] = inst_33549);

(statearr_33586_33607[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33567 === (8))){
var inst_33526 = (state_33566[(7)]);
var inst_33534 = (state_33566[(10)]);
var inst_33530 = (state_33566[(9)]);
var inst_33539 = inst_33526.push(inst_33530);
var tmp33583 = inst_33526;
var inst_33526__$1 = tmp33583;
var inst_33527 = inst_33534;
var state_33566__$1 = (function (){var statearr_33587 = state_33566;
(statearr_33587[(14)] = inst_33539);

(statearr_33587[(7)] = inst_33526__$1);

(statearr_33587[(8)] = inst_33527);

return statearr_33587;
})();
var statearr_33588_33608 = state_33566__$1;
(statearr_33588_33608[(2)] = null);

(statearr_33588_33608[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___33594,out))
;
return ((function (switch__31616__auto__,c__31704__auto___33594,out){
return (function() {
var cljs$core$async$state_machine__31617__auto__ = null;
var cljs$core$async$state_machine__31617__auto____0 = (function (){
var statearr_33589 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33589[(0)] = cljs$core$async$state_machine__31617__auto__);

(statearr_33589[(1)] = (1));

return statearr_33589;
});
var cljs$core$async$state_machine__31617__auto____1 = (function (state_33566){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_33566);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e33590){if((e33590 instanceof Object)){
var ex__31620__auto__ = e33590;
var statearr_33591_33609 = state_33566;
(statearr_33591_33609[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33566);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33590;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33610 = state_33566;
state_33566 = G__33610;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
cljs$core$async$state_machine__31617__auto__ = function(state_33566){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__31617__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__31617__auto____1.call(this,state_33566);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__31617__auto____0;
cljs$core$async$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__31617__auto____1;
return cljs$core$async$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___33594,out))
})();
var state__31706__auto__ = (function (){var statearr_33592 = f__31705__auto__.call(null);
(statearr_33592[(6)] = c__31704__auto___33594);

return statearr_33592;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___33594,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=async.js.map?rel=1503198187748
