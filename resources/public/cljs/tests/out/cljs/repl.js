// Compiled by ClojureScript 1.9.854 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__37348){
var map__37349 = p__37348;
var map__37349__$1 = ((((!((map__37349 == null)))?((((map__37349.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37349.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37349):map__37349);
var m = map__37349__$1;
var n = cljs.core.get.call(null,map__37349__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__37349__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var temp__4657__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4657__auto__)){
var ns = temp__4657__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),cljs.core.str.cljs$core$IFn$_invoke$arity$1("/")].join('');
} else {
return null;
}
})()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__37351_37373 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__37352_37374 = null;
var count__37353_37375 = (0);
var i__37354_37376 = (0);
while(true){
if((i__37354_37376 < count__37353_37375)){
var f_37377 = cljs.core._nth.call(null,chunk__37352_37374,i__37354_37376);
cljs.core.println.call(null,"  ",f_37377);

var G__37378 = seq__37351_37373;
var G__37379 = chunk__37352_37374;
var G__37380 = count__37353_37375;
var G__37381 = (i__37354_37376 + (1));
seq__37351_37373 = G__37378;
chunk__37352_37374 = G__37379;
count__37353_37375 = G__37380;
i__37354_37376 = G__37381;
continue;
} else {
var temp__4657__auto___37382 = cljs.core.seq.call(null,seq__37351_37373);
if(temp__4657__auto___37382){
var seq__37351_37383__$1 = temp__4657__auto___37382;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37351_37383__$1)){
var c__28803__auto___37384 = cljs.core.chunk_first.call(null,seq__37351_37383__$1);
var G__37385 = cljs.core.chunk_rest.call(null,seq__37351_37383__$1);
var G__37386 = c__28803__auto___37384;
var G__37387 = cljs.core.count.call(null,c__28803__auto___37384);
var G__37388 = (0);
seq__37351_37373 = G__37385;
chunk__37352_37374 = G__37386;
count__37353_37375 = G__37387;
i__37354_37376 = G__37388;
continue;
} else {
var f_37389 = cljs.core.first.call(null,seq__37351_37383__$1);
cljs.core.println.call(null,"  ",f_37389);

var G__37390 = cljs.core.next.call(null,seq__37351_37383__$1);
var G__37391 = null;
var G__37392 = (0);
var G__37393 = (0);
seq__37351_37373 = G__37390;
chunk__37352_37374 = G__37391;
count__37353_37375 = G__37392;
i__37354_37376 = G__37393;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_37394 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__27969__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_37394);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_37394)))?cljs.core.second.call(null,arglists_37394):arglists_37394));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n  Please see http://clojure.org/"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n  Please see http://clojure.org/special_forms#"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__37355_37395 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__37356_37396 = null;
var count__37357_37397 = (0);
var i__37358_37398 = (0);
while(true){
if((i__37358_37398 < count__37357_37397)){
var vec__37359_37399 = cljs.core._nth.call(null,chunk__37356_37396,i__37358_37398);
var name_37400 = cljs.core.nth.call(null,vec__37359_37399,(0),null);
var map__37362_37401 = cljs.core.nth.call(null,vec__37359_37399,(1),null);
var map__37362_37402__$1 = ((((!((map__37362_37401 == null)))?((((map__37362_37401.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37362_37401.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37362_37401):map__37362_37401);
var doc_37403 = cljs.core.get.call(null,map__37362_37402__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_37404 = cljs.core.get.call(null,map__37362_37402__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_37400);

cljs.core.println.call(null," ",arglists_37404);

if(cljs.core.truth_(doc_37403)){
cljs.core.println.call(null," ",doc_37403);
} else {
}

var G__37405 = seq__37355_37395;
var G__37406 = chunk__37356_37396;
var G__37407 = count__37357_37397;
var G__37408 = (i__37358_37398 + (1));
seq__37355_37395 = G__37405;
chunk__37356_37396 = G__37406;
count__37357_37397 = G__37407;
i__37358_37398 = G__37408;
continue;
} else {
var temp__4657__auto___37409 = cljs.core.seq.call(null,seq__37355_37395);
if(temp__4657__auto___37409){
var seq__37355_37410__$1 = temp__4657__auto___37409;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37355_37410__$1)){
var c__28803__auto___37411 = cljs.core.chunk_first.call(null,seq__37355_37410__$1);
var G__37412 = cljs.core.chunk_rest.call(null,seq__37355_37410__$1);
var G__37413 = c__28803__auto___37411;
var G__37414 = cljs.core.count.call(null,c__28803__auto___37411);
var G__37415 = (0);
seq__37355_37395 = G__37412;
chunk__37356_37396 = G__37413;
count__37357_37397 = G__37414;
i__37358_37398 = G__37415;
continue;
} else {
var vec__37364_37416 = cljs.core.first.call(null,seq__37355_37410__$1);
var name_37417 = cljs.core.nth.call(null,vec__37364_37416,(0),null);
var map__37367_37418 = cljs.core.nth.call(null,vec__37364_37416,(1),null);
var map__37367_37419__$1 = ((((!((map__37367_37418 == null)))?((((map__37367_37418.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37367_37418.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37367_37418):map__37367_37418);
var doc_37420 = cljs.core.get.call(null,map__37367_37419__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_37421 = cljs.core.get.call(null,map__37367_37419__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_37417);

cljs.core.println.call(null," ",arglists_37421);

if(cljs.core.truth_(doc_37420)){
cljs.core.println.call(null," ",doc_37420);
} else {
}

var G__37422 = cljs.core.next.call(null,seq__37355_37410__$1);
var G__37423 = null;
var G__37424 = (0);
var G__37425 = (0);
seq__37355_37395 = G__37422;
chunk__37356_37396 = G__37423;
count__37357_37397 = G__37424;
i__37358_37398 = G__37425;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__4657__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__4657__auto__)){
var fnspec = temp__4657__auto__;
cljs.core.print.call(null,"Spec");

var seq__37369 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__37370 = null;
var count__37371 = (0);
var i__37372 = (0);
while(true){
if((i__37372 < count__37371)){
var role = cljs.core._nth.call(null,chunk__37370,i__37372);
var temp__4657__auto___37426__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__4657__auto___37426__$1)){
var spec_37427 = temp__4657__auto___37426__$1;
cljs.core.print.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(":")].join(''),cljs.spec.alpha.describe.call(null,spec_37427));
} else {
}

var G__37428 = seq__37369;
var G__37429 = chunk__37370;
var G__37430 = count__37371;
var G__37431 = (i__37372 + (1));
seq__37369 = G__37428;
chunk__37370 = G__37429;
count__37371 = G__37430;
i__37372 = G__37431;
continue;
} else {
var temp__4657__auto____$1 = cljs.core.seq.call(null,seq__37369);
if(temp__4657__auto____$1){
var seq__37369__$1 = temp__4657__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__37369__$1)){
var c__28803__auto__ = cljs.core.chunk_first.call(null,seq__37369__$1);
var G__37432 = cljs.core.chunk_rest.call(null,seq__37369__$1);
var G__37433 = c__28803__auto__;
var G__37434 = cljs.core.count.call(null,c__28803__auto__);
var G__37435 = (0);
seq__37369 = G__37432;
chunk__37370 = G__37433;
count__37371 = G__37434;
i__37372 = G__37435;
continue;
} else {
var role = cljs.core.first.call(null,seq__37369__$1);
var temp__4657__auto___37436__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__4657__auto___37436__$2)){
var spec_37437 = temp__4657__auto___37436__$2;
cljs.core.print.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(":")].join(''),cljs.spec.alpha.describe.call(null,spec_37437));
} else {
}

var G__37438 = cljs.core.next.call(null,seq__37369__$1);
var G__37439 = null;
var G__37440 = (0);
var G__37441 = (0);
seq__37369 = G__37438;
chunk__37370 = G__37439;
count__37371 = G__37440;
i__37372 = G__37441;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map?rel=1503198194784
