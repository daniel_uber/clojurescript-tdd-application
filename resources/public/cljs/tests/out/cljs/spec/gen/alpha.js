// Compiled by ClojureScript 1.9.854 {}
goog.provide('cljs.spec.gen.alpha');
goog.require('cljs.core');
goog.require('cljs.core');

/**
* @constructor
 * @implements {cljs.core.IDeref}
*/
cljs.spec.gen.alpha.LazyVar = (function (f,cached){
this.f = f;
this.cached = cached;
this.cljs$lang$protocol_mask$partition0$ = 32768;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.spec.gen.alpha.LazyVar.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
if(!((self__.cached == null))){
return self__.cached;
} else {
var x = self__.f.call(null);
if((x == null)){
} else {
self__.cached = x;
}

return x;
}
});

cljs.spec.gen.alpha.LazyVar.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),cljs.core.with_meta(new cljs.core.Symbol(null,"cached","cached",-1216707864,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"mutable","mutable",875778266),true], null))], null);
});

cljs.spec.gen.alpha.LazyVar.cljs$lang$type = true;

cljs.spec.gen.alpha.LazyVar.cljs$lang$ctorStr = "cljs.spec.gen.alpha/LazyVar";

cljs.spec.gen.alpha.LazyVar.cljs$lang$ctorPrWriter = (function (this__28594__auto__,writer__28595__auto__,opt__28596__auto__){
return cljs.core._write.call(null,writer__28595__auto__,"cljs.spec.gen.alpha/LazyVar");
});

cljs.spec.gen.alpha.__GT_LazyVar = (function cljs$spec$gen$alpha$__GT_LazyVar(f,cached){
return (new cljs.spec.gen.alpha.LazyVar(f,cached));
});

cljs.spec.gen.alpha.quick_check_ref = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check.quick_check !== 'undefined')){
return clojure.test.check.quick_check;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check","quick-check","clojure.test.check/quick-check",-810344251,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check","quick-check","clojure.test.check/quick-check",-810344251,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
cljs.spec.gen.alpha.quick_check = (function cljs$spec$gen$alpha$quick_check(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36553 = arguments.length;
var i__29134__auto___36554 = (0);
while(true){
if((i__29134__auto___36554 < len__29133__auto___36553)){
args__29140__auto__.push((arguments[i__29134__auto___36554]));

var G__36555 = (i__29134__auto___36554 + (1));
i__29134__auto___36554 = G__36555;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.quick_check.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});

cljs.spec.gen.alpha.quick_check.cljs$core$IFn$_invoke$arity$variadic = (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,cljs.spec.gen.alpha.quick_check_ref),args);
});

cljs.spec.gen.alpha.quick_check.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.quick_check.cljs$lang$applyTo = (function (seq36552){
return cljs.spec.gen.alpha.quick_check.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36552));
});

cljs.spec.gen.alpha.for_all_STAR__ref = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.properties.for_all_STAR_ !== 'undefined')){
return clojure.test.check.properties.for_all_STAR_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.properties","for-all*","clojure.test.check.properties/for-all*",67088845,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.properties","for-all*","clojure.test.check.properties/for-all*",67088845,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Dynamically loaded clojure.test.check.properties/for-all*.
 */
cljs.spec.gen.alpha.for_all_STAR_ = (function cljs$spec$gen$alpha$for_all_STAR_(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36557 = arguments.length;
var i__29134__auto___36558 = (0);
while(true){
if((i__29134__auto___36558 < len__29133__auto___36557)){
args__29140__auto__.push((arguments[i__29134__auto___36558]));

var G__36559 = (i__29134__auto___36558 + (1));
i__29134__auto___36558 = G__36559;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});

cljs.spec.gen.alpha.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic = (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,cljs.spec.gen.alpha.for_all_STAR__ref),args);
});

cljs.spec.gen.alpha.for_all_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.for_all_STAR_.cljs$lang$applyTo = (function (seq36556){
return cljs.spec.gen.alpha.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36556));
});

var g_QMARK__36560 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.generator_QMARK_ !== 'undefined')){
return clojure.test.check.generators.generator_QMARK_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","generator?","clojure.test.check.generators/generator?",-1378210460,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","generator?","clojure.test.check.generators/generator?",-1378210460,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
var g_36561 = (new cljs.spec.gen.alpha.LazyVar(((function (g_QMARK__36560){
return (function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.generate !== 'undefined')){
return clojure.test.check.generators.generate;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","generate","clojure.test.check.generators/generate",-690390711,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","generate","clojure.test.check.generators/generate",-690390711,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
});})(g_QMARK__36560))
,null));
var mkg_36562 = (new cljs.spec.gen.alpha.LazyVar(((function (g_QMARK__36560,g_36561){
return (function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.__GT_Generator !== 'undefined')){
return clojure.test.check.generators.__GT_Generator;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","->Generator","clojure.test.check.generators/->Generator",-1179475051,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","->Generator","clojure.test.check.generators/->Generator",-1179475051,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
});})(g_QMARK__36560,g_36561))
,null));
cljs.spec.gen.alpha.generator_QMARK_ = ((function (g_QMARK__36560,g_36561,mkg_36562){
return (function cljs$spec$gen$alpha$generator_QMARK_(x){
return cljs.core.deref.call(null,g_QMARK__36560).call(null,x);
});})(g_QMARK__36560,g_36561,mkg_36562))
;

cljs.spec.gen.alpha.generator = ((function (g_QMARK__36560,g_36561,mkg_36562){
return (function cljs$spec$gen$alpha$generator(gfn){
return cljs.core.deref.call(null,mkg_36562).call(null,gfn);
});})(g_QMARK__36560,g_36561,mkg_36562))
;

/**
 * Generate a single value using generator.
 */
cljs.spec.gen.alpha.generate = ((function (g_QMARK__36560,g_36561,mkg_36562){
return (function cljs$spec$gen$alpha$generate(generator){
return cljs.core.deref.call(null,g_36561).call(null,generator);
});})(g_QMARK__36560,g_36561,mkg_36562))
;
cljs.spec.gen.alpha.delay_impl = (function cljs$spec$gen$alpha$delay_impl(gfnd){
return cljs.spec.gen.alpha.generator.call(null,(function (rnd,size){
return new cljs.core.Keyword(null,"gen","gen",142575302).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,gfnd)).call(null,rnd,size);
}));
});
var g__29226__auto___36582 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.hash_map !== 'undefined')){
return clojure.test.check.generators.hash_map;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","hash-map","clojure.test.check.generators/hash-map",1961346626,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","hash-map","clojure.test.check.generators/hash-map",1961346626,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/hash-map
 */
cljs.spec.gen.alpha.hash_map = ((function (g__29226__auto___36582){
return (function cljs$spec$gen$alpha$hash_map(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36583 = arguments.length;
var i__29134__auto___36584 = (0);
while(true){
if((i__29134__auto___36584 < len__29133__auto___36583)){
args__29140__auto__.push((arguments[i__29134__auto___36584]));

var G__36585 = (i__29134__auto___36584 + (1));
i__29134__auto___36584 = G__36585;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.hash_map.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36582))
;

cljs.spec.gen.alpha.hash_map.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36582){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36582),args);
});})(g__29226__auto___36582))
;

cljs.spec.gen.alpha.hash_map.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.hash_map.cljs$lang$applyTo = ((function (g__29226__auto___36582){
return (function (seq36563){
return cljs.spec.gen.alpha.hash_map.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36563));
});})(g__29226__auto___36582))
;


var g__29226__auto___36586 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.list !== 'undefined')){
return clojure.test.check.generators.list;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","list","clojure.test.check.generators/list",506971058,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","list","clojure.test.check.generators/list",506971058,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/list
 */
cljs.spec.gen.alpha.list = ((function (g__29226__auto___36586){
return (function cljs$spec$gen$alpha$list(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36587 = arguments.length;
var i__29134__auto___36588 = (0);
while(true){
if((i__29134__auto___36588 < len__29133__auto___36587)){
args__29140__auto__.push((arguments[i__29134__auto___36588]));

var G__36589 = (i__29134__auto___36588 + (1));
i__29134__auto___36588 = G__36589;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.list.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36586))
;

cljs.spec.gen.alpha.list.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36586){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36586),args);
});})(g__29226__auto___36586))
;

cljs.spec.gen.alpha.list.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.list.cljs$lang$applyTo = ((function (g__29226__auto___36586){
return (function (seq36564){
return cljs.spec.gen.alpha.list.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36564));
});})(g__29226__auto___36586))
;


var g__29226__auto___36590 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.map !== 'undefined')){
return clojure.test.check.generators.map;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","map","clojure.test.check.generators/map",45738796,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","map","clojure.test.check.generators/map",45738796,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/map
 */
cljs.spec.gen.alpha.map = ((function (g__29226__auto___36590){
return (function cljs$spec$gen$alpha$map(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36591 = arguments.length;
var i__29134__auto___36592 = (0);
while(true){
if((i__29134__auto___36592 < len__29133__auto___36591)){
args__29140__auto__.push((arguments[i__29134__auto___36592]));

var G__36593 = (i__29134__auto___36592 + (1));
i__29134__auto___36592 = G__36593;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.map.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36590))
;

cljs.spec.gen.alpha.map.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36590){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36590),args);
});})(g__29226__auto___36590))
;

cljs.spec.gen.alpha.map.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.map.cljs$lang$applyTo = ((function (g__29226__auto___36590){
return (function (seq36565){
return cljs.spec.gen.alpha.map.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36565));
});})(g__29226__auto___36590))
;


var g__29226__auto___36594 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.not_empty !== 'undefined')){
return clojure.test.check.generators.not_empty;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","not-empty","clojure.test.check.generators/not-empty",-876211682,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","not-empty","clojure.test.check.generators/not-empty",-876211682,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/not-empty
 */
cljs.spec.gen.alpha.not_empty = ((function (g__29226__auto___36594){
return (function cljs$spec$gen$alpha$not_empty(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36595 = arguments.length;
var i__29134__auto___36596 = (0);
while(true){
if((i__29134__auto___36596 < len__29133__auto___36595)){
args__29140__auto__.push((arguments[i__29134__auto___36596]));

var G__36597 = (i__29134__auto___36596 + (1));
i__29134__auto___36596 = G__36597;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.not_empty.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36594))
;

cljs.spec.gen.alpha.not_empty.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36594){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36594),args);
});})(g__29226__auto___36594))
;

cljs.spec.gen.alpha.not_empty.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.not_empty.cljs$lang$applyTo = ((function (g__29226__auto___36594){
return (function (seq36566){
return cljs.spec.gen.alpha.not_empty.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36566));
});})(g__29226__auto___36594))
;


var g__29226__auto___36598 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.set !== 'undefined')){
return clojure.test.check.generators.set;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","set","clojure.test.check.generators/set",-1027639543,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","set","clojure.test.check.generators/set",-1027639543,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/set
 */
cljs.spec.gen.alpha.set = ((function (g__29226__auto___36598){
return (function cljs$spec$gen$alpha$set(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36599 = arguments.length;
var i__29134__auto___36600 = (0);
while(true){
if((i__29134__auto___36600 < len__29133__auto___36599)){
args__29140__auto__.push((arguments[i__29134__auto___36600]));

var G__36601 = (i__29134__auto___36600 + (1));
i__29134__auto___36600 = G__36601;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.set.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36598))
;

cljs.spec.gen.alpha.set.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36598){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36598),args);
});})(g__29226__auto___36598))
;

cljs.spec.gen.alpha.set.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.set.cljs$lang$applyTo = ((function (g__29226__auto___36598){
return (function (seq36567){
return cljs.spec.gen.alpha.set.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36567));
});})(g__29226__auto___36598))
;


var g__29226__auto___36602 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.vector !== 'undefined')){
return clojure.test.check.generators.vector;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","vector","clojure.test.check.generators/vector",1081775325,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","vector","clojure.test.check.generators/vector",1081775325,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/vector
 */
cljs.spec.gen.alpha.vector = ((function (g__29226__auto___36602){
return (function cljs$spec$gen$alpha$vector(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36603 = arguments.length;
var i__29134__auto___36604 = (0);
while(true){
if((i__29134__auto___36604 < len__29133__auto___36603)){
args__29140__auto__.push((arguments[i__29134__auto___36604]));

var G__36605 = (i__29134__auto___36604 + (1));
i__29134__auto___36604 = G__36605;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.vector.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36602))
;

cljs.spec.gen.alpha.vector.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36602){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36602),args);
});})(g__29226__auto___36602))
;

cljs.spec.gen.alpha.vector.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.vector.cljs$lang$applyTo = ((function (g__29226__auto___36602){
return (function (seq36568){
return cljs.spec.gen.alpha.vector.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36568));
});})(g__29226__auto___36602))
;


var g__29226__auto___36606 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.vector_distinct !== 'undefined')){
return clojure.test.check.generators.vector_distinct;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","vector-distinct","clojure.test.check.generators/vector-distinct",1656877834,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","vector-distinct","clojure.test.check.generators/vector-distinct",1656877834,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/vector-distinct
 */
cljs.spec.gen.alpha.vector_distinct = ((function (g__29226__auto___36606){
return (function cljs$spec$gen$alpha$vector_distinct(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36607 = arguments.length;
var i__29134__auto___36608 = (0);
while(true){
if((i__29134__auto___36608 < len__29133__auto___36607)){
args__29140__auto__.push((arguments[i__29134__auto___36608]));

var G__36609 = (i__29134__auto___36608 + (1));
i__29134__auto___36608 = G__36609;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.vector_distinct.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36606))
;

cljs.spec.gen.alpha.vector_distinct.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36606){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36606),args);
});})(g__29226__auto___36606))
;

cljs.spec.gen.alpha.vector_distinct.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.vector_distinct.cljs$lang$applyTo = ((function (g__29226__auto___36606){
return (function (seq36569){
return cljs.spec.gen.alpha.vector_distinct.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36569));
});})(g__29226__auto___36606))
;


var g__29226__auto___36610 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.fmap !== 'undefined')){
return clojure.test.check.generators.fmap;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","fmap","clojure.test.check.generators/fmap",1957997092,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","fmap","clojure.test.check.generators/fmap",1957997092,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/fmap
 */
cljs.spec.gen.alpha.fmap = ((function (g__29226__auto___36610){
return (function cljs$spec$gen$alpha$fmap(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36611 = arguments.length;
var i__29134__auto___36612 = (0);
while(true){
if((i__29134__auto___36612 < len__29133__auto___36611)){
args__29140__auto__.push((arguments[i__29134__auto___36612]));

var G__36613 = (i__29134__auto___36612 + (1));
i__29134__auto___36612 = G__36613;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.fmap.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36610))
;

cljs.spec.gen.alpha.fmap.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36610){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36610),args);
});})(g__29226__auto___36610))
;

cljs.spec.gen.alpha.fmap.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.fmap.cljs$lang$applyTo = ((function (g__29226__auto___36610){
return (function (seq36570){
return cljs.spec.gen.alpha.fmap.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36570));
});})(g__29226__auto___36610))
;


var g__29226__auto___36614 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.elements !== 'undefined')){
return clojure.test.check.generators.elements;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","elements","clojure.test.check.generators/elements",438991326,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","elements","clojure.test.check.generators/elements",438991326,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/elements
 */
cljs.spec.gen.alpha.elements = ((function (g__29226__auto___36614){
return (function cljs$spec$gen$alpha$elements(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36615 = arguments.length;
var i__29134__auto___36616 = (0);
while(true){
if((i__29134__auto___36616 < len__29133__auto___36615)){
args__29140__auto__.push((arguments[i__29134__auto___36616]));

var G__36617 = (i__29134__auto___36616 + (1));
i__29134__auto___36616 = G__36617;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36614))
;

cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36614){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36614),args);
});})(g__29226__auto___36614))
;

cljs.spec.gen.alpha.elements.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.elements.cljs$lang$applyTo = ((function (g__29226__auto___36614){
return (function (seq36571){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36571));
});})(g__29226__auto___36614))
;


var g__29226__auto___36618 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.bind !== 'undefined')){
return clojure.test.check.generators.bind;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","bind","clojure.test.check.generators/bind",-361313906,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","bind","clojure.test.check.generators/bind",-361313906,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/bind
 */
cljs.spec.gen.alpha.bind = ((function (g__29226__auto___36618){
return (function cljs$spec$gen$alpha$bind(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36619 = arguments.length;
var i__29134__auto___36620 = (0);
while(true){
if((i__29134__auto___36620 < len__29133__auto___36619)){
args__29140__auto__.push((arguments[i__29134__auto___36620]));

var G__36621 = (i__29134__auto___36620 + (1));
i__29134__auto___36620 = G__36621;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.bind.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36618))
;

cljs.spec.gen.alpha.bind.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36618){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36618),args);
});})(g__29226__auto___36618))
;

cljs.spec.gen.alpha.bind.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.bind.cljs$lang$applyTo = ((function (g__29226__auto___36618){
return (function (seq36572){
return cljs.spec.gen.alpha.bind.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36572));
});})(g__29226__auto___36618))
;


var g__29226__auto___36622 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.choose !== 'undefined')){
return clojure.test.check.generators.choose;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","choose","clojure.test.check.generators/choose",909997832,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","choose","clojure.test.check.generators/choose",909997832,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/choose
 */
cljs.spec.gen.alpha.choose = ((function (g__29226__auto___36622){
return (function cljs$spec$gen$alpha$choose(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36623 = arguments.length;
var i__29134__auto___36624 = (0);
while(true){
if((i__29134__auto___36624 < len__29133__auto___36623)){
args__29140__auto__.push((arguments[i__29134__auto___36624]));

var G__36625 = (i__29134__auto___36624 + (1));
i__29134__auto___36624 = G__36625;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.choose.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36622))
;

cljs.spec.gen.alpha.choose.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36622){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36622),args);
});})(g__29226__auto___36622))
;

cljs.spec.gen.alpha.choose.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.choose.cljs$lang$applyTo = ((function (g__29226__auto___36622){
return (function (seq36573){
return cljs.spec.gen.alpha.choose.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36573));
});})(g__29226__auto___36622))
;


var g__29226__auto___36626 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.one_of !== 'undefined')){
return clojure.test.check.generators.one_of;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","one-of","clojure.test.check.generators/one-of",-183339191,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","one-of","clojure.test.check.generators/one-of",-183339191,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/one-of
 */
cljs.spec.gen.alpha.one_of = ((function (g__29226__auto___36626){
return (function cljs$spec$gen$alpha$one_of(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36627 = arguments.length;
var i__29134__auto___36628 = (0);
while(true){
if((i__29134__auto___36628 < len__29133__auto___36627)){
args__29140__auto__.push((arguments[i__29134__auto___36628]));

var G__36629 = (i__29134__auto___36628 + (1));
i__29134__auto___36628 = G__36629;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.one_of.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36626))
;

cljs.spec.gen.alpha.one_of.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36626){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36626),args);
});})(g__29226__auto___36626))
;

cljs.spec.gen.alpha.one_of.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.one_of.cljs$lang$applyTo = ((function (g__29226__auto___36626){
return (function (seq36574){
return cljs.spec.gen.alpha.one_of.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36574));
});})(g__29226__auto___36626))
;


var g__29226__auto___36630 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.such_that !== 'undefined')){
return clojure.test.check.generators.such_that;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","such-that","clojure.test.check.generators/such-that",-1754178732,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","such-that","clojure.test.check.generators/such-that",-1754178732,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/such-that
 */
cljs.spec.gen.alpha.such_that = ((function (g__29226__auto___36630){
return (function cljs$spec$gen$alpha$such_that(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36631 = arguments.length;
var i__29134__auto___36632 = (0);
while(true){
if((i__29134__auto___36632 < len__29133__auto___36631)){
args__29140__auto__.push((arguments[i__29134__auto___36632]));

var G__36633 = (i__29134__auto___36632 + (1));
i__29134__auto___36632 = G__36633;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.such_that.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36630))
;

cljs.spec.gen.alpha.such_that.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36630){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36630),args);
});})(g__29226__auto___36630))
;

cljs.spec.gen.alpha.such_that.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.such_that.cljs$lang$applyTo = ((function (g__29226__auto___36630){
return (function (seq36575){
return cljs.spec.gen.alpha.such_that.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36575));
});})(g__29226__auto___36630))
;


var g__29226__auto___36634 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.tuple !== 'undefined')){
return clojure.test.check.generators.tuple;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","tuple","clojure.test.check.generators/tuple",-143711557,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","tuple","clojure.test.check.generators/tuple",-143711557,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/tuple
 */
cljs.spec.gen.alpha.tuple = ((function (g__29226__auto___36634){
return (function cljs$spec$gen$alpha$tuple(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36635 = arguments.length;
var i__29134__auto___36636 = (0);
while(true){
if((i__29134__auto___36636 < len__29133__auto___36635)){
args__29140__auto__.push((arguments[i__29134__auto___36636]));

var G__36637 = (i__29134__auto___36636 + (1));
i__29134__auto___36636 = G__36637;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.tuple.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36634))
;

cljs.spec.gen.alpha.tuple.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36634){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36634),args);
});})(g__29226__auto___36634))
;

cljs.spec.gen.alpha.tuple.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.tuple.cljs$lang$applyTo = ((function (g__29226__auto___36634){
return (function (seq36576){
return cljs.spec.gen.alpha.tuple.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36576));
});})(g__29226__auto___36634))
;


var g__29226__auto___36638 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.sample !== 'undefined')){
return clojure.test.check.generators.sample;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","sample","clojure.test.check.generators/sample",-382944992,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","sample","clojure.test.check.generators/sample",-382944992,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/sample
 */
cljs.spec.gen.alpha.sample = ((function (g__29226__auto___36638){
return (function cljs$spec$gen$alpha$sample(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36639 = arguments.length;
var i__29134__auto___36640 = (0);
while(true){
if((i__29134__auto___36640 < len__29133__auto___36639)){
args__29140__auto__.push((arguments[i__29134__auto___36640]));

var G__36641 = (i__29134__auto___36640 + (1));
i__29134__auto___36640 = G__36641;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.sample.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36638))
;

cljs.spec.gen.alpha.sample.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36638){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36638),args);
});})(g__29226__auto___36638))
;

cljs.spec.gen.alpha.sample.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.sample.cljs$lang$applyTo = ((function (g__29226__auto___36638){
return (function (seq36577){
return cljs.spec.gen.alpha.sample.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36577));
});})(g__29226__auto___36638))
;


var g__29226__auto___36642 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.return$ !== 'undefined')){
return clojure.test.check.generators.return$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","return","clojure.test.check.generators/return",1744522038,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","return","clojure.test.check.generators/return",1744522038,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/return
 */
cljs.spec.gen.alpha.return$ = ((function (g__29226__auto___36642){
return (function cljs$spec$gen$alpha$return(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36643 = arguments.length;
var i__29134__auto___36644 = (0);
while(true){
if((i__29134__auto___36644 < len__29133__auto___36643)){
args__29140__auto__.push((arguments[i__29134__auto___36644]));

var G__36645 = (i__29134__auto___36644 + (1));
i__29134__auto___36644 = G__36645;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36642))
;

cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36642){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36642),args);
});})(g__29226__auto___36642))
;

cljs.spec.gen.alpha.return$.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.return$.cljs$lang$applyTo = ((function (g__29226__auto___36642){
return (function (seq36578){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36578));
});})(g__29226__auto___36642))
;


var g__29226__auto___36646 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.large_integer_STAR_ !== 'undefined')){
return clojure.test.check.generators.large_integer_STAR_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","large-integer*","clojure.test.check.generators/large-integer*",-437830670,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","large-integer*","clojure.test.check.generators/large-integer*",-437830670,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/large-integer*
 */
cljs.spec.gen.alpha.large_integer_STAR_ = ((function (g__29226__auto___36646){
return (function cljs$spec$gen$alpha$large_integer_STAR_(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36647 = arguments.length;
var i__29134__auto___36648 = (0);
while(true){
if((i__29134__auto___36648 < len__29133__auto___36647)){
args__29140__auto__.push((arguments[i__29134__auto___36648]));

var G__36649 = (i__29134__auto___36648 + (1));
i__29134__auto___36648 = G__36649;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36646))
;

cljs.spec.gen.alpha.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36646){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36646),args);
});})(g__29226__auto___36646))
;

cljs.spec.gen.alpha.large_integer_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.large_integer_STAR_.cljs$lang$applyTo = ((function (g__29226__auto___36646){
return (function (seq36579){
return cljs.spec.gen.alpha.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36579));
});})(g__29226__auto___36646))
;


var g__29226__auto___36650 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.double_STAR_ !== 'undefined')){
return clojure.test.check.generators.double_STAR_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","double*","clojure.test.check.generators/double*",841542265,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","double*","clojure.test.check.generators/double*",841542265,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/double*
 */
cljs.spec.gen.alpha.double_STAR_ = ((function (g__29226__auto___36650){
return (function cljs$spec$gen$alpha$double_STAR_(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36651 = arguments.length;
var i__29134__auto___36652 = (0);
while(true){
if((i__29134__auto___36652 < len__29133__auto___36651)){
args__29140__auto__.push((arguments[i__29134__auto___36652]));

var G__36653 = (i__29134__auto___36652 + (1));
i__29134__auto___36652 = G__36653;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.double_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36650))
;

cljs.spec.gen.alpha.double_STAR_.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36650){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36650),args);
});})(g__29226__auto___36650))
;

cljs.spec.gen.alpha.double_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.double_STAR_.cljs$lang$applyTo = ((function (g__29226__auto___36650){
return (function (seq36580){
return cljs.spec.gen.alpha.double_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36580));
});})(g__29226__auto___36650))
;


var g__29226__auto___36654 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.frequency !== 'undefined')){
return clojure.test.check.generators.frequency;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","frequency","clojure.test.check.generators/frequency",2090703177,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","frequency","clojure.test.check.generators/frequency",2090703177,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/frequency
 */
cljs.spec.gen.alpha.frequency = ((function (g__29226__auto___36654){
return (function cljs$spec$gen$alpha$frequency(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36655 = arguments.length;
var i__29134__auto___36656 = (0);
while(true){
if((i__29134__auto___36656 < len__29133__auto___36655)){
args__29140__auto__.push((arguments[i__29134__auto___36656]));

var G__36657 = (i__29134__auto___36656 + (1));
i__29134__auto___36656 = G__36657;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.frequency.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29226__auto___36654))
;

cljs.spec.gen.alpha.frequency.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29226__auto___36654){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__29226__auto___36654),args);
});})(g__29226__auto___36654))
;

cljs.spec.gen.alpha.frequency.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.frequency.cljs$lang$applyTo = ((function (g__29226__auto___36654){
return (function (seq36581){
return cljs.spec.gen.alpha.frequency.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36581));
});})(g__29226__auto___36654))
;

var g__29239__auto___36679 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.any !== 'undefined')){
return clojure.test.check.generators.any;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","any","clojure.test.check.generators/any",1883743710,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","any","clojure.test.check.generators/any",1883743710,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/any
 */
cljs.spec.gen.alpha.any = ((function (g__29239__auto___36679){
return (function cljs$spec$gen$alpha$any(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36680 = arguments.length;
var i__29134__auto___36681 = (0);
while(true){
if((i__29134__auto___36681 < len__29133__auto___36680)){
args__29140__auto__.push((arguments[i__29134__auto___36681]));

var G__36682 = (i__29134__auto___36681 + (1));
i__29134__auto___36681 = G__36682;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.any.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36679))
;

cljs.spec.gen.alpha.any.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36679){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36679);
});})(g__29239__auto___36679))
;

cljs.spec.gen.alpha.any.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.any.cljs$lang$applyTo = ((function (g__29239__auto___36679){
return (function (seq36658){
return cljs.spec.gen.alpha.any.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36658));
});})(g__29239__auto___36679))
;


var g__29239__auto___36683 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.any_printable !== 'undefined')){
return clojure.test.check.generators.any_printable;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","any-printable","clojure.test.check.generators/any-printable",-1570493991,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","any-printable","clojure.test.check.generators/any-printable",-1570493991,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/any-printable
 */
cljs.spec.gen.alpha.any_printable = ((function (g__29239__auto___36683){
return (function cljs$spec$gen$alpha$any_printable(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36684 = arguments.length;
var i__29134__auto___36685 = (0);
while(true){
if((i__29134__auto___36685 < len__29133__auto___36684)){
args__29140__auto__.push((arguments[i__29134__auto___36685]));

var G__36686 = (i__29134__auto___36685 + (1));
i__29134__auto___36685 = G__36686;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.any_printable.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36683))
;

cljs.spec.gen.alpha.any_printable.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36683){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36683);
});})(g__29239__auto___36683))
;

cljs.spec.gen.alpha.any_printable.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.any_printable.cljs$lang$applyTo = ((function (g__29239__auto___36683){
return (function (seq36659){
return cljs.spec.gen.alpha.any_printable.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36659));
});})(g__29239__auto___36683))
;


var g__29239__auto___36687 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.boolean$ !== 'undefined')){
return clojure.test.check.generators.boolean$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","boolean","clojure.test.check.generators/boolean",1586992347,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","boolean","clojure.test.check.generators/boolean",1586992347,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/boolean
 */
cljs.spec.gen.alpha.boolean$ = ((function (g__29239__auto___36687){
return (function cljs$spec$gen$alpha$boolean(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36688 = arguments.length;
var i__29134__auto___36689 = (0);
while(true){
if((i__29134__auto___36689 < len__29133__auto___36688)){
args__29140__auto__.push((arguments[i__29134__auto___36689]));

var G__36690 = (i__29134__auto___36689 + (1));
i__29134__auto___36689 = G__36690;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.boolean$.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36687))
;

cljs.spec.gen.alpha.boolean$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36687){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36687);
});})(g__29239__auto___36687))
;

cljs.spec.gen.alpha.boolean$.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.boolean$.cljs$lang$applyTo = ((function (g__29239__auto___36687){
return (function (seq36660){
return cljs.spec.gen.alpha.boolean$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36660));
});})(g__29239__auto___36687))
;


var g__29239__auto___36691 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char$ !== 'undefined')){
return clojure.test.check.generators.char$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char","clojure.test.check.generators/char",-1426343459,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char","clojure.test.check.generators/char",-1426343459,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char
 */
cljs.spec.gen.alpha.char$ = ((function (g__29239__auto___36691){
return (function cljs$spec$gen$alpha$char(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36692 = arguments.length;
var i__29134__auto___36693 = (0);
while(true){
if((i__29134__auto___36693 < len__29133__auto___36692)){
args__29140__auto__.push((arguments[i__29134__auto___36693]));

var G__36694 = (i__29134__auto___36693 + (1));
i__29134__auto___36693 = G__36694;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.char$.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36691))
;

cljs.spec.gen.alpha.char$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36691){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36691);
});})(g__29239__auto___36691))
;

cljs.spec.gen.alpha.char$.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.char$.cljs$lang$applyTo = ((function (g__29239__auto___36691){
return (function (seq36661){
return cljs.spec.gen.alpha.char$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36661));
});})(g__29239__auto___36691))
;


var g__29239__auto___36695 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_alpha !== 'undefined')){
return clojure.test.check.generators.char_alpha;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char-alpha","clojure.test.check.generators/char-alpha",615785796,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-alpha","clojure.test.check.generators/char-alpha",615785796,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-alpha
 */
cljs.spec.gen.alpha.char_alpha = ((function (g__29239__auto___36695){
return (function cljs$spec$gen$alpha$char_alpha(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36696 = arguments.length;
var i__29134__auto___36697 = (0);
while(true){
if((i__29134__auto___36697 < len__29133__auto___36696)){
args__29140__auto__.push((arguments[i__29134__auto___36697]));

var G__36698 = (i__29134__auto___36697 + (1));
i__29134__auto___36697 = G__36698;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.char_alpha.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36695))
;

cljs.spec.gen.alpha.char_alpha.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36695){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36695);
});})(g__29239__auto___36695))
;

cljs.spec.gen.alpha.char_alpha.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.char_alpha.cljs$lang$applyTo = ((function (g__29239__auto___36695){
return (function (seq36662){
return cljs.spec.gen.alpha.char_alpha.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36662));
});})(g__29239__auto___36695))
;


var g__29239__auto___36699 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_alphanumeric !== 'undefined')){
return clojure.test.check.generators.char_alphanumeric;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char-alphanumeric","clojure.test.check.generators/char-alphanumeric",1383091431,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-alphanumeric","clojure.test.check.generators/char-alphanumeric",1383091431,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-alphanumeric
 */
cljs.spec.gen.alpha.char_alphanumeric = ((function (g__29239__auto___36699){
return (function cljs$spec$gen$alpha$char_alphanumeric(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36700 = arguments.length;
var i__29134__auto___36701 = (0);
while(true){
if((i__29134__auto___36701 < len__29133__auto___36700)){
args__29140__auto__.push((arguments[i__29134__auto___36701]));

var G__36702 = (i__29134__auto___36701 + (1));
i__29134__auto___36701 = G__36702;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36699))
;

cljs.spec.gen.alpha.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36699){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36699);
});})(g__29239__auto___36699))
;

cljs.spec.gen.alpha.char_alphanumeric.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.char_alphanumeric.cljs$lang$applyTo = ((function (g__29239__auto___36699){
return (function (seq36663){
return cljs.spec.gen.alpha.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36663));
});})(g__29239__auto___36699))
;


var g__29239__auto___36703 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_ascii !== 'undefined')){
return clojure.test.check.generators.char_ascii;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char-ascii","clojure.test.check.generators/char-ascii",-899908538,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-ascii","clojure.test.check.generators/char-ascii",-899908538,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-ascii
 */
cljs.spec.gen.alpha.char_ascii = ((function (g__29239__auto___36703){
return (function cljs$spec$gen$alpha$char_ascii(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36704 = arguments.length;
var i__29134__auto___36705 = (0);
while(true){
if((i__29134__auto___36705 < len__29133__auto___36704)){
args__29140__auto__.push((arguments[i__29134__auto___36705]));

var G__36706 = (i__29134__auto___36705 + (1));
i__29134__auto___36705 = G__36706;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.char_ascii.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36703))
;

cljs.spec.gen.alpha.char_ascii.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36703){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36703);
});})(g__29239__auto___36703))
;

cljs.spec.gen.alpha.char_ascii.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.char_ascii.cljs$lang$applyTo = ((function (g__29239__auto___36703){
return (function (seq36664){
return cljs.spec.gen.alpha.char_ascii.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36664));
});})(g__29239__auto___36703))
;


var g__29239__auto___36707 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.double$ !== 'undefined')){
return clojure.test.check.generators.double$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","double","clojure.test.check.generators/double",668331090,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","double","clojure.test.check.generators/double",668331090,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/double
 */
cljs.spec.gen.alpha.double$ = ((function (g__29239__auto___36707){
return (function cljs$spec$gen$alpha$double(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36708 = arguments.length;
var i__29134__auto___36709 = (0);
while(true){
if((i__29134__auto___36709 < len__29133__auto___36708)){
args__29140__auto__.push((arguments[i__29134__auto___36709]));

var G__36710 = (i__29134__auto___36709 + (1));
i__29134__auto___36709 = G__36710;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.double$.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36707))
;

cljs.spec.gen.alpha.double$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36707){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36707);
});})(g__29239__auto___36707))
;

cljs.spec.gen.alpha.double$.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.double$.cljs$lang$applyTo = ((function (g__29239__auto___36707){
return (function (seq36665){
return cljs.spec.gen.alpha.double$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36665));
});})(g__29239__auto___36707))
;


var g__29239__auto___36711 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.int$ !== 'undefined')){
return clojure.test.check.generators.int$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","int","clojure.test.check.generators/int",1756228469,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","int","clojure.test.check.generators/int",1756228469,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/int
 */
cljs.spec.gen.alpha.int$ = ((function (g__29239__auto___36711){
return (function cljs$spec$gen$alpha$int(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36712 = arguments.length;
var i__29134__auto___36713 = (0);
while(true){
if((i__29134__auto___36713 < len__29133__auto___36712)){
args__29140__auto__.push((arguments[i__29134__auto___36713]));

var G__36714 = (i__29134__auto___36713 + (1));
i__29134__auto___36713 = G__36714;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.int$.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36711))
;

cljs.spec.gen.alpha.int$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36711){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36711);
});})(g__29239__auto___36711))
;

cljs.spec.gen.alpha.int$.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.int$.cljs$lang$applyTo = ((function (g__29239__auto___36711){
return (function (seq36666){
return cljs.spec.gen.alpha.int$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36666));
});})(g__29239__auto___36711))
;


var g__29239__auto___36715 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.keyword !== 'undefined')){
return clojure.test.check.generators.keyword;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","keyword","clojure.test.check.generators/keyword",24530530,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","keyword","clojure.test.check.generators/keyword",24530530,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/keyword
 */
cljs.spec.gen.alpha.keyword = ((function (g__29239__auto___36715){
return (function cljs$spec$gen$alpha$keyword(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36716 = arguments.length;
var i__29134__auto___36717 = (0);
while(true){
if((i__29134__auto___36717 < len__29133__auto___36716)){
args__29140__auto__.push((arguments[i__29134__auto___36717]));

var G__36718 = (i__29134__auto___36717 + (1));
i__29134__auto___36717 = G__36718;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.keyword.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36715))
;

cljs.spec.gen.alpha.keyword.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36715){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36715);
});})(g__29239__auto___36715))
;

cljs.spec.gen.alpha.keyword.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.keyword.cljs$lang$applyTo = ((function (g__29239__auto___36715){
return (function (seq36667){
return cljs.spec.gen.alpha.keyword.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36667));
});})(g__29239__auto___36715))
;


var g__29239__auto___36719 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.keyword_ns !== 'undefined')){
return clojure.test.check.generators.keyword_ns;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","keyword-ns","clojure.test.check.generators/keyword-ns",-1492628482,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","keyword-ns","clojure.test.check.generators/keyword-ns",-1492628482,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/keyword-ns
 */
cljs.spec.gen.alpha.keyword_ns = ((function (g__29239__auto___36719){
return (function cljs$spec$gen$alpha$keyword_ns(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36720 = arguments.length;
var i__29134__auto___36721 = (0);
while(true){
if((i__29134__auto___36721 < len__29133__auto___36720)){
args__29140__auto__.push((arguments[i__29134__auto___36721]));

var G__36722 = (i__29134__auto___36721 + (1));
i__29134__auto___36721 = G__36722;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.keyword_ns.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36719))
;

cljs.spec.gen.alpha.keyword_ns.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36719){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36719);
});})(g__29239__auto___36719))
;

cljs.spec.gen.alpha.keyword_ns.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.keyword_ns.cljs$lang$applyTo = ((function (g__29239__auto___36719){
return (function (seq36668){
return cljs.spec.gen.alpha.keyword_ns.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36668));
});})(g__29239__auto___36719))
;


var g__29239__auto___36723 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.large_integer !== 'undefined')){
return clojure.test.check.generators.large_integer;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","large-integer","clojure.test.check.generators/large-integer",-865967138,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","large-integer","clojure.test.check.generators/large-integer",-865967138,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/large-integer
 */
cljs.spec.gen.alpha.large_integer = ((function (g__29239__auto___36723){
return (function cljs$spec$gen$alpha$large_integer(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36724 = arguments.length;
var i__29134__auto___36725 = (0);
while(true){
if((i__29134__auto___36725 < len__29133__auto___36724)){
args__29140__auto__.push((arguments[i__29134__auto___36725]));

var G__36726 = (i__29134__auto___36725 + (1));
i__29134__auto___36725 = G__36726;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.large_integer.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36723))
;

cljs.spec.gen.alpha.large_integer.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36723){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36723);
});})(g__29239__auto___36723))
;

cljs.spec.gen.alpha.large_integer.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.large_integer.cljs$lang$applyTo = ((function (g__29239__auto___36723){
return (function (seq36669){
return cljs.spec.gen.alpha.large_integer.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36669));
});})(g__29239__auto___36723))
;


var g__29239__auto___36727 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.ratio !== 'undefined')){
return clojure.test.check.generators.ratio;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","ratio","clojure.test.check.generators/ratio",1540966915,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","ratio","clojure.test.check.generators/ratio",1540966915,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/ratio
 */
cljs.spec.gen.alpha.ratio = ((function (g__29239__auto___36727){
return (function cljs$spec$gen$alpha$ratio(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36728 = arguments.length;
var i__29134__auto___36729 = (0);
while(true){
if((i__29134__auto___36729 < len__29133__auto___36728)){
args__29140__auto__.push((arguments[i__29134__auto___36729]));

var G__36730 = (i__29134__auto___36729 + (1));
i__29134__auto___36729 = G__36730;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.ratio.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36727))
;

cljs.spec.gen.alpha.ratio.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36727){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36727);
});})(g__29239__auto___36727))
;

cljs.spec.gen.alpha.ratio.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.ratio.cljs$lang$applyTo = ((function (g__29239__auto___36727){
return (function (seq36670){
return cljs.spec.gen.alpha.ratio.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36670));
});})(g__29239__auto___36727))
;


var g__29239__auto___36731 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.simple_type !== 'undefined')){
return clojure.test.check.generators.simple_type;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","simple-type","clojure.test.check.generators/simple-type",892572284,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","simple-type","clojure.test.check.generators/simple-type",892572284,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/simple-type
 */
cljs.spec.gen.alpha.simple_type = ((function (g__29239__auto___36731){
return (function cljs$spec$gen$alpha$simple_type(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36732 = arguments.length;
var i__29134__auto___36733 = (0);
while(true){
if((i__29134__auto___36733 < len__29133__auto___36732)){
args__29140__auto__.push((arguments[i__29134__auto___36733]));

var G__36734 = (i__29134__auto___36733 + (1));
i__29134__auto___36733 = G__36734;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.simple_type.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36731))
;

cljs.spec.gen.alpha.simple_type.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36731){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36731);
});})(g__29239__auto___36731))
;

cljs.spec.gen.alpha.simple_type.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.simple_type.cljs$lang$applyTo = ((function (g__29239__auto___36731){
return (function (seq36671){
return cljs.spec.gen.alpha.simple_type.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36671));
});})(g__29239__auto___36731))
;


var g__29239__auto___36735 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.simple_type_printable !== 'undefined')){
return clojure.test.check.generators.simple_type_printable;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","simple-type-printable","clojure.test.check.generators/simple-type-printable",-58489962,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","simple-type-printable","clojure.test.check.generators/simple-type-printable",-58489962,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/simple-type-printable
 */
cljs.spec.gen.alpha.simple_type_printable = ((function (g__29239__auto___36735){
return (function cljs$spec$gen$alpha$simple_type_printable(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36736 = arguments.length;
var i__29134__auto___36737 = (0);
while(true){
if((i__29134__auto___36737 < len__29133__auto___36736)){
args__29140__auto__.push((arguments[i__29134__auto___36737]));

var G__36738 = (i__29134__auto___36737 + (1));
i__29134__auto___36737 = G__36738;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36735))
;

cljs.spec.gen.alpha.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36735){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36735);
});})(g__29239__auto___36735))
;

cljs.spec.gen.alpha.simple_type_printable.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.simple_type_printable.cljs$lang$applyTo = ((function (g__29239__auto___36735){
return (function (seq36672){
return cljs.spec.gen.alpha.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36672));
});})(g__29239__auto___36735))
;


var g__29239__auto___36739 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string !== 'undefined')){
return clojure.test.check.generators.string;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","string","clojure.test.check.generators/string",-1704750979,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string","clojure.test.check.generators/string",-1704750979,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string
 */
cljs.spec.gen.alpha.string = ((function (g__29239__auto___36739){
return (function cljs$spec$gen$alpha$string(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36740 = arguments.length;
var i__29134__auto___36741 = (0);
while(true){
if((i__29134__auto___36741 < len__29133__auto___36740)){
args__29140__auto__.push((arguments[i__29134__auto___36741]));

var G__36742 = (i__29134__auto___36741 + (1));
i__29134__auto___36741 = G__36742;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.string.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36739))
;

cljs.spec.gen.alpha.string.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36739){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36739);
});})(g__29239__auto___36739))
;

cljs.spec.gen.alpha.string.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.string.cljs$lang$applyTo = ((function (g__29239__auto___36739){
return (function (seq36673){
return cljs.spec.gen.alpha.string.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36673));
});})(g__29239__auto___36739))
;


var g__29239__auto___36743 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string_ascii !== 'undefined')){
return clojure.test.check.generators.string_ascii;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","string-ascii","clojure.test.check.generators/string-ascii",-2009877640,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string-ascii","clojure.test.check.generators/string-ascii",-2009877640,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string-ascii
 */
cljs.spec.gen.alpha.string_ascii = ((function (g__29239__auto___36743){
return (function cljs$spec$gen$alpha$string_ascii(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36744 = arguments.length;
var i__29134__auto___36745 = (0);
while(true){
if((i__29134__auto___36745 < len__29133__auto___36744)){
args__29140__auto__.push((arguments[i__29134__auto___36745]));

var G__36746 = (i__29134__auto___36745 + (1));
i__29134__auto___36745 = G__36746;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.string_ascii.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36743))
;

cljs.spec.gen.alpha.string_ascii.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36743){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36743);
});})(g__29239__auto___36743))
;

cljs.spec.gen.alpha.string_ascii.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.string_ascii.cljs$lang$applyTo = ((function (g__29239__auto___36743){
return (function (seq36674){
return cljs.spec.gen.alpha.string_ascii.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36674));
});})(g__29239__auto___36743))
;


var g__29239__auto___36747 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string_alphanumeric !== 'undefined')){
return clojure.test.check.generators.string_alphanumeric;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","string-alphanumeric","clojure.test.check.generators/string-alphanumeric",836374939,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string-alphanumeric","clojure.test.check.generators/string-alphanumeric",836374939,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string-alphanumeric
 */
cljs.spec.gen.alpha.string_alphanumeric = ((function (g__29239__auto___36747){
return (function cljs$spec$gen$alpha$string_alphanumeric(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36748 = arguments.length;
var i__29134__auto___36749 = (0);
while(true){
if((i__29134__auto___36749 < len__29133__auto___36748)){
args__29140__auto__.push((arguments[i__29134__auto___36749]));

var G__36750 = (i__29134__auto___36749 + (1));
i__29134__auto___36749 = G__36750;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36747))
;

cljs.spec.gen.alpha.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36747){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36747);
});})(g__29239__auto___36747))
;

cljs.spec.gen.alpha.string_alphanumeric.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.string_alphanumeric.cljs$lang$applyTo = ((function (g__29239__auto___36747){
return (function (seq36675){
return cljs.spec.gen.alpha.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36675));
});})(g__29239__auto___36747))
;


var g__29239__auto___36751 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.symbol !== 'undefined')){
return clojure.test.check.generators.symbol;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","symbol","clojure.test.check.generators/symbol",-1305461065,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","symbol","clojure.test.check.generators/symbol",-1305461065,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/symbol
 */
cljs.spec.gen.alpha.symbol = ((function (g__29239__auto___36751){
return (function cljs$spec$gen$alpha$symbol(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36752 = arguments.length;
var i__29134__auto___36753 = (0);
while(true){
if((i__29134__auto___36753 < len__29133__auto___36752)){
args__29140__auto__.push((arguments[i__29134__auto___36753]));

var G__36754 = (i__29134__auto___36753 + (1));
i__29134__auto___36753 = G__36754;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.symbol.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36751))
;

cljs.spec.gen.alpha.symbol.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36751){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36751);
});})(g__29239__auto___36751))
;

cljs.spec.gen.alpha.symbol.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.symbol.cljs$lang$applyTo = ((function (g__29239__auto___36751){
return (function (seq36676){
return cljs.spec.gen.alpha.symbol.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36676));
});})(g__29239__auto___36751))
;


var g__29239__auto___36755 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.symbol_ns !== 'undefined')){
return clojure.test.check.generators.symbol_ns;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","symbol-ns","clojure.test.check.generators/symbol-ns",-862629490,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","symbol-ns","clojure.test.check.generators/symbol-ns",-862629490,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/symbol-ns
 */
cljs.spec.gen.alpha.symbol_ns = ((function (g__29239__auto___36755){
return (function cljs$spec$gen$alpha$symbol_ns(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36756 = arguments.length;
var i__29134__auto___36757 = (0);
while(true){
if((i__29134__auto___36757 < len__29133__auto___36756)){
args__29140__auto__.push((arguments[i__29134__auto___36757]));

var G__36758 = (i__29134__auto___36757 + (1));
i__29134__auto___36757 = G__36758;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.symbol_ns.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36755))
;

cljs.spec.gen.alpha.symbol_ns.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36755){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36755);
});})(g__29239__auto___36755))
;

cljs.spec.gen.alpha.symbol_ns.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.symbol_ns.cljs$lang$applyTo = ((function (g__29239__auto___36755){
return (function (seq36677){
return cljs.spec.gen.alpha.symbol_ns.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36677));
});})(g__29239__auto___36755))
;


var g__29239__auto___36759 = (new cljs.spec.gen.alpha.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.uuid !== 'undefined')){
return clojure.test.check.generators.uuid;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","uuid","clojure.test.check.generators/uuid",1589373144,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","uuid","clojure.test.check.generators/uuid",1589373144,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/uuid
 */
cljs.spec.gen.alpha.uuid = ((function (g__29239__auto___36759){
return (function cljs$spec$gen$alpha$uuid(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36760 = arguments.length;
var i__29134__auto___36761 = (0);
while(true){
if((i__29134__auto___36761 < len__29133__auto___36760)){
args__29140__auto__.push((arguments[i__29134__auto___36761]));

var G__36762 = (i__29134__auto___36761 + (1));
i__29134__auto___36761 = G__36762;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.uuid.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});})(g__29239__auto___36759))
;

cljs.spec.gen.alpha.uuid.cljs$core$IFn$_invoke$arity$variadic = ((function (g__29239__auto___36759){
return (function (args){
return cljs.core.deref.call(null,g__29239__auto___36759);
});})(g__29239__auto___36759))
;

cljs.spec.gen.alpha.uuid.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.uuid.cljs$lang$applyTo = ((function (g__29239__auto___36759){
return (function (seq36678){
return cljs.spec.gen.alpha.uuid.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36678));
});})(g__29239__auto___36759))
;

/**
 * Returns a generator of a sequence catenated from results of
 * gens, each of which should generate something sequential.
 */
cljs.spec.gen.alpha.cat = (function cljs$spec$gen$alpha$cat(var_args){
var args__29140__auto__ = [];
var len__29133__auto___36765 = arguments.length;
var i__29134__auto___36766 = (0);
while(true){
if((i__29134__auto___36766 < len__29133__auto___36765)){
args__29140__auto__.push((arguments[i__29134__auto___36766]));

var G__36767 = (i__29134__auto___36766 + (1));
i__29134__auto___36766 = G__36767;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return cljs.spec.gen.alpha.cat.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});

cljs.spec.gen.alpha.cat.cljs$core$IFn$_invoke$arity$variadic = (function (gens){
return cljs.spec.gen.alpha.fmap.call(null,(function (p1__36763_SHARP_){
return cljs.core.apply.call(null,cljs.core.concat,p1__36763_SHARP_);
}),cljs.core.apply.call(null,cljs.spec.gen.alpha.tuple,gens));
});

cljs.spec.gen.alpha.cat.cljs$lang$maxFixedArity = (0);

cljs.spec.gen.alpha.cat.cljs$lang$applyTo = (function (seq36764){
return cljs.spec.gen.alpha.cat.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36764));
});

cljs.spec.gen.alpha.qualified_QMARK_ = (function cljs$spec$gen$alpha$qualified_QMARK_(ident){
return !((cljs.core.namespace.call(null,ident) == null));
});
cljs.spec.gen.alpha.gen_builtins = (new cljs.core.Delay((function (){
var simple = cljs.spec.gen.alpha.simple_type_printable.call(null);
return cljs.core.PersistentHashMap.fromArrays([cljs.core.qualified_keyword_QMARK_,cljs.core.seq_QMARK_,cljs.core.vector_QMARK_,cljs.core.any_QMARK_,cljs.core.boolean_QMARK_,cljs.core.char_QMARK_,cljs.core.inst_QMARK_,cljs.core.simple_symbol_QMARK_,cljs.core.sequential_QMARK_,cljs.core.float_QMARK_,cljs.core.set_QMARK_,cljs.core.map_QMARK_,cljs.core.empty_QMARK_,cljs.core.string_QMARK_,cljs.core.double_QMARK_,cljs.core.int_QMARK_,cljs.core.associative_QMARK_,cljs.core.keyword_QMARK_,cljs.core.indexed_QMARK_,cljs.core.zero_QMARK_,cljs.core.simple_keyword_QMARK_,cljs.core.neg_int_QMARK_,cljs.core.nil_QMARK_,cljs.core.ident_QMARK_,cljs.core.qualified_ident_QMARK_,cljs.core.true_QMARK_,cljs.core.integer_QMARK_,cljs.core.nat_int_QMARK_,cljs.core.pos_int_QMARK_,cljs.core.uuid_QMARK_,cljs.core.false_QMARK_,cljs.core.list_QMARK_,cljs.core.simple_ident_QMARK_,cljs.core.number_QMARK_,cljs.core.qualified_symbol_QMARK_,cljs.core.seqable_QMARK_,cljs.core.symbol_QMARK_,cljs.core.coll_QMARK_],[cljs.spec.gen.alpha.such_that.call(null,cljs.spec.gen.alpha.qualified_QMARK_,cljs.spec.gen.alpha.keyword_ns.call(null)),cljs.spec.gen.alpha.list.call(null,simple),cljs.spec.gen.alpha.vector.call(null,simple),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.return$.call(null,null),cljs.spec.gen.alpha.any_printable.call(null)], null)),cljs.spec.gen.alpha.boolean$.call(null),cljs.spec.gen.alpha.char$.call(null),cljs.spec.gen.alpha.fmap.call(null,((function (simple){
return (function (p1__36768_SHARP_){
return (new Date(p1__36768_SHARP_));
});})(simple))
,cljs.spec.gen.alpha.large_integer.call(null)),cljs.spec.gen.alpha.symbol.call(null),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.list.call(null,simple),cljs.spec.gen.alpha.vector.call(null,simple)], null)),cljs.spec.gen.alpha.double$.call(null),cljs.spec.gen.alpha.set.call(null,simple),cljs.spec.gen.alpha.map.call(null,simple,simple),cljs.spec.gen.alpha.elements.call(null,new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,cljs.core.List.EMPTY,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentHashSet.EMPTY], null)),cljs.spec.gen.alpha.string_alphanumeric.call(null),cljs.spec.gen.alpha.double$.call(null),cljs.spec.gen.alpha.large_integer.call(null),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.map.call(null,simple,simple),cljs.spec.gen.alpha.vector.call(null,simple)], null)),cljs.spec.gen.alpha.keyword_ns.call(null),cljs.spec.gen.alpha.vector.call(null,simple),cljs.spec.gen.alpha.return$.call(null,(0)),cljs.spec.gen.alpha.keyword.call(null),cljs.spec.gen.alpha.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"max","max",61366548),(-1)], null)),cljs.spec.gen.alpha.return$.call(null,null),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.keyword_ns.call(null),cljs.spec.gen.alpha.symbol_ns.call(null)], null)),cljs.spec.gen.alpha.such_that.call(null,cljs.spec.gen.alpha.qualified_QMARK_,cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.keyword_ns.call(null),cljs.spec.gen.alpha.symbol_ns.call(null)], null))),cljs.spec.gen.alpha.return$.call(null,true),cljs.spec.gen.alpha.large_integer.call(null),cljs.spec.gen.alpha.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"min","min",444991522),(0)], null)),cljs.spec.gen.alpha.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"min","min",444991522),(1)], null)),cljs.spec.gen.alpha.uuid.call(null),cljs.spec.gen.alpha.return$.call(null,false),cljs.spec.gen.alpha.list.call(null,simple),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.keyword.call(null),cljs.spec.gen.alpha.symbol.call(null)], null)),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.large_integer.call(null),cljs.spec.gen.alpha.double$.call(null)], null)),cljs.spec.gen.alpha.such_that.call(null,cljs.spec.gen.alpha.qualified_QMARK_,cljs.spec.gen.alpha.symbol_ns.call(null)),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.return$.call(null,null),cljs.spec.gen.alpha.list.call(null,simple),cljs.spec.gen.alpha.vector.call(null,simple),cljs.spec.gen.alpha.map.call(null,simple,simple),cljs.spec.gen.alpha.set.call(null,simple),cljs.spec.gen.alpha.string_alphanumeric.call(null)], null)),cljs.spec.gen.alpha.symbol_ns.call(null),cljs.spec.gen.alpha.one_of.call(null,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.gen.alpha.map.call(null,simple,simple),cljs.spec.gen.alpha.list.call(null,simple),cljs.spec.gen.alpha.vector.call(null,simple),cljs.spec.gen.alpha.set.call(null,simple)], null))]);
}),null));
/**
 * Given a predicate, returns a built-in generator if one exists.
 */
cljs.spec.gen.alpha.gen_for_pred = (function cljs$spec$gen$alpha$gen_for_pred(pred){
if(cljs.core.set_QMARK_.call(null,pred)){
return cljs.spec.gen.alpha.elements.call(null,pred);
} else {
return cljs.core.get.call(null,cljs.core.deref.call(null,cljs.spec.gen.alpha.gen_builtins),pred);
}
});

//# sourceMappingURL=alpha.js.map?rel=1503198193052
