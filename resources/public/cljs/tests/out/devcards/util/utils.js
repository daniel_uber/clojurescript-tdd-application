// Compiled by ClojureScript 1.9.854 {}
goog.provide('devcards.util.utils');
goog.require('cljs.core');
goog.require('cljs.pprint');
devcards.util.utils.html_env_QMARK_ = (function devcards$util$utils$html_env_QMARK_(){
var temp__4655__auto__ = goog.global.document;
if(cljs.core.truth_(temp__4655__auto__)){
var doc = temp__4655__auto__;
return (doc["write"]);
} else {
return null;
}
});
devcards.util.utils.node_env_QMARK_ = (function devcards$util$utils$node_env_QMARK_(){
return !((goog.nodeGlobalRequire == null));
});
devcards.util.utils.pprint_str = (function devcards$util$utils$pprint_str(obj){
try{var sb__28991__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR_30640_30642 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR_30641_30643 = cljs.core._STAR_print_fn_STAR_;
cljs.core._STAR_print_newline_STAR_ = true;

cljs.core._STAR_print_fn_STAR_ = ((function (_STAR_print_newline_STAR_30640_30642,_STAR_print_fn_STAR_30641_30643,sb__28991__auto__){
return (function (x__28992__auto__){
return sb__28991__auto__.append(x__28992__auto__);
});})(_STAR_print_newline_STAR_30640_30642,_STAR_print_fn_STAR_30641_30643,sb__28991__auto__))
;

try{cljs.pprint.pprint.call(null,obj);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR_30641_30643;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR_30640_30642;
}
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__28991__auto__)].join('');
}catch (e30638){if((e30638 instanceof Error)){
var e1 = e30638;
try{return obj.toString();
}catch (e30639){if((e30639 instanceof Error)){
var e2 = e30639;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1("<<Un-printable Type>>")].join('');
} else {
throw e30639;

}
}} else {
throw e30638;

}
}});
devcards.util.utils.pprint_code = (function devcards$util$utils$pprint_code(code){
var _STAR_print_pprint_dispatch_STAR_30644 = cljs.pprint._STAR_print_pprint_dispatch_STAR_;
cljs.pprint._STAR_print_pprint_dispatch_STAR_ = cljs.pprint.code_dispatch;

try{return devcards.util.utils.pprint_str.call(null,code);
}finally {cljs.pprint._STAR_print_pprint_dispatch_STAR_ = _STAR_print_pprint_dispatch_STAR_30644;
}});

//# sourceMappingURL=utils.js.map?rel=1503198185294
