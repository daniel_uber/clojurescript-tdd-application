// Compiled by ClojureScript 1.9.854 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.Uri');
goog.require('goog.userAgent.product');
goog.require('goog.object');
goog.require('cljs.reader');
goog.require('cljs.core.async');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.utils');
goog.require('figwheel.client.heads_up');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('cljs.repl');
figwheel.client._figwheel_version_ = "0.5.13";
figwheel.client.js_stringify = (((typeof JSON !== 'undefined') && (!((JSON.stringify == null))))?(function (x){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1("#js "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(JSON.stringify(x,null," "))].join('');
}):(function (x){
try{return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(x)].join('');
}catch (e37636){if((e37636 instanceof Error)){
var e = e37636;
return "Error: Unable to stringify";
} else {
throw e37636;

}
}}));
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(var_args){
var G__37639 = arguments.length;
switch (G__37639) {
case 2:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2 = (function (stream,args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"stream","stream",1534941648),stream,new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.mapv.call(null,(function (p1__37637_SHARP_){
if(typeof p1__37637_SHARP_ === 'string'){
return p1__37637_SHARP_;
} else {
return figwheel.client.js_stringify.call(null,p1__37637_SHARP_);
}
}),args)], null)], null));

return null;
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1 = (function (args){
return figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);
});

figwheel.client.figwheel_repl_print.cljs$lang$maxFixedArity = 2;

figwheel.client.console_out_print = (function figwheel$client$console_out_print(args){
return console.log.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.console_err_print = (function figwheel$client$console_err_print(args){
return console.error.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.repl_out_print_fn = (function figwheel$client$repl_out_print_fn(var_args){
var args__29140__auto__ = [];
var len__29133__auto___37642 = arguments.length;
var i__29134__auto___37643 = (0);
while(true){
if((i__29134__auto___37643 < len__29133__auto___37642)){
args__29140__auto__.push((arguments[i__29134__auto___37643]));

var G__37644 = (i__29134__auto___37643 + (1));
i__29134__auto___37643 = G__37644;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});

figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_out_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);

return null;
});

figwheel.client.repl_out_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_out_print_fn.cljs$lang$applyTo = (function (seq37641){
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq37641));
});

figwheel.client.repl_err_print_fn = (function figwheel$client$repl_err_print_fn(var_args){
var args__29140__auto__ = [];
var len__29133__auto___37646 = arguments.length;
var i__29134__auto___37647 = (0);
while(true){
if((i__29134__auto___37647 < len__29133__auto___37646)){
args__29140__auto__.push((arguments[i__29134__auto___37647]));

var G__37648 = (i__29134__auto___37647 + (1));
i__29134__auto___37647 = G__37648;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});

figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_err_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"err","err",-2089457205),args);

return null;
});

figwheel.client.repl_err_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_err_print_fn.cljs$lang$applyTo = (function (seq37645){
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq37645));
});

figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

cljs.core.set_print_fn_BANG_.call(null,figwheel.client.repl_out_print_fn);

cljs.core.set_print_err_fn_BANG_.call(null,figwheel.client.repl_err_print_fn);

return null;
});
figwheel.client.autoload_QMARK_ = (function figwheel$client$autoload_QMARK_(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),true);
});
figwheel.client.toggle_autoload = (function figwheel$client$toggle_autoload(){
var res = figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),cljs.core.not.call(null,figwheel.client.autoload_QMARK_.call(null)));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Toggle autoload deprecated! Use (figwheel.client/set-autoload! false)")].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel autoloading "),cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(figwheel.client.autoload_QMARK_.call(null))?"ON":"OFF"))].join(''));

return res;
});
goog.exportSymbol('figwheel.client.toggle_autoload', figwheel.client.toggle_autoload);
/**
 * Figwheel by default loads code changes as you work. Sometimes you
 *   just want to work on your code without the ramifications of
 *   autoloading and simply load your code piecemeal in the REPL. You can
 *   turn autoloading on and of with this method.
 * 
 *   (figwheel.client/set-autoload false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_autoload = (function figwheel$client$set_autoload(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),b);
});
goog.exportSymbol('figwheel.client.set_autoload', figwheel.client.set_autoload);
figwheel.client.repl_pprint = (function figwheel$client$repl_pprint(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),true);
});
goog.exportSymbol('figwheel.client.repl_pprint', figwheel.client.repl_pprint);
/**
 * This method gives you the ability to turn the pretty printing of
 *   the REPL's return value on and off.
 * 
 *   (figwheel.client/set-repl-pprint false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_repl_pprint = (function figwheel$client$set_repl_pprint(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),b);
});
goog.exportSymbol('figwheel.client.set_repl_pprint', figwheel.client.set_repl_pprint);
figwheel.client.repl_result_pr_str = (function figwheel$client$repl_result_pr_str(v){
if(cljs.core.truth_(figwheel.client.repl_pprint.call(null))){
return figwheel.client.utils.pprint_to_string.call(null,v);
} else {
return cljs.core.pr_str.call(null,v);
}
});
goog.exportSymbol('figwheel.client.repl_result_pr_str', figwheel.client.repl_result_pr_str);
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel.client.get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__37649){
var map__37650 = p__37649;
var map__37650__$1 = ((((!((map__37650 == null)))?((((map__37650.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37650.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37650):map__37650);
var message = cljs.core.get.call(null,map__37650__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var class$ = cljs.core.get.call(null,map__37650__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(class$),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" : "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__27969__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__27957__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__27957__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__27957__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return (cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts)));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__31704__auto___37729 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___37729,ch){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___37729,ch){
return (function (state_37701){
var state_val_37702 = (state_37701[(1)]);
if((state_val_37702 === (7))){
var inst_37697 = (state_37701[(2)]);
var state_37701__$1 = state_37701;
var statearr_37703_37730 = state_37701__$1;
(statearr_37703_37730[(2)] = inst_37697);

(statearr_37703_37730[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (1))){
var state_37701__$1 = state_37701;
var statearr_37704_37731 = state_37701__$1;
(statearr_37704_37731[(2)] = null);

(statearr_37704_37731[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (4))){
var inst_37654 = (state_37701[(7)]);
var inst_37654__$1 = (state_37701[(2)]);
var state_37701__$1 = (function (){var statearr_37705 = state_37701;
(statearr_37705[(7)] = inst_37654__$1);

return statearr_37705;
})();
if(cljs.core.truth_(inst_37654__$1)){
var statearr_37706_37732 = state_37701__$1;
(statearr_37706_37732[(1)] = (5));

} else {
var statearr_37707_37733 = state_37701__$1;
(statearr_37707_37733[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (15))){
var inst_37661 = (state_37701[(8)]);
var inst_37676 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_37661);
var inst_37677 = cljs.core.first.call(null,inst_37676);
var inst_37678 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_37677);
var inst_37679 = [cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel: Not loading code with warnings - "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_37678)].join('');
var inst_37680 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),inst_37679);
var state_37701__$1 = state_37701;
var statearr_37708_37734 = state_37701__$1;
(statearr_37708_37734[(2)] = inst_37680);

(statearr_37708_37734[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (13))){
var inst_37685 = (state_37701[(2)]);
var state_37701__$1 = state_37701;
var statearr_37709_37735 = state_37701__$1;
(statearr_37709_37735[(2)] = inst_37685);

(statearr_37709_37735[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (6))){
var state_37701__$1 = state_37701;
var statearr_37710_37736 = state_37701__$1;
(statearr_37710_37736[(2)] = null);

(statearr_37710_37736[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (17))){
var inst_37683 = (state_37701[(2)]);
var state_37701__$1 = state_37701;
var statearr_37711_37737 = state_37701__$1;
(statearr_37711_37737[(2)] = inst_37683);

(statearr_37711_37737[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (3))){
var inst_37699 = (state_37701[(2)]);
var state_37701__$1 = state_37701;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_37701__$1,inst_37699);
} else {
if((state_val_37702 === (12))){
var inst_37660 = (state_37701[(9)]);
var inst_37674 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_37660,opts);
var state_37701__$1 = state_37701;
if(cljs.core.truth_(inst_37674)){
var statearr_37712_37738 = state_37701__$1;
(statearr_37712_37738[(1)] = (15));

} else {
var statearr_37713_37739 = state_37701__$1;
(statearr_37713_37739[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (2))){
var state_37701__$1 = state_37701;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37701__$1,(4),ch);
} else {
if((state_val_37702 === (11))){
var inst_37661 = (state_37701[(8)]);
var inst_37666 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_37667 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_37661);
var inst_37668 = cljs.core.async.timeout.call(null,(1000));
var inst_37669 = [inst_37667,inst_37668];
var inst_37670 = (new cljs.core.PersistentVector(null,2,(5),inst_37666,inst_37669,null));
var state_37701__$1 = state_37701;
return cljs.core.async.ioc_alts_BANG_.call(null,state_37701__$1,(14),inst_37670);
} else {
if((state_val_37702 === (9))){
var inst_37661 = (state_37701[(8)]);
var inst_37687 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Figwheel: code autoloading is OFF");
var inst_37688 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_37661);
var inst_37689 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_37688);
var inst_37690 = [cljs.core.str.cljs$core$IFn$_invoke$arity$1("Not loading: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_37689)].join('');
var inst_37691 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),inst_37690);
var state_37701__$1 = (function (){var statearr_37714 = state_37701;
(statearr_37714[(10)] = inst_37687);

return statearr_37714;
})();
var statearr_37715_37740 = state_37701__$1;
(statearr_37715_37740[(2)] = inst_37691);

(statearr_37715_37740[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (5))){
var inst_37654 = (state_37701[(7)]);
var inst_37656 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_37657 = (new cljs.core.PersistentArrayMap(null,2,inst_37656,null));
var inst_37658 = (new cljs.core.PersistentHashSet(null,inst_37657,null));
var inst_37659 = figwheel.client.focus_msgs.call(null,inst_37658,inst_37654);
var inst_37660 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_37659);
var inst_37661 = cljs.core.first.call(null,inst_37659);
var inst_37662 = figwheel.client.autoload_QMARK_.call(null);
var state_37701__$1 = (function (){var statearr_37716 = state_37701;
(statearr_37716[(9)] = inst_37660);

(statearr_37716[(8)] = inst_37661);

return statearr_37716;
})();
if(cljs.core.truth_(inst_37662)){
var statearr_37717_37741 = state_37701__$1;
(statearr_37717_37741[(1)] = (8));

} else {
var statearr_37718_37742 = state_37701__$1;
(statearr_37718_37742[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (14))){
var inst_37672 = (state_37701[(2)]);
var state_37701__$1 = state_37701;
var statearr_37719_37743 = state_37701__$1;
(statearr_37719_37743[(2)] = inst_37672);

(statearr_37719_37743[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (16))){
var state_37701__$1 = state_37701;
var statearr_37720_37744 = state_37701__$1;
(statearr_37720_37744[(2)] = null);

(statearr_37720_37744[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (10))){
var inst_37693 = (state_37701[(2)]);
var state_37701__$1 = (function (){var statearr_37721 = state_37701;
(statearr_37721[(11)] = inst_37693);

return statearr_37721;
})();
var statearr_37722_37745 = state_37701__$1;
(statearr_37722_37745[(2)] = null);

(statearr_37722_37745[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37702 === (8))){
var inst_37660 = (state_37701[(9)]);
var inst_37664 = figwheel.client.reload_file_state_QMARK_.call(null,inst_37660,opts);
var state_37701__$1 = state_37701;
if(cljs.core.truth_(inst_37664)){
var statearr_37723_37746 = state_37701__$1;
(statearr_37723_37746[(1)] = (11));

} else {
var statearr_37724_37747 = state_37701__$1;
(statearr_37724_37747[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto___37729,ch))
;
return ((function (switch__31616__auto__,c__31704__auto___37729,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__31617__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__31617__auto____0 = (function (){
var statearr_37725 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_37725[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__31617__auto__);

(statearr_37725[(1)] = (1));

return statearr_37725;
});
var figwheel$client$file_reloader_plugin_$_state_machine__31617__auto____1 = (function (state_37701){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_37701);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e37726){if((e37726 instanceof Object)){
var ex__31620__auto__ = e37726;
var statearr_37727_37748 = state_37701;
(statearr_37727_37748[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_37701);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e37726;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__37749 = state_37701;
state_37701 = G__37749;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__31617__auto__ = function(state_37701){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__31617__auto____1.call(this,state_37701);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloader_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__31617__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__31617__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___37729,ch))
})();
var state__31706__auto__ = (function (){var statearr_37728 = f__31705__auto__.call(null);
(statearr_37728[(6)] = c__31704__auto___37729);

return statearr_37728;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___37729,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__37750_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__37750_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
figwheel.client.get_ua_product = (function figwheel$client$get_ua_product(){
if(cljs.core.truth_(figwheel.client.utils.node_env_QMARK_.call(null))){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
}
});
var base_path_37752 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_37752){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,opts,result_handler){
try{figwheel.client.enable_repl_print_BANG_.call(null);

var result_value = figwheel.client.utils.eval_helper.call(null,code,opts);
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),result_value], null));
}catch (e37751){if((e37751 instanceof Error)){
var e = e37751;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_37752], null));
} else {
var e = e37751;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}finally {figwheel.client.enable_repl_print_BANG_.call(null);
}});})(base_path_37752))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = ({});
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__37753){
var map__37754 = p__37753;
var map__37754__$1 = ((((!((map__37754 == null)))?((((map__37754.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37754.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37754):map__37754);
var opts = map__37754__$1;
var build_id = cljs.core.get.call(null,map__37754__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__37754,map__37754__$1,opts,build_id){
return (function (p__37756){
var vec__37757 = p__37756;
var seq__37758 = cljs.core.seq.call(null,vec__37757);
var first__37759 = cljs.core.first.call(null,seq__37758);
var seq__37758__$1 = cljs.core.next.call(null,seq__37758);
var map__37760 = first__37759;
var map__37760__$1 = ((((!((map__37760 == null)))?((((map__37760.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37760.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37760):map__37760);
var msg = map__37760__$1;
var msg_name = cljs.core.get.call(null,map__37760__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__37758__$1;
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),opts,((function (vec__37757,seq__37758,first__37759,seq__37758__$1,map__37760,map__37760__$1,msg,msg_name,_,map__37754,map__37754__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__37757,seq__37758,first__37759,seq__37758__$1,map__37760,map__37760__$1,msg,msg_name,_,map__37754,map__37754__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__37754,map__37754__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__37762){
var vec__37763 = p__37762;
var seq__37764 = cljs.core.seq.call(null,vec__37763);
var first__37765 = cljs.core.first.call(null,seq__37764);
var seq__37764__$1 = cljs.core.next.call(null,seq__37764);
var map__37766 = first__37765;
var map__37766__$1 = ((((!((map__37766 == null)))?((((map__37766.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37766.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37766):map__37766);
var msg = map__37766__$1;
var msg_name = cljs.core.get.call(null,map__37766__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__37764__$1;
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__37768){
var map__37769 = p__37768;
var map__37769__$1 = ((((!((map__37769 == null)))?((((map__37769.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37769.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37769):map__37769);
var on_compile_warning = cljs.core.get.call(null,map__37769__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
var on_compile_fail = cljs.core.get.call(null,map__37769__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
return ((function (map__37769,map__37769__$1,on_compile_warning,on_compile_fail){
return (function (p__37771){
var vec__37772 = p__37771;
var seq__37773 = cljs.core.seq.call(null,vec__37772);
var first__37774 = cljs.core.first.call(null,seq__37773);
var seq__37773__$1 = cljs.core.next.call(null,seq__37773);
var map__37775 = first__37774;
var map__37775__$1 = ((((!((map__37775 == null)))?((((map__37775.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37775.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__37775):map__37775);
var msg = map__37775__$1;
var msg_name = cljs.core.get.call(null,map__37775__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__37773__$1;
var pred__37777 = cljs.core._EQ_;
var expr__37778 = msg_name;
if(cljs.core.truth_(pred__37777.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__37778))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__37777.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__37778))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__37769,map__37769__$1,on_compile_warning,on_compile_fail))
});
figwheel.client.auto_jump_to_error = (function figwheel$client$auto_jump_to_error(opts,error){
if(cljs.core.truth_(new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920).cljs$core$IFn$_invoke$arity$1(opts))){
return figwheel.client.heads_up.auto_notify_source_file_line.call(null,error);
} else {
return null;
}
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__,msg_hist,msg_names,msg){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__,msg_hist,msg_names,msg){
return (function (state_37867){
var state_val_37868 = (state_37867[(1)]);
if((state_val_37868 === (7))){
var inst_37787 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37787)){
var statearr_37869_37916 = state_37867__$1;
(statearr_37869_37916[(1)] = (8));

} else {
var statearr_37870_37917 = state_37867__$1;
(statearr_37870_37917[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (20))){
var inst_37861 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37871_37918 = state_37867__$1;
(statearr_37871_37918[(2)] = inst_37861);

(statearr_37871_37918[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (27))){
var inst_37857 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37872_37919 = state_37867__$1;
(statearr_37872_37919[(2)] = inst_37857);

(statearr_37872_37919[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (1))){
var inst_37780 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37780)){
var statearr_37873_37920 = state_37867__$1;
(statearr_37873_37920[(1)] = (2));

} else {
var statearr_37874_37921 = state_37867__$1;
(statearr_37874_37921[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (24))){
var inst_37859 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37875_37922 = state_37867__$1;
(statearr_37875_37922[(2)] = inst_37859);

(statearr_37875_37922[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (4))){
var inst_37865 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_37867__$1,inst_37865);
} else {
if((state_val_37868 === (15))){
var inst_37863 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37876_37923 = state_37867__$1;
(statearr_37876_37923[(2)] = inst_37863);

(statearr_37876_37923[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (21))){
var inst_37816 = (state_37867[(2)]);
var inst_37817 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37818 = figwheel.client.auto_jump_to_error.call(null,opts,inst_37817);
var state_37867__$1 = (function (){var statearr_37877 = state_37867;
(statearr_37877[(7)] = inst_37816);

return statearr_37877;
})();
var statearr_37878_37924 = state_37867__$1;
(statearr_37878_37924[(2)] = inst_37818);

(statearr_37878_37924[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (31))){
var inst_37846 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37846)){
var statearr_37879_37925 = state_37867__$1;
(statearr_37879_37925[(1)] = (34));

} else {
var statearr_37880_37926 = state_37867__$1;
(statearr_37880_37926[(1)] = (35));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (32))){
var inst_37855 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37881_37927 = state_37867__$1;
(statearr_37881_37927[(2)] = inst_37855);

(statearr_37881_37927[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (33))){
var inst_37842 = (state_37867[(2)]);
var inst_37843 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37844 = figwheel.client.auto_jump_to_error.call(null,opts,inst_37843);
var state_37867__$1 = (function (){var statearr_37882 = state_37867;
(statearr_37882[(8)] = inst_37842);

return statearr_37882;
})();
var statearr_37883_37928 = state_37867__$1;
(statearr_37883_37928[(2)] = inst_37844);

(statearr_37883_37928[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (13))){
var inst_37801 = figwheel.client.heads_up.clear.call(null);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(16),inst_37801);
} else {
if((state_val_37868 === (22))){
var inst_37822 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37823 = figwheel.client.heads_up.append_warning_message.call(null,inst_37822);
var state_37867__$1 = state_37867;
var statearr_37884_37929 = state_37867__$1;
(statearr_37884_37929[(2)] = inst_37823);

(statearr_37884_37929[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (36))){
var inst_37853 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37885_37930 = state_37867__$1;
(statearr_37885_37930[(2)] = inst_37853);

(statearr_37885_37930[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (29))){
var inst_37833 = (state_37867[(2)]);
var inst_37834 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37835 = figwheel.client.auto_jump_to_error.call(null,opts,inst_37834);
var state_37867__$1 = (function (){var statearr_37886 = state_37867;
(statearr_37886[(9)] = inst_37833);

return statearr_37886;
})();
var statearr_37887_37931 = state_37867__$1;
(statearr_37887_37931[(2)] = inst_37835);

(statearr_37887_37931[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (6))){
var inst_37782 = (state_37867[(10)]);
var state_37867__$1 = state_37867;
var statearr_37888_37932 = state_37867__$1;
(statearr_37888_37932[(2)] = inst_37782);

(statearr_37888_37932[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (28))){
var inst_37829 = (state_37867[(2)]);
var inst_37830 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37831 = figwheel.client.heads_up.display_warning.call(null,inst_37830);
var state_37867__$1 = (function (){var statearr_37889 = state_37867;
(statearr_37889[(11)] = inst_37829);

return statearr_37889;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(29),inst_37831);
} else {
if((state_val_37868 === (25))){
var inst_37827 = figwheel.client.heads_up.clear.call(null);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(28),inst_37827);
} else {
if((state_val_37868 === (34))){
var inst_37848 = figwheel.client.heads_up.flash_loaded.call(null);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(37),inst_37848);
} else {
if((state_val_37868 === (17))){
var inst_37807 = (state_37867[(2)]);
var inst_37808 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37809 = figwheel.client.auto_jump_to_error.call(null,opts,inst_37808);
var state_37867__$1 = (function (){var statearr_37890 = state_37867;
(statearr_37890[(12)] = inst_37807);

return statearr_37890;
})();
var statearr_37891_37933 = state_37867__$1;
(statearr_37891_37933[(2)] = inst_37809);

(statearr_37891_37933[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (3))){
var inst_37799 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37799)){
var statearr_37892_37934 = state_37867__$1;
(statearr_37892_37934[(1)] = (13));

} else {
var statearr_37893_37935 = state_37867__$1;
(statearr_37893_37935[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (12))){
var inst_37795 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37894_37936 = state_37867__$1;
(statearr_37894_37936[(2)] = inst_37795);

(statearr_37894_37936[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (2))){
var inst_37782 = (state_37867[(10)]);
var inst_37782__$1 = figwheel.client.autoload_QMARK_.call(null);
var state_37867__$1 = (function (){var statearr_37895 = state_37867;
(statearr_37895[(10)] = inst_37782__$1);

return statearr_37895;
})();
if(cljs.core.truth_(inst_37782__$1)){
var statearr_37896_37937 = state_37867__$1;
(statearr_37896_37937[(1)] = (5));

} else {
var statearr_37897_37938 = state_37867__$1;
(statearr_37897_37938[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (23))){
var inst_37825 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37825)){
var statearr_37898_37939 = state_37867__$1;
(statearr_37898_37939[(1)] = (25));

} else {
var statearr_37899_37940 = state_37867__$1;
(statearr_37899_37940[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (35))){
var state_37867__$1 = state_37867;
var statearr_37900_37941 = state_37867__$1;
(statearr_37900_37941[(2)] = null);

(statearr_37900_37941[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (19))){
var inst_37820 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37820)){
var statearr_37901_37942 = state_37867__$1;
(statearr_37901_37942[(1)] = (22));

} else {
var statearr_37902_37943 = state_37867__$1;
(statearr_37902_37943[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (11))){
var inst_37791 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37903_37944 = state_37867__$1;
(statearr_37903_37944[(2)] = inst_37791);

(statearr_37903_37944[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (9))){
var inst_37793 = figwheel.client.heads_up.clear.call(null);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(12),inst_37793);
} else {
if((state_val_37868 === (5))){
var inst_37784 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_37867__$1 = state_37867;
var statearr_37904_37945 = state_37867__$1;
(statearr_37904_37945[(2)] = inst_37784);

(statearr_37904_37945[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (14))){
var inst_37811 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37811)){
var statearr_37905_37946 = state_37867__$1;
(statearr_37905_37946[(1)] = (18));

} else {
var statearr_37906_37947 = state_37867__$1;
(statearr_37906_37947[(1)] = (19));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (26))){
var inst_37837 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_37867__$1 = state_37867;
if(cljs.core.truth_(inst_37837)){
var statearr_37907_37948 = state_37867__$1;
(statearr_37907_37948[(1)] = (30));

} else {
var statearr_37908_37949 = state_37867__$1;
(statearr_37908_37949[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (16))){
var inst_37803 = (state_37867[(2)]);
var inst_37804 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37805 = figwheel.client.heads_up.display_exception.call(null,inst_37804);
var state_37867__$1 = (function (){var statearr_37909 = state_37867;
(statearr_37909[(13)] = inst_37803);

return statearr_37909;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(17),inst_37805);
} else {
if((state_val_37868 === (30))){
var inst_37839 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37840 = figwheel.client.heads_up.display_warning.call(null,inst_37839);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(33),inst_37840);
} else {
if((state_val_37868 === (10))){
var inst_37797 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37910_37950 = state_37867__$1;
(statearr_37910_37950[(2)] = inst_37797);

(statearr_37910_37950[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (18))){
var inst_37813 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_37814 = figwheel.client.heads_up.display_exception.call(null,inst_37813);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(21),inst_37814);
} else {
if((state_val_37868 === (37))){
var inst_37850 = (state_37867[(2)]);
var state_37867__$1 = state_37867;
var statearr_37911_37951 = state_37867__$1;
(statearr_37911_37951[(2)] = inst_37850);

(statearr_37911_37951[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37868 === (8))){
var inst_37789 = figwheel.client.heads_up.flash_loaded.call(null);
var state_37867__$1 = state_37867;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37867__$1,(11),inst_37789);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__31616__auto__,c__31704__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto____0 = (function (){
var statearr_37912 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_37912[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto__);

(statearr_37912[(1)] = (1));

return statearr_37912;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto____1 = (function (state_37867){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_37867);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e37913){if((e37913 instanceof Object)){
var ex__31620__auto__ = e37913;
var statearr_37914_37952 = state_37867;
(statearr_37914_37952[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_37867);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e37913;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__37953 = state_37867;
state_37867 = G__37953;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto__ = function(state_37867){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto____1.call(this,state_37867);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__,msg_hist,msg_names,msg))
})();
var state__31706__auto__ = (function (){var statearr_37915 = f__31705__auto__.call(null);
(statearr_37915[(6)] = c__31704__auto__);

return statearr_37915;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__,msg_hist,msg_names,msg))
);

return c__31704__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__31704__auto___37982 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___37982,ch){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___37982,ch){
return (function (state_37968){
var state_val_37969 = (state_37968[(1)]);
if((state_val_37969 === (1))){
var state_37968__$1 = state_37968;
var statearr_37970_37983 = state_37968__$1;
(statearr_37970_37983[(2)] = null);

(statearr_37970_37983[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37969 === (2))){
var state_37968__$1 = state_37968;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37968__$1,(4),ch);
} else {
if((state_val_37969 === (3))){
var inst_37966 = (state_37968[(2)]);
var state_37968__$1 = state_37968;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_37968__$1,inst_37966);
} else {
if((state_val_37969 === (4))){
var inst_37956 = (state_37968[(7)]);
var inst_37956__$1 = (state_37968[(2)]);
var state_37968__$1 = (function (){var statearr_37971 = state_37968;
(statearr_37971[(7)] = inst_37956__$1);

return statearr_37971;
})();
if(cljs.core.truth_(inst_37956__$1)){
var statearr_37972_37984 = state_37968__$1;
(statearr_37972_37984[(1)] = (5));

} else {
var statearr_37973_37985 = state_37968__$1;
(statearr_37973_37985[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37969 === (5))){
var inst_37956 = (state_37968[(7)]);
var inst_37958 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_37956);
var state_37968__$1 = state_37968;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37968__$1,(8),inst_37958);
} else {
if((state_val_37969 === (6))){
var state_37968__$1 = state_37968;
var statearr_37974_37986 = state_37968__$1;
(statearr_37974_37986[(2)] = null);

(statearr_37974_37986[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37969 === (7))){
var inst_37964 = (state_37968[(2)]);
var state_37968__$1 = state_37968;
var statearr_37975_37987 = state_37968__$1;
(statearr_37975_37987[(2)] = inst_37964);

(statearr_37975_37987[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37969 === (8))){
var inst_37960 = (state_37968[(2)]);
var state_37968__$1 = (function (){var statearr_37976 = state_37968;
(statearr_37976[(8)] = inst_37960);

return statearr_37976;
})();
var statearr_37977_37988 = state_37968__$1;
(statearr_37977_37988[(2)] = null);

(statearr_37977_37988[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__31704__auto___37982,ch))
;
return ((function (switch__31616__auto__,c__31704__auto___37982,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__31617__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__31617__auto____0 = (function (){
var statearr_37978 = [null,null,null,null,null,null,null,null,null];
(statearr_37978[(0)] = figwheel$client$heads_up_plugin_$_state_machine__31617__auto__);

(statearr_37978[(1)] = (1));

return statearr_37978;
});
var figwheel$client$heads_up_plugin_$_state_machine__31617__auto____1 = (function (state_37968){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_37968);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e37979){if((e37979 instanceof Object)){
var ex__31620__auto__ = e37979;
var statearr_37980_37989 = state_37968;
(statearr_37980_37989[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_37968);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e37979;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__37990 = state_37968;
state_37968 = G__37990;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__31617__auto__ = function(state_37968){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__31617__auto____1.call(this,state_37968);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$heads_up_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__31617__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__31617__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___37982,ch))
})();
var state__31706__auto__ = (function (){var statearr_37981 = f__31705__auto__.call(null);
(statearr_37981[(6)] = c__31704__auto___37982);

return statearr_37981;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___37982,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__){
return (function (state_37996){
var state_val_37997 = (state_37996[(1)]);
if((state_val_37997 === (1))){
var inst_37991 = cljs.core.async.timeout.call(null,(3000));
var state_37996__$1 = state_37996;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_37996__$1,(2),inst_37991);
} else {
if((state_val_37997 === (2))){
var inst_37993 = (state_37996[(2)]);
var inst_37994 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_37996__$1 = (function (){var statearr_37998 = state_37996;
(statearr_37998[(7)] = inst_37993);

return statearr_37998;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_37996__$1,inst_37994);
} else {
return null;
}
}
});})(c__31704__auto__))
;
return ((function (switch__31616__auto__,c__31704__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__31617__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__31617__auto____0 = (function (){
var statearr_37999 = [null,null,null,null,null,null,null,null];
(statearr_37999[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__31617__auto__);

(statearr_37999[(1)] = (1));

return statearr_37999;
});
var figwheel$client$enforce_project_plugin_$_state_machine__31617__auto____1 = (function (state_37996){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_37996);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e38000){if((e38000 instanceof Object)){
var ex__31620__auto__ = e38000;
var statearr_38001_38003 = state_37996;
(statearr_38001_38003[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_37996);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38000;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38004 = state_37996;
state_37996 = G__38004;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__31617__auto__ = function(state_37996){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__31617__auto____1.call(this,state_37996);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$enforce_project_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__31617__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__31617__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__))
})();
var state__31706__auto__ = (function (){var statearr_38002 = f__31705__auto__.call(null);
(statearr_38002[(6)] = c__31704__auto__);

return statearr_38002;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__))
);

return c__31704__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.enforce_figwheel_version_plugin = (function figwheel$client$enforce_figwheel_version_plugin(opts){
return (function (msg_hist){
var temp__4657__auto__ = new cljs.core.Keyword(null,"figwheel-version","figwheel-version",1409553832).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,msg_hist));
if(cljs.core.truth_(temp__4657__auto__)){
var figwheel_version = temp__4657__auto__;
if(cljs.core.not_EQ_.call(null,figwheel_version,figwheel.client._figwheel_version_)){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different version of Figwheel.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__,figwheel_version,temp__4657__auto__){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__,figwheel_version,temp__4657__auto__){
return (function (state_38011){
var state_val_38012 = (state_38011[(1)]);
if((state_val_38012 === (1))){
var inst_38005 = cljs.core.async.timeout.call(null,(2000));
var state_38011__$1 = state_38011;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_38011__$1,(2),inst_38005);
} else {
if((state_val_38012 === (2))){
var inst_38007 = (state_38011[(2)]);
var inst_38008 = [cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel Client Version <strong>"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client._figwheel_version_),cljs.core.str.cljs$core$IFn$_invoke$arity$1("</strong> is not equal to "),cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel Sidecar Version <strong>"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel_version),cljs.core.str.cljs$core$IFn$_invoke$arity$1("</strong>"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(".  Shutting down Websocket Connection!"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("<h4>To fix try:</h4>"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("<ol><li>Reload this page and make sure you are not getting a cached version of the client.</li>"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("<li>You may have to clean (delete compiled assets) and rebuild to make sure that the new client code is being used.</li>"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("<li>Also, make sure you have consistent Figwheel dependencies.</li></ol>")].join('');
var inst_38009 = figwheel.client.heads_up.display_system_warning.call(null,"Figwheel Client and Server have different versions!!",inst_38008);
var state_38011__$1 = (function (){var statearr_38013 = state_38011;
(statearr_38013[(7)] = inst_38007);

return statearr_38013;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_38011__$1,inst_38009);
} else {
return null;
}
}
});})(c__31704__auto__,figwheel_version,temp__4657__auto__))
;
return ((function (switch__31616__auto__,c__31704__auto__,figwheel_version,temp__4657__auto__){
return (function() {
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto__ = null;
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto____0 = (function (){
var statearr_38014 = [null,null,null,null,null,null,null,null];
(statearr_38014[(0)] = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto__);

(statearr_38014[(1)] = (1));

return statearr_38014;
});
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto____1 = (function (state_38011){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_38011);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e38015){if((e38015 instanceof Object)){
var ex__31620__auto__ = e38015;
var statearr_38016_38018 = state_38011;
(statearr_38016_38018[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_38011);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e38015;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__38019 = state_38011;
state_38011 = G__38019;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto__ = function(state_38011){
switch(arguments.length){
case 0:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto____1.call(this,state_38011);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto____0;
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto____1;
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__,figwheel_version,temp__4657__auto__))
})();
var state__31706__auto__ = (function (){var statearr_38017 = f__31705__auto__.call(null);
(statearr_38017[(6)] = c__31704__auto__);

return statearr_38017;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__,figwheel_version,temp__4657__auto__))
);

return c__31704__auto__;
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.file_line_column = (function figwheel$client$file_line_column(p__38020){
var map__38021 = p__38020;
var map__38021__$1 = ((((!((map__38021 == null)))?((((map__38021.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38021.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38021):map__38021);
var file = cljs.core.get.call(null,map__38021__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__38021__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__38021__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var G__38023 = "";
var G__38023__$1 = (cljs.core.truth_(file)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__38023),cljs.core.str.cljs$core$IFn$_invoke$arity$1("file "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''):G__38023);
var G__38023__$2 = (cljs.core.truth_(line)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__38023__$1),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" at line "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(line)].join(''):G__38023__$1);
if(cljs.core.truth_((function (){var and__27957__auto__ = line;
if(cljs.core.truth_(and__27957__auto__)){
return column;
} else {
return and__27957__auto__;
}
})())){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__38023__$2),cljs.core.str.cljs$core$IFn$_invoke$arity$1(", column "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join('');
} else {
return G__38023__$2;
}
});
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__38024){
var map__38025 = p__38024;
var map__38025__$1 = ((((!((map__38025 == null)))?((((map__38025.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38025.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38025):map__38025);
var ed = map__38025__$1;
var formatted_exception = cljs.core.get.call(null,map__38025__$1,new cljs.core.Keyword(null,"formatted-exception","formatted-exception",-116489026));
var exception_data = cljs.core.get.call(null,map__38025__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var cause = cljs.core.get.call(null,map__38025__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: Compile Exception");

var seq__38027_38031 = cljs.core.seq.call(null,figwheel.client.format_messages.call(null,exception_data));
var chunk__38028_38032 = null;
var count__38029_38033 = (0);
var i__38030_38034 = (0);
while(true){
if((i__38030_38034 < count__38029_38033)){
var msg_38035 = cljs.core._nth.call(null,chunk__38028_38032,i__38030_38034);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_38035);

var G__38036 = seq__38027_38031;
var G__38037 = chunk__38028_38032;
var G__38038 = count__38029_38033;
var G__38039 = (i__38030_38034 + (1));
seq__38027_38031 = G__38036;
chunk__38028_38032 = G__38037;
count__38029_38033 = G__38038;
i__38030_38034 = G__38039;
continue;
} else {
var temp__4657__auto___38040 = cljs.core.seq.call(null,seq__38027_38031);
if(temp__4657__auto___38040){
var seq__38027_38041__$1 = temp__4657__auto___38040;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__38027_38041__$1)){
var c__28803__auto___38042 = cljs.core.chunk_first.call(null,seq__38027_38041__$1);
var G__38043 = cljs.core.chunk_rest.call(null,seq__38027_38041__$1);
var G__38044 = c__28803__auto___38042;
var G__38045 = cljs.core.count.call(null,c__28803__auto___38042);
var G__38046 = (0);
seq__38027_38031 = G__38043;
chunk__38028_38032 = G__38044;
count__38029_38033 = G__38045;
i__38030_38034 = G__38046;
continue;
} else {
var msg_38047 = cljs.core.first.call(null,seq__38027_38041__$1);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_38047);

var G__38048 = cljs.core.next.call(null,seq__38027_38041__$1);
var G__38049 = null;
var G__38050 = (0);
var G__38051 = (0);
seq__38027_38031 = G__38048;
chunk__38028_38032 = G__38049;
count__38029_38033 = G__38050;
i__38030_38034 = G__38051;
continue;
}
} else {
}
}
break;
}

if(cljs.core.truth_(cause)){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Error on "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_line_column.call(null,ed))].join(''));
} else {
}

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__38052){
var map__38053 = p__38052;
var map__38053__$1 = ((((!((map__38053 == null)))?((((map__38053.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38053.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38053):map__38053);
var w = map__38053__$1;
var message = cljs.core.get.call(null,map__38053__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel: Compile Warning - "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(message)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" in "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_line_column.call(null,message))].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if(typeof figwheel.client.config_defaults !== 'undefined'){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"eval-fn","eval-fn",-1111644294),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[new cljs.core.Var(function(){return figwheel.client.default_on_compile_warning;},new cljs.core.Symbol("figwheel.client","default-on-compile-warning","figwheel.client/default-on-compile-warning",584144208,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-warning","default-on-compile-warning",-18911586,null),"resources/public/cljs/tests/out/figwheel/client.cljs",33,1,363,363,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"message","message",1234475525,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"w","w",1994700528,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_warning)?figwheel.client.default_on_compile_warning.cljs$lang$test:null)])),figwheel.client.default_on_jsload,true,new cljs.core.Var(function(){return figwheel.client.default_on_compile_fail;},new cljs.core.Symbol("figwheel.client","default-on-compile-fail","figwheel.client/default-on-compile-fail",1384826337,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-fail","default-on-compile-fail",-158814813,null),"resources/public/cljs/tests/out/figwheel/client.cljs",30,1,355,355,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"formatted-exception","formatted-exception",1524042501,null),new cljs.core.Symbol(null,"exception-data","exception-data",1128056641,null),new cljs.core.Symbol(null,"cause","cause",1872432779,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"ed","ed",2076825751,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_fail)?figwheel.client.default_on_compile_fail.cljs$lang$test:null)])),false,true,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("ws://"),cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),cljs.core.str.cljs$core$IFn$_invoke$arity$1("/figwheel-ws")].join(''),false,figwheel.client.default_before_load,false,false,(100),true,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.fill_url_template = (function figwheel$client$fill_url_template(config){
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
return cljs.core.update_in.call(null,config,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938)], null),(function (x){
return clojure.string.replace.call(null,clojure.string.replace.call(null,x,"[[client-hostname]]",location.hostname),"[[client-port]]",location.port);
}));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"enforce-figwheel-version-plugin","enforce-figwheel-version-plugin",-1916185220),figwheel.client.enforce_figwheel_version_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = ((cljs.core.not.call(null,figwheel.client.utils.html_env_QMARK_.call(null)))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__27957__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__27957__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__27957__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_message_watch = (function figwheel$client$add_message_watch(key,callback){
return cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,key,(function (_,___$1,___$2,msg_hist){
return callback.call(null,cljs.core.first.call(null,msg_hist));
}));
});
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__38055 = cljs.core.seq.call(null,plugins);
var chunk__38056 = null;
var count__38057 = (0);
var i__38058 = (0);
while(true){
if((i__38058 < count__38057)){
var vec__38059 = cljs.core._nth.call(null,chunk__38056,i__38058);
var k = cljs.core.nth.call(null,vec__38059,(0),null);
var plugin = cljs.core.nth.call(null,vec__38059,(1),null);
if(cljs.core.truth_(plugin)){
var pl_38065 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__38055,chunk__38056,count__38057,i__38058,pl_38065,vec__38059,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_38065.call(null,msg_hist);
});})(seq__38055,chunk__38056,count__38057,i__38058,pl_38065,vec__38059,k,plugin))
);
} else {
}

var G__38066 = seq__38055;
var G__38067 = chunk__38056;
var G__38068 = count__38057;
var G__38069 = (i__38058 + (1));
seq__38055 = G__38066;
chunk__38056 = G__38067;
count__38057 = G__38068;
i__38058 = G__38069;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__38055);
if(temp__4657__auto__){
var seq__38055__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__38055__$1)){
var c__28803__auto__ = cljs.core.chunk_first.call(null,seq__38055__$1);
var G__38070 = cljs.core.chunk_rest.call(null,seq__38055__$1);
var G__38071 = c__28803__auto__;
var G__38072 = cljs.core.count.call(null,c__28803__auto__);
var G__38073 = (0);
seq__38055 = G__38070;
chunk__38056 = G__38071;
count__38057 = G__38072;
i__38058 = G__38073;
continue;
} else {
var vec__38062 = cljs.core.first.call(null,seq__38055__$1);
var k = cljs.core.nth.call(null,vec__38062,(0),null);
var plugin = cljs.core.nth.call(null,vec__38062,(1),null);
if(cljs.core.truth_(plugin)){
var pl_38074 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__38055,chunk__38056,count__38057,i__38058,pl_38074,vec__38062,k,plugin,seq__38055__$1,temp__4657__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_38074.call(null,msg_hist);
});})(seq__38055,chunk__38056,count__38057,i__38058,pl_38074,vec__38062,k,plugin,seq__38055__$1,temp__4657__auto__))
);
} else {
}

var G__38075 = cljs.core.next.call(null,seq__38055__$1);
var G__38076 = null;
var G__38077 = (0);
var G__38078 = (0);
seq__38055 = G__38075;
chunk__38056 = G__38076;
count__38057 = G__38077;
i__38058 = G__38078;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(var_args){
var G__38080 = arguments.length;
switch (G__38080) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if(typeof figwheel.client.__figwheel_start_once__ !== 'undefined'){
return null;
} else {
return (
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.fill_url_template.call(null,figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370)))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.enable_repl_print_BANG_.call(null);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

var seq__38081_38086 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"initial-messages","initial-messages",2057377771).cljs$core$IFn$_invoke$arity$1(system_options));
var chunk__38082_38087 = null;
var count__38083_38088 = (0);
var i__38084_38089 = (0);
while(true){
if((i__38084_38089 < count__38083_38088)){
var msg_38090 = cljs.core._nth.call(null,chunk__38082_38087,i__38084_38089);
figwheel.client.socket.handle_incoming_message.call(null,msg_38090);

var G__38091 = seq__38081_38086;
var G__38092 = chunk__38082_38087;
var G__38093 = count__38083_38088;
var G__38094 = (i__38084_38089 + (1));
seq__38081_38086 = G__38091;
chunk__38082_38087 = G__38092;
count__38083_38088 = G__38093;
i__38084_38089 = G__38094;
continue;
} else {
var temp__4657__auto___38095 = cljs.core.seq.call(null,seq__38081_38086);
if(temp__4657__auto___38095){
var seq__38081_38096__$1 = temp__4657__auto___38095;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__38081_38096__$1)){
var c__28803__auto___38097 = cljs.core.chunk_first.call(null,seq__38081_38096__$1);
var G__38098 = cljs.core.chunk_rest.call(null,seq__38081_38096__$1);
var G__38099 = c__28803__auto___38097;
var G__38100 = cljs.core.count.call(null,c__28803__auto___38097);
var G__38101 = (0);
seq__38081_38086 = G__38098;
chunk__38082_38087 = G__38099;
count__38083_38088 = G__38100;
i__38084_38089 = G__38101;
continue;
} else {
var msg_38102 = cljs.core.first.call(null,seq__38081_38096__$1);
figwheel.client.socket.handle_incoming_message.call(null,msg_38102);

var G__38103 = cljs.core.next.call(null,seq__38081_38096__$1);
var G__38104 = null;
var G__38105 = (0);
var G__38106 = (0);
seq__38081_38086 = G__38103;
chunk__38082_38087 = G__38104;
count__38083_38088 = G__38105;
i__38084_38089 = G__38106;
continue;
}
} else {
}
}
break;
}

return figwheel.client.socket.open.call(null,system_options);
})))
;
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;

figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(var_args){
var args__29140__auto__ = [];
var len__29133__auto___38111 = arguments.length;
var i__29134__auto___38112 = (0);
while(true){
if((i__29134__auto___38112 < len__29133__auto___38111)){
args__29140__auto__.push((arguments[i__29134__auto___38112]));

var G__38113 = (i__29134__auto___38112 + (1));
i__29134__auto___38112 = G__38113;
continue;
} else {
}
break;
}

var argseq__29141__auto__ = ((((0) < args__29140__auto__.length))?(new cljs.core.IndexedSeq(args__29140__auto__.slice((0)),(0),null)):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__29141__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__38108){
var map__38109 = p__38108;
var map__38109__$1 = ((((!((map__38109 == null)))?((((map__38109.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38109.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38109):map__38109);
var opts = map__38109__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq38107){
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq38107));
});

figwheel.client.fetch_data_from_env = (function figwheel$client$fetch_data_from_env(){
try{return cljs.reader.read_string.call(null,goog.object.get(window,"FIGWHEEL_CLIENT_CONFIGURATION"));
}catch (e38114){if((e38114 instanceof Error)){
var e = e38114;
cljs.core._STAR_print_err_fn_STAR_.call(null,"Unable to load FIGWHEEL_CLIENT_CONFIGURATION from the environment");

return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"autoload","autoload",-354122500),false], null);
} else {
throw e38114;

}
}});
figwheel.client.console_intro_message = "Figwheel has compiled a temporary helper application to your :output-file.\n\nThe code currently in your configured output file does not\nrepresent the code that you are trying to compile.\n\nThis temporary application is intended to help you continue to get\nfeedback from Figwheel until the build you are working on compiles\ncorrectly.\n\nWhen your ClojureScript source code compiles correctly this helper\napplication will auto-reload and pick up your freshly compiled\nClojureScript program.";
figwheel.client.bad_compile_helper_app = (function figwheel$client$bad_compile_helper_app(){
cljs.core.enable_console_print_BANG_.call(null);

var config = figwheel.client.fetch_data_from_env.call(null);
cljs.core.println.call(null,figwheel.client.console_intro_message);

figwheel.client.heads_up.bad_compile_screen.call(null);

if(cljs.core.truth_(goog.dependencies_)){
} else {
goog.dependencies_ = true;
}

figwheel.client.start.call(null,config);

return figwheel.client.add_message_watch.call(null,new cljs.core.Keyword(null,"listen-for-successful-compile","listen-for-successful-compile",-995277603),((function (config){
return (function (p__38115){
var map__38116 = p__38115;
var map__38116__$1 = ((((!((map__38116 == null)))?((((map__38116.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__38116.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__38116):map__38116);
var msg_name = cljs.core.get.call(null,map__38116__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))){
return location.href = location.href;
} else {
return null;
}
});})(config))
);
});

//# sourceMappingURL=client.js.map?rel=1503198195712
