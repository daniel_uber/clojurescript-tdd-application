// Compiled by ClojureScript 1.9.854 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('figwheel.client.utils');
goog.require('goog.Uri');
goog.require('goog.string');
goog.require('goog.object');
goog.require('goog.net.jsloader');
goog.require('goog.html.legacyconversions');
goog.require('clojure.string');
goog.require('clojure.set');
goog.require('cljs.core.async');
goog.require('goog.async.Deferred');
if(typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined'){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.on_cssload_custom_event = (function figwheel$client$file_reloading$on_cssload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.css-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__27969__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string')) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__27969__auto__){
return or__27969__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return goog.object.get(goog.dependencies_.nameToPath,ns);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return goog.object.get(goog.dependencies_.written,figwheel.client.file_reloading.name__GT_path.call(null,ns));
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__27969__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, ["cljs.nodejs",null,"goog",null,"cljs.core",null], null), null).call(null,name);
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
var or__27969__auto____$1 = goog.string.startsWith("clojure.",name);
if(cljs.core.truth_(or__27969__auto____$1)){
return or__27969__auto____$1;
} else {
return goog.string.startsWith("goog.",name);
}
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__35916_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__35916_SHARP_));
}),goog.object.getKeys(goog.object.get(goog.dependencies_.requires,figwheel.client.file_reloading.name__GT_path.call(null,ns)))));
});
if(typeof figwheel.client.file_reloading.dependency_data !== 'undefined'){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([name]));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([parent_ns]));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__35917 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__35918 = null;
var count__35919 = (0);
var i__35920 = (0);
while(true){
if((i__35920 < count__35919)){
var n = cljs.core._nth.call(null,chunk__35918,i__35920);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__35921 = seq__35917;
var G__35922 = chunk__35918;
var G__35923 = count__35919;
var G__35924 = (i__35920 + (1));
seq__35917 = G__35921;
chunk__35918 = G__35922;
count__35919 = G__35923;
i__35920 = G__35924;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__35917);
if(temp__4657__auto__){
var seq__35917__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35917__$1)){
var c__28803__auto__ = cljs.core.chunk_first.call(null,seq__35917__$1);
var G__35925 = cljs.core.chunk_rest.call(null,seq__35917__$1);
var G__35926 = c__28803__auto__;
var G__35927 = cljs.core.count.call(null,c__28803__auto__);
var G__35928 = (0);
seq__35917 = G__35925;
chunk__35918 = G__35926;
count__35919 = G__35927;
i__35920 = G__35928;
continue;
} else {
var n = cljs.core.first.call(null,seq__35917__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__35929 = cljs.core.next.call(null,seq__35917__$1);
var G__35930 = null;
var G__35931 = (0);
var G__35932 = (0);
seq__35917 = G__35929;
chunk__35918 = G__35930;
count__35919 = G__35931;
i__35920 = G__35932;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__35942_35950 = cljs.core.seq.call(null,deps);
var chunk__35943_35951 = null;
var count__35944_35952 = (0);
var i__35945_35953 = (0);
while(true){
if((i__35945_35953 < count__35944_35952)){
var dep_35954 = cljs.core._nth.call(null,chunk__35943_35951,i__35945_35953);
topo_sort_helper_STAR_.call(null,dep_35954,(depth + (1)),state);

var G__35955 = seq__35942_35950;
var G__35956 = chunk__35943_35951;
var G__35957 = count__35944_35952;
var G__35958 = (i__35945_35953 + (1));
seq__35942_35950 = G__35955;
chunk__35943_35951 = G__35956;
count__35944_35952 = G__35957;
i__35945_35953 = G__35958;
continue;
} else {
var temp__4657__auto___35959 = cljs.core.seq.call(null,seq__35942_35950);
if(temp__4657__auto___35959){
var seq__35942_35960__$1 = temp__4657__auto___35959;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35942_35960__$1)){
var c__28803__auto___35961 = cljs.core.chunk_first.call(null,seq__35942_35960__$1);
var G__35962 = cljs.core.chunk_rest.call(null,seq__35942_35960__$1);
var G__35963 = c__28803__auto___35961;
var G__35964 = cljs.core.count.call(null,c__28803__auto___35961);
var G__35965 = (0);
seq__35942_35950 = G__35962;
chunk__35943_35951 = G__35963;
count__35944_35952 = G__35964;
i__35945_35953 = G__35965;
continue;
} else {
var dep_35966 = cljs.core.first.call(null,seq__35942_35960__$1);
topo_sort_helper_STAR_.call(null,dep_35966,(depth + (1)),state);

var G__35967 = cljs.core.next.call(null,seq__35942_35960__$1);
var G__35968 = null;
var G__35969 = (0);
var G__35970 = (0);
seq__35942_35950 = G__35967;
chunk__35943_35951 = G__35968;
count__35944_35952 = G__35969;
i__35945_35953 = G__35970;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__35946){
var vec__35947 = p__35946;
var seq__35948 = cljs.core.seq.call(null,vec__35947);
var first__35949 = cljs.core.first.call(null,seq__35948);
var seq__35948__$1 = cljs.core.next.call(null,seq__35948);
var x = first__35949;
var xs = seq__35948__$1;
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__35947,seq__35948,first__35949,seq__35948__$1,x,xs,get_deps__$1){
return (function (p1__35933_SHARP_){
return clojure.set.difference.call(null,p1__35933_SHARP_,x);
});})(vec__35947,seq__35948,first__35949,seq__35948__$1,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,figwheel.client.file_reloading.immutable_ns_QMARK_),cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss)))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__35971 = cljs.core.seq.call(null,provides);
var chunk__35972 = null;
var count__35973 = (0);
var i__35974 = (0);
while(true){
if((i__35974 < count__35973)){
var prov = cljs.core._nth.call(null,chunk__35972,i__35974);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__35975_35983 = cljs.core.seq.call(null,requires);
var chunk__35976_35984 = null;
var count__35977_35985 = (0);
var i__35978_35986 = (0);
while(true){
if((i__35978_35986 < count__35977_35985)){
var req_35987 = cljs.core._nth.call(null,chunk__35976_35984,i__35978_35986);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_35987,prov);

var G__35988 = seq__35975_35983;
var G__35989 = chunk__35976_35984;
var G__35990 = count__35977_35985;
var G__35991 = (i__35978_35986 + (1));
seq__35975_35983 = G__35988;
chunk__35976_35984 = G__35989;
count__35977_35985 = G__35990;
i__35978_35986 = G__35991;
continue;
} else {
var temp__4657__auto___35992 = cljs.core.seq.call(null,seq__35975_35983);
if(temp__4657__auto___35992){
var seq__35975_35993__$1 = temp__4657__auto___35992;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35975_35993__$1)){
var c__28803__auto___35994 = cljs.core.chunk_first.call(null,seq__35975_35993__$1);
var G__35995 = cljs.core.chunk_rest.call(null,seq__35975_35993__$1);
var G__35996 = c__28803__auto___35994;
var G__35997 = cljs.core.count.call(null,c__28803__auto___35994);
var G__35998 = (0);
seq__35975_35983 = G__35995;
chunk__35976_35984 = G__35996;
count__35977_35985 = G__35997;
i__35978_35986 = G__35998;
continue;
} else {
var req_35999 = cljs.core.first.call(null,seq__35975_35993__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_35999,prov);

var G__36000 = cljs.core.next.call(null,seq__35975_35993__$1);
var G__36001 = null;
var G__36002 = (0);
var G__36003 = (0);
seq__35975_35983 = G__36000;
chunk__35976_35984 = G__36001;
count__35977_35985 = G__36002;
i__35978_35986 = G__36003;
continue;
}
} else {
}
}
break;
}

var G__36004 = seq__35971;
var G__36005 = chunk__35972;
var G__36006 = count__35973;
var G__36007 = (i__35974 + (1));
seq__35971 = G__36004;
chunk__35972 = G__36005;
count__35973 = G__36006;
i__35974 = G__36007;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__35971);
if(temp__4657__auto__){
var seq__35971__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35971__$1)){
var c__28803__auto__ = cljs.core.chunk_first.call(null,seq__35971__$1);
var G__36008 = cljs.core.chunk_rest.call(null,seq__35971__$1);
var G__36009 = c__28803__auto__;
var G__36010 = cljs.core.count.call(null,c__28803__auto__);
var G__36011 = (0);
seq__35971 = G__36008;
chunk__35972 = G__36009;
count__35973 = G__36010;
i__35974 = G__36011;
continue;
} else {
var prov = cljs.core.first.call(null,seq__35971__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__35979_36012 = cljs.core.seq.call(null,requires);
var chunk__35980_36013 = null;
var count__35981_36014 = (0);
var i__35982_36015 = (0);
while(true){
if((i__35982_36015 < count__35981_36014)){
var req_36016 = cljs.core._nth.call(null,chunk__35980_36013,i__35982_36015);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_36016,prov);

var G__36017 = seq__35979_36012;
var G__36018 = chunk__35980_36013;
var G__36019 = count__35981_36014;
var G__36020 = (i__35982_36015 + (1));
seq__35979_36012 = G__36017;
chunk__35980_36013 = G__36018;
count__35981_36014 = G__36019;
i__35982_36015 = G__36020;
continue;
} else {
var temp__4657__auto___36021__$1 = cljs.core.seq.call(null,seq__35979_36012);
if(temp__4657__auto___36021__$1){
var seq__35979_36022__$1 = temp__4657__auto___36021__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35979_36022__$1)){
var c__28803__auto___36023 = cljs.core.chunk_first.call(null,seq__35979_36022__$1);
var G__36024 = cljs.core.chunk_rest.call(null,seq__35979_36022__$1);
var G__36025 = c__28803__auto___36023;
var G__36026 = cljs.core.count.call(null,c__28803__auto___36023);
var G__36027 = (0);
seq__35979_36012 = G__36024;
chunk__35980_36013 = G__36025;
count__35981_36014 = G__36026;
i__35982_36015 = G__36027;
continue;
} else {
var req_36028 = cljs.core.first.call(null,seq__35979_36022__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_36028,prov);

var G__36029 = cljs.core.next.call(null,seq__35979_36022__$1);
var G__36030 = null;
var G__36031 = (0);
var G__36032 = (0);
seq__35979_36012 = G__36029;
chunk__35980_36013 = G__36030;
count__35981_36014 = G__36031;
i__35982_36015 = G__36032;
continue;
}
} else {
}
}
break;
}

var G__36033 = cljs.core.next.call(null,seq__35971__$1);
var G__36034 = null;
var G__36035 = (0);
var G__36036 = (0);
seq__35971 = G__36033;
chunk__35972 = G__36034;
count__35973 = G__36035;
i__35974 = G__36036;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel.client.file_reloading.figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__36037_36041 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__36038_36042 = null;
var count__36039_36043 = (0);
var i__36040_36044 = (0);
while(true){
if((i__36040_36044 < count__36039_36043)){
var ns_36045 = cljs.core._nth.call(null,chunk__36038_36042,i__36040_36044);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_36045);

var G__36046 = seq__36037_36041;
var G__36047 = chunk__36038_36042;
var G__36048 = count__36039_36043;
var G__36049 = (i__36040_36044 + (1));
seq__36037_36041 = G__36046;
chunk__36038_36042 = G__36047;
count__36039_36043 = G__36048;
i__36040_36044 = G__36049;
continue;
} else {
var temp__4657__auto___36050 = cljs.core.seq.call(null,seq__36037_36041);
if(temp__4657__auto___36050){
var seq__36037_36051__$1 = temp__4657__auto___36050;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36037_36051__$1)){
var c__28803__auto___36052 = cljs.core.chunk_first.call(null,seq__36037_36051__$1);
var G__36053 = cljs.core.chunk_rest.call(null,seq__36037_36051__$1);
var G__36054 = c__28803__auto___36052;
var G__36055 = cljs.core.count.call(null,c__28803__auto___36052);
var G__36056 = (0);
seq__36037_36041 = G__36053;
chunk__36038_36042 = G__36054;
count__36039_36043 = G__36055;
i__36040_36044 = G__36056;
continue;
} else {
var ns_36057 = cljs.core.first.call(null,seq__36037_36051__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_36057);

var G__36058 = cljs.core.next.call(null,seq__36037_36051__$1);
var G__36059 = null;
var G__36060 = (0);
var G__36061 = (0);
seq__36037_36041 = G__36058;
chunk__36038_36042 = G__36059;
count__36039_36043 = G__36060;
i__36040_36044 = G__36061;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__27969__auto__ = goog.require__;
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__36062__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__36062 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__36063__i = 0, G__36063__a = new Array(arguments.length -  0);
while (G__36063__i < G__36063__a.length) {G__36063__a[G__36063__i] = arguments[G__36063__i + 0]; ++G__36063__i;}
  args = new cljs.core.IndexedSeq(G__36063__a,0,null);
} 
return G__36062__delegate.call(this,args);};
G__36062.cljs$lang$maxFixedArity = 0;
G__36062.cljs$lang$applyTo = (function (arglist__36064){
var args = cljs.core.seq(arglist__36064);
return G__36062__delegate(args);
});
G__36062.cljs$core$IFn$_invoke$arity$variadic = G__36062__delegate;
return G__36062;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if(typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined'){
return null;
} else {
return (
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
)
;
}
});
figwheel.client.file_reloading.gloader = ((typeof goog.net.jsloader.safeLoad !== 'undefined')?(function (p1__36065_SHARP_,p2__36066_SHARP_){
return goog.net.jsloader.safeLoad(goog.html.legacyconversions.trustedResourceUrlFromString([cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__36065_SHARP_)].join('')),p2__36066_SHARP_);
}):((typeof goog.net.jsloader.load !== 'undefined')?(function (p1__36067_SHARP_,p2__36068_SHARP_){
return goog.net.jsloader.load([cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__36067_SHARP_)].join(''),p2__36068_SHARP_);
}):(function(){throw cljs.core.ex_info.call(null,"No remote script loading function found.",cljs.core.PersistentArrayMap.EMPTY)})()
));
figwheel.client.file_reloading.reload_file_in_html_env = (function figwheel$client$file_reloading$reload_file_in_html_env(request_url,callback){

var G__36069 = figwheel.client.file_reloading.gloader.call(null,figwheel.client.file_reloading.add_cache_buster.call(null,request_url),({"cleanupWhenDone": true}));
G__36069.addCallback(((function (G__36069){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(G__36069))
);

G__36069.addErrback(((function (G__36069){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(G__36069))
);

return G__36069;
});
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__36070 = cljs.core._EQ_;
var expr__36071 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__36070.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__36071))){
var node_path_lib = require("path");
var util_pattern = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.sep),cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.join("goog","bootstrap","nodejs.js"))].join('');
var util_path = goog.object.findKey(require.cache,((function (node_path_lib,util_pattern,pred__36070,expr__36071){
return (function (v,k,o){
return goog.string.endsWith(k,util_pattern);
});})(node_path_lib,util_pattern,pred__36070,expr__36071))
);
var parts = cljs.core.pop.call(null,cljs.core.pop.call(null,clojure.string.split.call(null,util_path,/[\/\\]/)));
var root_path = clojure.string.join.call(null,node_path_lib.sep,parts);
return ((function (node_path_lib,util_pattern,util_path,parts,root_path,pred__36070,expr__36071){
return (function (request_url,callback){

var cache_path = node_path_lib.resolve(root_path,request_url);
goog.object.remove(require.cache,cache_path);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e36073){if((e36073 instanceof Error)){
var e = e36073;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel: Error loading file "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e36073;

}
}})());
});
;})(node_path_lib,util_pattern,util_path,parts,root_path,pred__36070,expr__36071))
} else {
if(cljs.core.truth_(pred__36070.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__36071))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__36070.call(null,new cljs.core.Keyword(null,"react-native","react-native",-1543085138),expr__36071))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__36070.call(null,new cljs.core.Keyword(null,"worker","worker",938239996),expr__36071))){
return ((function (pred__36070,expr__36071){
return (function (request_url,callback){

return callback.call(null,(function (){try{self.importScripts(figwheel.client.file_reloading.add_cache_buster.call(null,request_url));

return true;
}catch (e36074){if((e36074 instanceof Error)){
var e = e36074;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel: Error loading file "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e36074;

}
}})());
});
;})(pred__36070,expr__36071))
} else {
return ((function (pred__36070,expr__36071){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__36070,expr__36071))
}
}
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__36075,callback){
var map__36076 = p__36075;
var map__36076__$1 = ((((!((map__36076 == null)))?((((map__36076.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36076.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36076):map__36076);
var file_msg = map__36076__$1;
var request_url = cljs.core.get.call(null,map__36076__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("FigWheel: Attempting to load "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__36076,map__36076__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("FigWheel: Successfully loaded "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel: Error loading file "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__36076,map__36076__$1,file_msg,request_url))
);
});
if(typeof figwheel.client.file_reloading.reload_chan !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if(typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined'){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if(typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined'){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if(typeof figwheel.client.file_reloading.reloader_loop !== 'undefined'){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__){
return (function (state_36100){
var state_val_36101 = (state_36100[(1)]);
if((state_val_36101 === (7))){
var inst_36096 = (state_36100[(2)]);
var state_36100__$1 = state_36100;
var statearr_36102_36119 = state_36100__$1;
(statearr_36102_36119[(2)] = inst_36096);

(statearr_36102_36119[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36101 === (1))){
var state_36100__$1 = state_36100;
var statearr_36103_36120 = state_36100__$1;
(statearr_36103_36120[(2)] = null);

(statearr_36103_36120[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36101 === (4))){
var inst_36080 = (state_36100[(7)]);
var inst_36080__$1 = (state_36100[(2)]);
var state_36100__$1 = (function (){var statearr_36104 = state_36100;
(statearr_36104[(7)] = inst_36080__$1);

return statearr_36104;
})();
if(cljs.core.truth_(inst_36080__$1)){
var statearr_36105_36121 = state_36100__$1;
(statearr_36105_36121[(1)] = (5));

} else {
var statearr_36106_36122 = state_36100__$1;
(statearr_36106_36122[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36101 === (6))){
var state_36100__$1 = state_36100;
var statearr_36107_36123 = state_36100__$1;
(statearr_36107_36123[(2)] = null);

(statearr_36107_36123[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36101 === (3))){
var inst_36098 = (state_36100[(2)]);
var state_36100__$1 = state_36100;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36100__$1,inst_36098);
} else {
if((state_val_36101 === (2))){
var state_36100__$1 = state_36100;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36100__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_36101 === (11))){
var inst_36092 = (state_36100[(2)]);
var state_36100__$1 = (function (){var statearr_36108 = state_36100;
(statearr_36108[(8)] = inst_36092);

return statearr_36108;
})();
var statearr_36109_36124 = state_36100__$1;
(statearr_36109_36124[(2)] = null);

(statearr_36109_36124[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36101 === (9))){
var inst_36084 = (state_36100[(9)]);
var inst_36086 = (state_36100[(10)]);
var inst_36088 = inst_36086.call(null,inst_36084);
var state_36100__$1 = state_36100;
var statearr_36110_36125 = state_36100__$1;
(statearr_36110_36125[(2)] = inst_36088);

(statearr_36110_36125[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36101 === (5))){
var inst_36080 = (state_36100[(7)]);
var inst_36082 = figwheel.client.file_reloading.blocking_load.call(null,inst_36080);
var state_36100__$1 = state_36100;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36100__$1,(8),inst_36082);
} else {
if((state_val_36101 === (10))){
var inst_36084 = (state_36100[(9)]);
var inst_36090 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_36084);
var state_36100__$1 = state_36100;
var statearr_36111_36126 = state_36100__$1;
(statearr_36111_36126[(2)] = inst_36090);

(statearr_36111_36126[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36101 === (8))){
var inst_36086 = (state_36100[(10)]);
var inst_36080 = (state_36100[(7)]);
var inst_36084 = (state_36100[(2)]);
var inst_36085 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_36086__$1 = cljs.core.get.call(null,inst_36085,inst_36080);
var state_36100__$1 = (function (){var statearr_36112 = state_36100;
(statearr_36112[(9)] = inst_36084);

(statearr_36112[(10)] = inst_36086__$1);

return statearr_36112;
})();
if(cljs.core.truth_(inst_36086__$1)){
var statearr_36113_36127 = state_36100__$1;
(statearr_36113_36127[(1)] = (9));

} else {
var statearr_36114_36128 = state_36100__$1;
(statearr_36114_36128[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto__))
;
return ((function (switch__31616__auto__,c__31704__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__31617__auto__ = null;
var figwheel$client$file_reloading$state_machine__31617__auto____0 = (function (){
var statearr_36115 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_36115[(0)] = figwheel$client$file_reloading$state_machine__31617__auto__);

(statearr_36115[(1)] = (1));

return statearr_36115;
});
var figwheel$client$file_reloading$state_machine__31617__auto____1 = (function (state_36100){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_36100);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e36116){if((e36116 instanceof Object)){
var ex__31620__auto__ = e36116;
var statearr_36117_36129 = state_36100;
(statearr_36117_36129[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36100);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36116;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36130 = state_36100;
state_36100 = G__36130;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__31617__auto__ = function(state_36100){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__31617__auto____1.call(this,state_36100);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__31617__auto____0;
figwheel$client$file_reloading$state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__31617__auto____1;
return figwheel$client$file_reloading$state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__))
})();
var state__31706__auto__ = (function (){var statearr_36118 = f__31705__auto__.call(null);
(statearr_36118[(6)] = c__31704__auto__);

return statearr_36118;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__))
);

return c__31704__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(url){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,url);
});
figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__36131,callback){
var map__36132 = p__36131;
var map__36132__$1 = ((((!((map__36132 == null)))?((((map__36132.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36132.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36132):map__36132);
var file_msg = map__36132__$1;
var namespace = cljs.core.get.call(null,map__36132__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__36132,map__36132__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__36132,map__36132__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.figwheel_no_load_QMARK_ = (function figwheel$client$file_reloading$figwheel_no_load_QMARK_(p__36134){
var map__36135 = p__36134;
var map__36135__$1 = ((((!((map__36135 == null)))?((((map__36135.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36135.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36135):map__36135);
var file_msg = map__36135__$1;
var namespace = cljs.core.get.call(null,map__36135__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__36137){
var map__36138 = p__36137;
var map__36138__$1 = ((((!((map__36138 == null)))?((((map__36138.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36138.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36138):map__36138);
var file_msg = map__36138__$1;
var namespace = cljs.core.get.call(null,map__36138__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__27957__auto__ = cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,file_msg));
if(and__27957__auto__){
var or__27969__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__27969__auto__)){
return or__27969__auto__;
} else {
var or__27969__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__27969__auto____$1)){
return or__27969__auto____$1;
} else {
return figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
}
}
} else {
return and__27957__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__36140,callback){
var map__36141 = p__36140;
var map__36141__$1 = ((((!((map__36141 == null)))?((((map__36141.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36141.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36141):map__36141);
var file_msg = map__36141__$1;
var request_url = cljs.core.get.call(null,map__36141__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__36141__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Figwheel: Not trying to load file "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__31704__auto___36191 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto___36191,out){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto___36191,out){
return (function (state_36176){
var state_val_36177 = (state_36176[(1)]);
if((state_val_36177 === (1))){
var inst_36150 = cljs.core.seq.call(null,files);
var inst_36151 = cljs.core.first.call(null,inst_36150);
var inst_36152 = cljs.core.next.call(null,inst_36150);
var inst_36153 = files;
var state_36176__$1 = (function (){var statearr_36178 = state_36176;
(statearr_36178[(7)] = inst_36152);

(statearr_36178[(8)] = inst_36153);

(statearr_36178[(9)] = inst_36151);

return statearr_36178;
})();
var statearr_36179_36192 = state_36176__$1;
(statearr_36179_36192[(2)] = null);

(statearr_36179_36192[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36177 === (2))){
var inst_36153 = (state_36176[(8)]);
var inst_36159 = (state_36176[(10)]);
var inst_36158 = cljs.core.seq.call(null,inst_36153);
var inst_36159__$1 = cljs.core.first.call(null,inst_36158);
var inst_36160 = cljs.core.next.call(null,inst_36158);
var inst_36161 = (inst_36159__$1 == null);
var inst_36162 = cljs.core.not.call(null,inst_36161);
var state_36176__$1 = (function (){var statearr_36180 = state_36176;
(statearr_36180[(11)] = inst_36160);

(statearr_36180[(10)] = inst_36159__$1);

return statearr_36180;
})();
if(inst_36162){
var statearr_36181_36193 = state_36176__$1;
(statearr_36181_36193[(1)] = (4));

} else {
var statearr_36182_36194 = state_36176__$1;
(statearr_36182_36194[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36177 === (3))){
var inst_36174 = (state_36176[(2)]);
var state_36176__$1 = state_36176;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36176__$1,inst_36174);
} else {
if((state_val_36177 === (4))){
var inst_36159 = (state_36176[(10)]);
var inst_36164 = figwheel.client.file_reloading.reload_js_file.call(null,inst_36159);
var state_36176__$1 = state_36176;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36176__$1,(7),inst_36164);
} else {
if((state_val_36177 === (5))){
var inst_36170 = cljs.core.async.close_BANG_.call(null,out);
var state_36176__$1 = state_36176;
var statearr_36183_36195 = state_36176__$1;
(statearr_36183_36195[(2)] = inst_36170);

(statearr_36183_36195[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36177 === (6))){
var inst_36172 = (state_36176[(2)]);
var state_36176__$1 = state_36176;
var statearr_36184_36196 = state_36176__$1;
(statearr_36184_36196[(2)] = inst_36172);

(statearr_36184_36196[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36177 === (7))){
var inst_36160 = (state_36176[(11)]);
var inst_36166 = (state_36176[(2)]);
var inst_36167 = cljs.core.async.put_BANG_.call(null,out,inst_36166);
var inst_36153 = inst_36160;
var state_36176__$1 = (function (){var statearr_36185 = state_36176;
(statearr_36185[(12)] = inst_36167);

(statearr_36185[(8)] = inst_36153);

return statearr_36185;
})();
var statearr_36186_36197 = state_36176__$1;
(statearr_36186_36197[(2)] = null);

(statearr_36186_36197[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__31704__auto___36191,out))
;
return ((function (switch__31616__auto__,c__31704__auto___36191,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto____0 = (function (){
var statearr_36187 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_36187[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto__);

(statearr_36187[(1)] = (1));

return statearr_36187;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto____1 = (function (state_36176){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_36176);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e36188){if((e36188 instanceof Object)){
var ex__31620__auto__ = e36188;
var statearr_36189_36198 = state_36176;
(statearr_36189_36198[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36176);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36188;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36199 = state_36176;
state_36176 = G__36199;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto__ = function(state_36176){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto____1.call(this,state_36176);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto___36191,out))
})();
var state__31706__auto__ = (function (){var statearr_36190 = f__31705__auto__.call(null);
(statearr_36190[(6)] = c__31704__auto___36191);

return statearr_36190;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto___36191,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__36200,opts){
var map__36201 = p__36200;
var map__36201__$1 = ((((!((map__36201 == null)))?((((map__36201.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36201.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36201):map__36201);
var eval_body = cljs.core.get.call(null,map__36201__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__36201__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__27957__auto__ = eval_body;
if(cljs.core.truth_(and__27957__auto__)){
return typeof eval_body === 'string';
} else {
return and__27957__auto__;
}
})())){
var code = eval_body;
try{figwheel.client.utils.debug_prn.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Evaling file "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e36203){var e = e36203;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("Unable to evaluate "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,cljs.core.partial.call(null,cljs.core.re_matches,/figwheel\.connect.*/),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__4655__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__36204_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__36204_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__4655__auto__)){
var file_msg = temp__4655__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__36205){
var vec__36206 = p__36205;
var k = cljs.core.nth.call(null,vec__36206,(0),null);
var v = cljs.core.nth.call(null,vec__36206,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__36209){
var vec__36210 = p__36209;
var k = cljs.core.nth.call(null,vec__36210,(0),null);
var v = cljs.core.nth.call(null,vec__36210,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__36216,p__36217){
var map__36218 = p__36216;
var map__36218__$1 = ((((!((map__36218 == null)))?((((map__36218.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36218.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36218):map__36218);
var opts = map__36218__$1;
var before_jsload = cljs.core.get.call(null,map__36218__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__36218__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__36218__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__36219 = p__36217;
var map__36219__$1 = ((((!((map__36219 == null)))?((((map__36219.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36219.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36219):map__36219);
var msg = map__36219__$1;
var files = cljs.core.get.call(null,map__36219__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__36219__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__36219__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__31704__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__31705__auto__ = (function (){var switch__31616__auto__ = ((function (c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_36373){
var state_val_36374 = (state_36373[(1)]);
if((state_val_36374 === (7))){
var inst_36233 = (state_36373[(7)]);
var inst_36234 = (state_36373[(8)]);
var inst_36235 = (state_36373[(9)]);
var inst_36236 = (state_36373[(10)]);
var inst_36241 = cljs.core._nth.call(null,inst_36234,inst_36236);
var inst_36242 = figwheel.client.file_reloading.eval_body.call(null,inst_36241,opts);
var inst_36243 = (inst_36236 + (1));
var tmp36375 = inst_36233;
var tmp36376 = inst_36234;
var tmp36377 = inst_36235;
var inst_36233__$1 = tmp36375;
var inst_36234__$1 = tmp36376;
var inst_36235__$1 = tmp36377;
var inst_36236__$1 = inst_36243;
var state_36373__$1 = (function (){var statearr_36378 = state_36373;
(statearr_36378[(7)] = inst_36233__$1);

(statearr_36378[(8)] = inst_36234__$1);

(statearr_36378[(9)] = inst_36235__$1);

(statearr_36378[(10)] = inst_36236__$1);

(statearr_36378[(11)] = inst_36242);

return statearr_36378;
})();
var statearr_36379_36462 = state_36373__$1;
(statearr_36379_36462[(2)] = null);

(statearr_36379_36462[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (20))){
var inst_36276 = (state_36373[(12)]);
var inst_36284 = figwheel.client.file_reloading.sort_files.call(null,inst_36276);
var state_36373__$1 = state_36373;
var statearr_36380_36463 = state_36373__$1;
(statearr_36380_36463[(2)] = inst_36284);

(statearr_36380_36463[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (27))){
var state_36373__$1 = state_36373;
var statearr_36381_36464 = state_36373__$1;
(statearr_36381_36464[(2)] = null);

(statearr_36381_36464[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (1))){
var inst_36225 = (state_36373[(13)]);
var inst_36222 = before_jsload.call(null,files);
var inst_36223 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_36224 = (function (){return ((function (inst_36225,inst_36222,inst_36223,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__36213_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__36213_SHARP_);
});
;})(inst_36225,inst_36222,inst_36223,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_36225__$1 = cljs.core.filter.call(null,inst_36224,files);
var inst_36226 = cljs.core.not_empty.call(null,inst_36225__$1);
var state_36373__$1 = (function (){var statearr_36382 = state_36373;
(statearr_36382[(14)] = inst_36222);

(statearr_36382[(15)] = inst_36223);

(statearr_36382[(13)] = inst_36225__$1);

return statearr_36382;
})();
if(cljs.core.truth_(inst_36226)){
var statearr_36383_36465 = state_36373__$1;
(statearr_36383_36465[(1)] = (2));

} else {
var statearr_36384_36466 = state_36373__$1;
(statearr_36384_36466[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (24))){
var state_36373__$1 = state_36373;
var statearr_36385_36467 = state_36373__$1;
(statearr_36385_36467[(2)] = null);

(statearr_36385_36467[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (39))){
var inst_36326 = (state_36373[(16)]);
var state_36373__$1 = state_36373;
var statearr_36386_36468 = state_36373__$1;
(statearr_36386_36468[(2)] = inst_36326);

(statearr_36386_36468[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (46))){
var inst_36368 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
var statearr_36387_36469 = state_36373__$1;
(statearr_36387_36469[(2)] = inst_36368);

(statearr_36387_36469[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (4))){
var inst_36270 = (state_36373[(2)]);
var inst_36271 = cljs.core.List.EMPTY;
var inst_36272 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_36271);
var inst_36273 = (function (){return ((function (inst_36270,inst_36271,inst_36272,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__36214_SHARP_){
var and__27957__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__36214_SHARP_);
if(cljs.core.truth_(and__27957__auto__)){
return (cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__36214_SHARP_))) && (cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,p1__36214_SHARP_)));
} else {
return and__27957__auto__;
}
});
;})(inst_36270,inst_36271,inst_36272,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_36274 = cljs.core.filter.call(null,inst_36273,files);
var inst_36275 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_36276 = cljs.core.concat.call(null,inst_36274,inst_36275);
var state_36373__$1 = (function (){var statearr_36388 = state_36373;
(statearr_36388[(12)] = inst_36276);

(statearr_36388[(17)] = inst_36270);

(statearr_36388[(18)] = inst_36272);

return statearr_36388;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_36389_36470 = state_36373__$1;
(statearr_36389_36470[(1)] = (16));

} else {
var statearr_36390_36471 = state_36373__$1;
(statearr_36390_36471[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (15))){
var inst_36260 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
var statearr_36391_36472 = state_36373__$1;
(statearr_36391_36472[(2)] = inst_36260);

(statearr_36391_36472[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (21))){
var inst_36286 = (state_36373[(19)]);
var inst_36286__$1 = (state_36373[(2)]);
var inst_36287 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_36286__$1);
var state_36373__$1 = (function (){var statearr_36392 = state_36373;
(statearr_36392[(19)] = inst_36286__$1);

return statearr_36392;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36373__$1,(22),inst_36287);
} else {
if((state_val_36374 === (31))){
var inst_36371 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36373__$1,inst_36371);
} else {
if((state_val_36374 === (32))){
var inst_36326 = (state_36373[(16)]);
var inst_36331 = inst_36326.cljs$lang$protocol_mask$partition0$;
var inst_36332 = (inst_36331 & (64));
var inst_36333 = inst_36326.cljs$core$ISeq$;
var inst_36334 = (cljs.core.PROTOCOL_SENTINEL === inst_36333);
var inst_36335 = (inst_36332) || (inst_36334);
var state_36373__$1 = state_36373;
if(cljs.core.truth_(inst_36335)){
var statearr_36393_36473 = state_36373__$1;
(statearr_36393_36473[(1)] = (35));

} else {
var statearr_36394_36474 = state_36373__$1;
(statearr_36394_36474[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (40))){
var inst_36348 = (state_36373[(20)]);
var inst_36347 = (state_36373[(2)]);
var inst_36348__$1 = cljs.core.get.call(null,inst_36347,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_36349 = cljs.core.get.call(null,inst_36347,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_36350 = cljs.core.not_empty.call(null,inst_36348__$1);
var state_36373__$1 = (function (){var statearr_36395 = state_36373;
(statearr_36395[(21)] = inst_36349);

(statearr_36395[(20)] = inst_36348__$1);

return statearr_36395;
})();
if(cljs.core.truth_(inst_36350)){
var statearr_36396_36475 = state_36373__$1;
(statearr_36396_36475[(1)] = (41));

} else {
var statearr_36397_36476 = state_36373__$1;
(statearr_36397_36476[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (33))){
var state_36373__$1 = state_36373;
var statearr_36398_36477 = state_36373__$1;
(statearr_36398_36477[(2)] = false);

(statearr_36398_36477[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (13))){
var inst_36246 = (state_36373[(22)]);
var inst_36250 = cljs.core.chunk_first.call(null,inst_36246);
var inst_36251 = cljs.core.chunk_rest.call(null,inst_36246);
var inst_36252 = cljs.core.count.call(null,inst_36250);
var inst_36233 = inst_36251;
var inst_36234 = inst_36250;
var inst_36235 = inst_36252;
var inst_36236 = (0);
var state_36373__$1 = (function (){var statearr_36399 = state_36373;
(statearr_36399[(7)] = inst_36233);

(statearr_36399[(8)] = inst_36234);

(statearr_36399[(9)] = inst_36235);

(statearr_36399[(10)] = inst_36236);

return statearr_36399;
})();
var statearr_36400_36478 = state_36373__$1;
(statearr_36400_36478[(2)] = null);

(statearr_36400_36478[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (22))){
var inst_36290 = (state_36373[(23)]);
var inst_36286 = (state_36373[(19)]);
var inst_36289 = (state_36373[(24)]);
var inst_36294 = (state_36373[(25)]);
var inst_36289__$1 = (state_36373[(2)]);
var inst_36290__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_36289__$1);
var inst_36291 = (function (){var all_files = inst_36286;
var res_SINGLEQUOTE_ = inst_36289__$1;
var res = inst_36290__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_36290,inst_36286,inst_36289,inst_36294,inst_36289__$1,inst_36290__$1,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__36215_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__36215_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_36290,inst_36286,inst_36289,inst_36294,inst_36289__$1,inst_36290__$1,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_36292 = cljs.core.filter.call(null,inst_36291,inst_36289__$1);
var inst_36293 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_36294__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_36293);
var inst_36295 = cljs.core.not_empty.call(null,inst_36294__$1);
var state_36373__$1 = (function (){var statearr_36401 = state_36373;
(statearr_36401[(23)] = inst_36290__$1);

(statearr_36401[(24)] = inst_36289__$1);

(statearr_36401[(25)] = inst_36294__$1);

(statearr_36401[(26)] = inst_36292);

return statearr_36401;
})();
if(cljs.core.truth_(inst_36295)){
var statearr_36402_36479 = state_36373__$1;
(statearr_36402_36479[(1)] = (23));

} else {
var statearr_36403_36480 = state_36373__$1;
(statearr_36403_36480[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (36))){
var state_36373__$1 = state_36373;
var statearr_36404_36481 = state_36373__$1;
(statearr_36404_36481[(2)] = false);

(statearr_36404_36481[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (41))){
var inst_36348 = (state_36373[(20)]);
var inst_36352 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_36353 = cljs.core.map.call(null,inst_36352,inst_36348);
var inst_36354 = cljs.core.pr_str.call(null,inst_36353);
var inst_36355 = [cljs.core.str.cljs$core$IFn$_invoke$arity$1("figwheel-no-load meta-data: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_36354)].join('');
var inst_36356 = figwheel.client.utils.log.call(null,inst_36355);
var state_36373__$1 = state_36373;
var statearr_36405_36482 = state_36373__$1;
(statearr_36405_36482[(2)] = inst_36356);

(statearr_36405_36482[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (43))){
var inst_36349 = (state_36373[(21)]);
var inst_36359 = (state_36373[(2)]);
var inst_36360 = cljs.core.not_empty.call(null,inst_36349);
var state_36373__$1 = (function (){var statearr_36406 = state_36373;
(statearr_36406[(27)] = inst_36359);

return statearr_36406;
})();
if(cljs.core.truth_(inst_36360)){
var statearr_36407_36483 = state_36373__$1;
(statearr_36407_36483[(1)] = (44));

} else {
var statearr_36408_36484 = state_36373__$1;
(statearr_36408_36484[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (29))){
var inst_36290 = (state_36373[(23)]);
var inst_36286 = (state_36373[(19)]);
var inst_36289 = (state_36373[(24)]);
var inst_36326 = (state_36373[(16)]);
var inst_36294 = (state_36373[(25)]);
var inst_36292 = (state_36373[(26)]);
var inst_36322 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_36325 = (function (){var all_files = inst_36286;
var res_SINGLEQUOTE_ = inst_36289;
var res = inst_36290;
var files_not_loaded = inst_36292;
var dependencies_that_loaded = inst_36294;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36326,inst_36294,inst_36292,inst_36322,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__36324){
var map__36409 = p__36324;
var map__36409__$1 = ((((!((map__36409 == null)))?((((map__36409.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36409.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36409):map__36409);
var namespace = cljs.core.get.call(null,map__36409__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36326,inst_36294,inst_36292,inst_36322,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_36326__$1 = cljs.core.group_by.call(null,inst_36325,inst_36292);
var inst_36328 = (inst_36326__$1 == null);
var inst_36329 = cljs.core.not.call(null,inst_36328);
var state_36373__$1 = (function (){var statearr_36411 = state_36373;
(statearr_36411[(28)] = inst_36322);

(statearr_36411[(16)] = inst_36326__$1);

return statearr_36411;
})();
if(inst_36329){
var statearr_36412_36485 = state_36373__$1;
(statearr_36412_36485[(1)] = (32));

} else {
var statearr_36413_36486 = state_36373__$1;
(statearr_36413_36486[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (44))){
var inst_36349 = (state_36373[(21)]);
var inst_36362 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_36349);
var inst_36363 = cljs.core.pr_str.call(null,inst_36362);
var inst_36364 = [cljs.core.str.cljs$core$IFn$_invoke$arity$1("not required: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_36363)].join('');
var inst_36365 = figwheel.client.utils.log.call(null,inst_36364);
var state_36373__$1 = state_36373;
var statearr_36414_36487 = state_36373__$1;
(statearr_36414_36487[(2)] = inst_36365);

(statearr_36414_36487[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (6))){
var inst_36267 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
var statearr_36415_36488 = state_36373__$1;
(statearr_36415_36488[(2)] = inst_36267);

(statearr_36415_36488[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (28))){
var inst_36292 = (state_36373[(26)]);
var inst_36319 = (state_36373[(2)]);
var inst_36320 = cljs.core.not_empty.call(null,inst_36292);
var state_36373__$1 = (function (){var statearr_36416 = state_36373;
(statearr_36416[(29)] = inst_36319);

return statearr_36416;
})();
if(cljs.core.truth_(inst_36320)){
var statearr_36417_36489 = state_36373__$1;
(statearr_36417_36489[(1)] = (29));

} else {
var statearr_36418_36490 = state_36373__$1;
(statearr_36418_36490[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (25))){
var inst_36290 = (state_36373[(23)]);
var inst_36306 = (state_36373[(2)]);
var inst_36307 = cljs.core.not_empty.call(null,inst_36290);
var state_36373__$1 = (function (){var statearr_36419 = state_36373;
(statearr_36419[(30)] = inst_36306);

return statearr_36419;
})();
if(cljs.core.truth_(inst_36307)){
var statearr_36420_36491 = state_36373__$1;
(statearr_36420_36491[(1)] = (26));

} else {
var statearr_36421_36492 = state_36373__$1;
(statearr_36421_36492[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (34))){
var inst_36342 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
if(cljs.core.truth_(inst_36342)){
var statearr_36422_36493 = state_36373__$1;
(statearr_36422_36493[(1)] = (38));

} else {
var statearr_36423_36494 = state_36373__$1;
(statearr_36423_36494[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (17))){
var state_36373__$1 = state_36373;
var statearr_36424_36495 = state_36373__$1;
(statearr_36424_36495[(2)] = recompile_dependents);

(statearr_36424_36495[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (3))){
var state_36373__$1 = state_36373;
var statearr_36425_36496 = state_36373__$1;
(statearr_36425_36496[(2)] = null);

(statearr_36425_36496[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (12))){
var inst_36263 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
var statearr_36426_36497 = state_36373__$1;
(statearr_36426_36497[(2)] = inst_36263);

(statearr_36426_36497[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (2))){
var inst_36225 = (state_36373[(13)]);
var inst_36232 = cljs.core.seq.call(null,inst_36225);
var inst_36233 = inst_36232;
var inst_36234 = null;
var inst_36235 = (0);
var inst_36236 = (0);
var state_36373__$1 = (function (){var statearr_36427 = state_36373;
(statearr_36427[(7)] = inst_36233);

(statearr_36427[(8)] = inst_36234);

(statearr_36427[(9)] = inst_36235);

(statearr_36427[(10)] = inst_36236);

return statearr_36427;
})();
var statearr_36428_36498 = state_36373__$1;
(statearr_36428_36498[(2)] = null);

(statearr_36428_36498[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (23))){
var inst_36290 = (state_36373[(23)]);
var inst_36286 = (state_36373[(19)]);
var inst_36289 = (state_36373[(24)]);
var inst_36294 = (state_36373[(25)]);
var inst_36292 = (state_36373[(26)]);
var inst_36297 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_36299 = (function (){var all_files = inst_36286;
var res_SINGLEQUOTE_ = inst_36289;
var res = inst_36290;
var files_not_loaded = inst_36292;
var dependencies_that_loaded = inst_36294;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36294,inst_36292,inst_36297,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__36298){
var map__36429 = p__36298;
var map__36429__$1 = ((((!((map__36429 == null)))?((((map__36429.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36429.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36429):map__36429);
var request_url = cljs.core.get.call(null,map__36429__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36294,inst_36292,inst_36297,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_36300 = cljs.core.reverse.call(null,inst_36294);
var inst_36301 = cljs.core.map.call(null,inst_36299,inst_36300);
var inst_36302 = cljs.core.pr_str.call(null,inst_36301);
var inst_36303 = figwheel.client.utils.log.call(null,inst_36302);
var state_36373__$1 = (function (){var statearr_36431 = state_36373;
(statearr_36431[(31)] = inst_36297);

return statearr_36431;
})();
var statearr_36432_36499 = state_36373__$1;
(statearr_36432_36499[(2)] = inst_36303);

(statearr_36432_36499[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (35))){
var state_36373__$1 = state_36373;
var statearr_36433_36500 = state_36373__$1;
(statearr_36433_36500[(2)] = true);

(statearr_36433_36500[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (19))){
var inst_36276 = (state_36373[(12)]);
var inst_36282 = figwheel.client.file_reloading.expand_files.call(null,inst_36276);
var state_36373__$1 = state_36373;
var statearr_36434_36501 = state_36373__$1;
(statearr_36434_36501[(2)] = inst_36282);

(statearr_36434_36501[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (11))){
var state_36373__$1 = state_36373;
var statearr_36435_36502 = state_36373__$1;
(statearr_36435_36502[(2)] = null);

(statearr_36435_36502[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (9))){
var inst_36265 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
var statearr_36436_36503 = state_36373__$1;
(statearr_36436_36503[(2)] = inst_36265);

(statearr_36436_36503[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (5))){
var inst_36235 = (state_36373[(9)]);
var inst_36236 = (state_36373[(10)]);
var inst_36238 = (inst_36236 < inst_36235);
var inst_36239 = inst_36238;
var state_36373__$1 = state_36373;
if(cljs.core.truth_(inst_36239)){
var statearr_36437_36504 = state_36373__$1;
(statearr_36437_36504[(1)] = (7));

} else {
var statearr_36438_36505 = state_36373__$1;
(statearr_36438_36505[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (14))){
var inst_36246 = (state_36373[(22)]);
var inst_36255 = cljs.core.first.call(null,inst_36246);
var inst_36256 = figwheel.client.file_reloading.eval_body.call(null,inst_36255,opts);
var inst_36257 = cljs.core.next.call(null,inst_36246);
var inst_36233 = inst_36257;
var inst_36234 = null;
var inst_36235 = (0);
var inst_36236 = (0);
var state_36373__$1 = (function (){var statearr_36439 = state_36373;
(statearr_36439[(32)] = inst_36256);

(statearr_36439[(7)] = inst_36233);

(statearr_36439[(8)] = inst_36234);

(statearr_36439[(9)] = inst_36235);

(statearr_36439[(10)] = inst_36236);

return statearr_36439;
})();
var statearr_36440_36506 = state_36373__$1;
(statearr_36440_36506[(2)] = null);

(statearr_36440_36506[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (45))){
var state_36373__$1 = state_36373;
var statearr_36441_36507 = state_36373__$1;
(statearr_36441_36507[(2)] = null);

(statearr_36441_36507[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (26))){
var inst_36290 = (state_36373[(23)]);
var inst_36286 = (state_36373[(19)]);
var inst_36289 = (state_36373[(24)]);
var inst_36294 = (state_36373[(25)]);
var inst_36292 = (state_36373[(26)]);
var inst_36309 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_36311 = (function (){var all_files = inst_36286;
var res_SINGLEQUOTE_ = inst_36289;
var res = inst_36290;
var files_not_loaded = inst_36292;
var dependencies_that_loaded = inst_36294;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36294,inst_36292,inst_36309,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__36310){
var map__36442 = p__36310;
var map__36442__$1 = ((((!((map__36442 == null)))?((((map__36442.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36442.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36442):map__36442);
var namespace = cljs.core.get.call(null,map__36442__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__36442__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36294,inst_36292,inst_36309,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_36312 = cljs.core.map.call(null,inst_36311,inst_36290);
var inst_36313 = cljs.core.pr_str.call(null,inst_36312);
var inst_36314 = figwheel.client.utils.log.call(null,inst_36313);
var inst_36315 = (function (){var all_files = inst_36286;
var res_SINGLEQUOTE_ = inst_36289;
var res = inst_36290;
var files_not_loaded = inst_36292;
var dependencies_that_loaded = inst_36294;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36294,inst_36292,inst_36309,inst_36311,inst_36312,inst_36313,inst_36314,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_36290,inst_36286,inst_36289,inst_36294,inst_36292,inst_36309,inst_36311,inst_36312,inst_36313,inst_36314,state_val_36374,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_36316 = setTimeout(inst_36315,(10));
var state_36373__$1 = (function (){var statearr_36444 = state_36373;
(statearr_36444[(33)] = inst_36309);

(statearr_36444[(34)] = inst_36314);

return statearr_36444;
})();
var statearr_36445_36508 = state_36373__$1;
(statearr_36445_36508[(2)] = inst_36316);

(statearr_36445_36508[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (16))){
var state_36373__$1 = state_36373;
var statearr_36446_36509 = state_36373__$1;
(statearr_36446_36509[(2)] = reload_dependents);

(statearr_36446_36509[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (38))){
var inst_36326 = (state_36373[(16)]);
var inst_36344 = cljs.core.apply.call(null,cljs.core.hash_map,inst_36326);
var state_36373__$1 = state_36373;
var statearr_36447_36510 = state_36373__$1;
(statearr_36447_36510[(2)] = inst_36344);

(statearr_36447_36510[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (30))){
var state_36373__$1 = state_36373;
var statearr_36448_36511 = state_36373__$1;
(statearr_36448_36511[(2)] = null);

(statearr_36448_36511[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (10))){
var inst_36246 = (state_36373[(22)]);
var inst_36248 = cljs.core.chunked_seq_QMARK_.call(null,inst_36246);
var state_36373__$1 = state_36373;
if(inst_36248){
var statearr_36449_36512 = state_36373__$1;
(statearr_36449_36512[(1)] = (13));

} else {
var statearr_36450_36513 = state_36373__$1;
(statearr_36450_36513[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (18))){
var inst_36280 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
if(cljs.core.truth_(inst_36280)){
var statearr_36451_36514 = state_36373__$1;
(statearr_36451_36514[(1)] = (19));

} else {
var statearr_36452_36515 = state_36373__$1;
(statearr_36452_36515[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (42))){
var state_36373__$1 = state_36373;
var statearr_36453_36516 = state_36373__$1;
(statearr_36453_36516[(2)] = null);

(statearr_36453_36516[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (37))){
var inst_36339 = (state_36373[(2)]);
var state_36373__$1 = state_36373;
var statearr_36454_36517 = state_36373__$1;
(statearr_36454_36517[(2)] = inst_36339);

(statearr_36454_36517[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36374 === (8))){
var inst_36233 = (state_36373[(7)]);
var inst_36246 = (state_36373[(22)]);
var inst_36246__$1 = cljs.core.seq.call(null,inst_36233);
var state_36373__$1 = (function (){var statearr_36455 = state_36373;
(statearr_36455[(22)] = inst_36246__$1);

return statearr_36455;
})();
if(inst_36246__$1){
var statearr_36456_36518 = state_36373__$1;
(statearr_36456_36518[(1)] = (10));

} else {
var statearr_36457_36519 = state_36373__$1;
(statearr_36457_36519[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__31616__auto__,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto____0 = (function (){
var statearr_36458 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_36458[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto__);

(statearr_36458[(1)] = (1));

return statearr_36458;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto____1 = (function (state_36373){
while(true){
var ret_value__31618__auto__ = (function (){try{while(true){
var result__31619__auto__ = switch__31616__auto__.call(null,state_36373);
if(cljs.core.keyword_identical_QMARK_.call(null,result__31619__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__31619__auto__;
}
break;
}
}catch (e36459){if((e36459 instanceof Object)){
var ex__31620__auto__ = e36459;
var statearr_36460_36520 = state_36373;
(statearr_36460_36520[(5)] = ex__31620__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36373);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36459;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__31618__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36521 = state_36373;
state_36373 = G__36521;
continue;
} else {
return ret_value__31618__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto__ = function(state_36373){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto____1.call(this,state_36373);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__31617__auto__;
})()
;})(switch__31616__auto__,c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__31706__auto__ = (function (){var statearr_36461 = f__31705__auto__.call(null);
(statearr_36461[(6)] = c__31704__auto__);

return statearr_36461;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__31706__auto__);
});})(c__31704__auto__,map__36218,map__36218__$1,opts,before_jsload,on_jsload,reload_dependents,map__36219,map__36219__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__31704__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(location.protocol),cljs.core.str.cljs$core$IFn$_invoke$arity$1("//")].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__36524,link){
var map__36525 = p__36524;
var map__36525__$1 = ((((!((map__36525 == null)))?((((map__36525.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36525.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36525):map__36525);
var file = cljs.core.get.call(null,map__36525__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4657__auto__ = link.href;
if(cljs.core.truth_(temp__4657__auto__)){
var link_href = temp__4657__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__4657__auto__,map__36525,map__36525__$1,file){
return (function (p1__36522_SHARP_,p2__36523_SHARP_){
if(cljs.core._EQ_.call(null,p1__36522_SHARP_,p2__36523_SHARP_)){
return p1__36522_SHARP_;
} else {
return false;
}
});})(link_href,temp__4657__auto__,map__36525,map__36525__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__4657__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__36528){
var map__36529 = p__36528;
var map__36529__$1 = ((((!((map__36529 == null)))?((((map__36529.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36529.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36529):map__36529);
var match_length = cljs.core.get.call(null,map__36529__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__36529__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__36527_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__36527_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__4657__auto__)){
var res = temp__4657__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.distinctify = (function figwheel$client$file_reloading$distinctify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__36531_SHARP_,p2__36532_SHARP_){
return cljs.core.assoc.call(null,p1__36531_SHARP_,cljs.core.get.call(null,p2__36532_SHARP_,key),p2__36532_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.add_link_to_document = (function figwheel$client$file_reloading$add_link_to_document(orig_link,klone,finished_fn){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
parent.removeChild(orig_link);

return finished_fn.call(null);
});})(parent))
,(300));
});
if(typeof figwheel.client.file_reloading.reload_css_deferred_chain !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_css_deferred_chain = cljs.core.atom.call(null,goog.async.Deferred.succeed());
}
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(f_data,fin){
var temp__4655__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__4655__auto__)){
var link = temp__4655__auto__;
return figwheel.client.file_reloading.add_link_to_document.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href),((function (link,temp__4655__auto__){
return (function (){
return fin.call(null,cljs.core.assoc.call(null,f_data,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),true));
});})(link,temp__4655__auto__))
);
} else {
return fin.call(null,f_data);
}
});
figwheel.client.file_reloading.reload_css_files_STAR_ = (function figwheel$client$file_reloading$reload_css_files_STAR_(deferred,f_datas,on_cssload){
return figwheel.client.utils.liftContD.call(null,figwheel.client.utils.mapConcatD.call(null,deferred,figwheel.client.file_reloading.reload_css_file,f_datas),(function (f_datas_SINGLEQUOTE_,fin){
var loaded_f_datas_36533 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),f_datas_SINGLEQUOTE_);
figwheel.client.file_reloading.on_cssload_custom_event.call(null,loaded_f_datas_36533);

if(cljs.core.fn_QMARK_.call(null,on_cssload)){
on_cssload.call(null,loaded_f_datas_36533);
} else {
}

return fin.call(null);
}));
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__36534,p__36535){
var map__36536 = p__36534;
var map__36536__$1 = ((((!((map__36536 == null)))?((((map__36536.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36536.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36536):map__36536);
var on_cssload = cljs.core.get.call(null,map__36536__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
var map__36537 = p__36535;
var map__36537__$1 = ((((!((map__36537 == null)))?((((map__36537.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36537.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36537):map__36537);
var files_msg = map__36537__$1;
var files = cljs.core.get.call(null,map__36537__$1,new cljs.core.Keyword(null,"files","files",-472457450));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var temp__4657__auto__ = cljs.core.not_empty.call(null,figwheel.client.file_reloading.distinctify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files));
if(cljs.core.truth_(temp__4657__auto__)){
var f_datas = temp__4657__auto__;
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.reload_css_deferred_chain,figwheel.client.file_reloading.reload_css_files_STAR_,f_datas,on_cssload);
} else {
return null;
}
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1503198192516
